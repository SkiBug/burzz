//
//  SubscriptionVCExtension.swift
//  Burzz
//
//  Created by Viru on 05/11/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire
import SwiftyStoreKit

extension SubscriptionVC{
    
    func alertWithTitle(title : String, message : String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
        
    }
    func showAlert(alert : UIAlertController) {
        guard let _ = self.presentedViewController else {
            self.present(alert, animated: true, completion: nil)
            return
        }
        
    }
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {

           if let product = result.retrievedProducts.first {
               let priceString = product.localizedPrice!
            return alertWithTitle(title: product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
           } else if let invalidProductId = result.invalidProductIDs.first {
            return alertWithTitle(title: "Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
           } else {
               let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
            return alertWithTitle(title: "Could not retrieve product info", message: errorString)
           }
       }
     // swiftlint:disable cyclomatic_complexity
       func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
           switch result {
           case .success(let purchase):
               print("Purchase Success: \(purchase.productId)")
               return nil
           case .error(let error):
               print("Purchase Failed: \(error)")
               switch error.code {
               case .unknown: return alertWithTitle(title: "Purchase failed", message: error.localizedDescription)
               case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle(title: "Purchase failed", message: "Not allowed to make the payment")
               case .paymentCancelled: // user cancelled the request, etc.
                   return nil
               case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle(title: "Purchase failed", message: "The purchase identifier was invalid")
               case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle(title: "Purchase failed", message: "The device is not allowed to make the payment")
               case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle(title: "Purchase failed", message: "The product is not available in the current storefront")
               case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle(title: "Purchase failed", message: "Access to cloud service information is not allowed")
               case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle(title: "Purchase failed", message: "Could not connect to the network")
               case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle(title: "Purchase failed", message: "Cloud service was revoked")
               default:
                return alertWithTitle(title: "Purchase failed", message: (error as NSError).localizedDescription)
               }
           }
       }
    
    func alertForRestorePurchases(result : RestoreResults) -> UIAlertController {
        if result.restoreFailedPurchases.count > 0 {
            print("Restore Failed: \(result.restoreFailedPurchases)")
            return alertWithTitle(title: "Restore Failed", message: "Unknown Error. Please Contact Support")
        }
        else if result.restoredPurchases.count > 0 {
            return alertWithTitle(title: "Purchases Restored", message: "All purchases have been restored.")
        }
        else {
            return alertWithTitle(title: "Nothing To Restore", message: "No previous purchases were made.")
        }
        
    }
     func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {

        switch result {
        case .success(let receipt):
            print("Verify receipt Success: \(receipt)")
            return alertWithTitle(title: "Receipt verified", message: "Receipt verified remotely")
        case .error(let error):
            print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle(title: "Receipt verification", message: "No receipt data. Try again.")
            case .networkError(let error):
                return alertWithTitle(title: "Receipt verification", message: "Network error while verifying receipt: \(error)")
            default:
                return alertWithTitle(title: "Receipt verification", message: "Receipt verification failed: \(error)")
            }
        }
    }
    func alertForVerifySubscriptions(_ result: VerifySubscriptionResult, productIds: Set<String>) -> UIAlertController {

         switch result {
         case .purchased(let expiryDate, let items):
             print("\(productIds) is valid until \(expiryDate)\n\(items)\n")
             UserDefaults.standard.setValue("1", forKey: "isProduct")
             return alertWithTitle(title: "Product is purchased", message: "Product is valid until \(expiryDate)")
         case .expired(let expiryDate, let items):
             print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
              UserDefaults.standard.setValue("2", forKey: "isProduct")
             return alertWithTitle(title: "Product expired", message: "Product is expired since \(expiryDate)")
         case .notPurchased:
             print("\(productIds) has never been purchased")
             UserDefaults.standard.setValue("3", forKey: "isProduct")
             return alertWithTitle(title: "Not purchased", message: "This product has never been purchased")
         }
     }
    func alertForVerifyPurchase(_ result: VerifyPurchaseResult, productId: String) -> UIAlertController {

          switch result {
          case .purchased:
              print("\(productId) is purchased")
              return alertWithTitle(title: "Product is purchased", message: "Product will not expire")
          case .notPurchased:
              print("\(productId) has never been purchased")
              return alertWithTitle(title: "Not purchased", message: "This product has never been purchased")
          }
      }
}
