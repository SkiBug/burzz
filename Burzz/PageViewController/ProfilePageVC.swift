import UIKit

class ProfilePageVC: UIPageViewController,UIPageViewControllerDelegate, UIPageViewControllerDataSource,UIScrollViewDelegate {
    
    var pageControl = UIPageControl()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        self.delegate = self
        self.dataSource = self
        
        pageControl.isUserInteractionEnabled = false
        
        // This sets up the first view that will show up on our page control
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
        
        configurePageControl()
        
        if currentUser.result?.interest == "1"
        {
            if let firstViewController = orderedViewControllers.first {
                setViewControllers([firstViewController],
                                   direction: .forward,
                                   animated: false,
                                   completion: nil)
            }
        }
        else if currentUser.result?.interest == "2"
        {
            self.goToSecond()
        }
        else
        {
            self.goToThird()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

        pageControl.isUserInteractionEnabled = false
        
    }
    
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "EditInfoVC"),self.newVc(viewController: "BuzzBff"),self.newVc(viewController: "BusinessVC")]
    }()
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            //  return orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            //return orderedViewControllers.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: (self.view.bounds.width-100)/2, y: 90, width: 100, height: 40))
        self.pageControl.numberOfPages = orderedViewControllers.count
        self.pageControl.currentPage = Int(currentUser.result?.interest ?? "1")! - 1
        self.pageControl.tintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.currentPageIndicatorTintColor = UIColor.yellow
        self.pageControl.layer.cornerRadius = pageControl.frame.height/2
        self.pageControl.layer.borderWidth = 2
        self.pageControl.layer.borderColor = UIColor.lightGray.cgColor
        self.pageControl.backgroundColor = UIColor.white
        self.view.addSubview(pageControl)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
    
    
    
    
    
}

extension UIPageViewController {
    
    func goToThird(animated: Bool = false) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        guard let thirdViewController = dataSource?.pageViewController(self, viewControllerAfter: nextViewController) else { return }
        setViewControllers([thirdViewController], direction: .forward, animated: animated, completion: nil)
    }
    
    func goToSecond(animated: Bool = false) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .reverse, animated: animated, completion: nil)
    }
    
}
