//
//  EditInfoVC.swift
//  Burzz
//
//  Created by Raul Menendez on 03/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire
import SafariServices
import AVFoundation
import SwiftyJSON

/*protocol Photopicker {
 func ptototickerContollerCall()
 }*/

//struct API{
//
//    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
//    static let INSTAGRAM_CLIENT_ID = "fcb7ac6a6c4d43738434d9da5eb55a32"
//    static let INSTAGRAM_CLIENTSERCRET = "fd2e7d647c2c4e84b379bc2c2b2d2937"
//    static let INSTAGRAM_REDIRECT_URI = "http://www.burzz.com"
//    static let INSTAGRAM_ACCESS_TOKEN = "access_token"
//    static let INSTAGRAM_SCOPE = "follower_list+public_content"
//    /* add whatever scope you need https://www.instagram.com/developer/authorization/ */
//
//}

struct API{
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_USER_INFO = "https://api.instagram.com/v1/users/self/?access_token="
    static let INSTAGRAM_USER_POST = "https://api.instagram.com/v1/users/self/media/recent/?access_token="
    static let INSTAGRAM_CLIENT_ID = "fcb7ac6a6c4d43738434d9da5eb55a32"
    static let INSTAGRAM_CLIENTSERCRET = "fd2e7d647c2c4e84b379bc2c2b2d2937"
    static let INSTAGRAM_REDIRECT_URI = "http://www.burzz.com"
    static var INSTAGRAM_ACCESS_TOKEN = "access_token"
    static let INSTAGRAM_SCOPE = "basic"
    
    /* add whatever scope you need https://www.instagram.com/developer/authorization/ */
}

class EditInfoVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate,SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate {
    
    //--------------------------------------
    // MARK: Variables
    //--------------------------------------
    
    // Variables
    var auth = SPTAuth.defaultInstance()!
    var session:SPTSession!
    
    // Initialzed in either updateAfterFirstLogin: (if first time login) or in viewDidLoad (when there is a check for a session object in User Defaults
    var player: SPTAudioStreamingController?
    var loginUrl: URL?
    
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var tableVEditInfo: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnSwitch: UIButton!
    
    @IBOutlet weak var instaGramView: UIView!
    
    var companyName = String()
    var jobName = String()
    var aboutme = String()
    var userProfileImage = UIImage()
    var responseProfileData : JSON = []
    
    var DataArray = ["","","","","","","","","","Male","Height","Exercise","Education level","Drinking","Smoking","Pets","Kids","Zodiac","Religion","","","",""]
    
    var DataDicArray: [[String : AnyObject]] = [[ "Question" : "What is your height?" as AnyObject],[ "Question" : "Do you work out?" as AnyObject],[ "Question" : "What's your education?" as AnyObject],[ "Question" : "Do you drink?" as AnyObject],[ "Question" : "Do you have pets?" as AnyObject],[ "Question" : "Do you smoke?" as AnyObject],[ "Question" : "what are your ideal plans for children?" as AnyObject],[ "Question" : "What's your zodiac sign?" as AnyObject],[ "Question" : "Do you identify with a religion?" as AnyObject]]
    
    //var dic2 = {"":""}
    var CategoryArray : [String] = []
    // var delegate : Photopicker?
    var CompleteDict : NSDictionary?
    var viewController1 : UIViewController!
    var selectGender = "Please select your gender"
    var arrEditInfo  = NSArray()
    
    var arrImg:[UIImage?] = [UIImage(named: "ScreenS5"), nil, nil, nil, nil,nil]
    var currentInd: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSwitch.layer.cornerRadius = btnSwitch.frame.height / 2.0
        btnSwitch.layer.masksToBounds = true
        
        self.headerView.dropShadow()
        if currentUser.result?.gender == "1"{
            selectGender = "Male"
        }
        else{
            selectGender = "Female"
        }
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getBrandName(_:)), name: NSNotification.Name(rawValue: "ForBrandName"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ShowpopUp(notification:)), name: Notification.Name("ShowPopUp"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ShowLoader(notification:)), name: Notification.Name("ShowLoader"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HideLoader(notification:)), name: Notification.Name("HideLoader"), object: nil)
        
        setup()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterFirstLogin), name: NSNotification.Name(rawValue: "loginSuccessfull"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterFirstLogin), name: NSNotification.Name(rawValue: "CallPreferenceApi"), object: nil)
        
    }
    
    
    
    @objc func callPereferanceAPI() {
        
        
        
    }
    
    
    
    func setup () {
        // insert redirect your url and client ID below
        let redirectURL = "burzz://spotify-login-callback" // put your redirect URL here
        let clientID = "4199654919064c03ac7d737c75c56d1b" // put your client ID here
        auth.redirectURL     = URL(string: redirectURL)
        auth.clientID        = "4199654919064c03ac7d737c75c56d1b"
        auth.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthPlaylistModifyPublicScope, SPTAuthPlaylistModifyPrivateScope]
        loginUrl = auth.spotifyWebAuthenticationURL()
        
    }
    
    @IBAction func onClickBackInstaGram(_ sender: Any) {
        instaGramView.isHidden = true
    }
    
    @objc func updateAfterFirstLogin () {
        
        //  loginButton.isHidden = true
        let userDefaults = UserDefaults.standard
        
        if let sessionObj:AnyObject = userDefaults.object(forKey: "SpotifySession") as AnyObject? {
            
            let sessionDataObj = sessionObj as! Data
            let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
            
            self.session = firstTimeSession
            initializaPlayer(authSession: session)
            //   self.loginButton.isHidden = true
            // self.loadingLabel.isHidden = false
            
            tableVEditInfo.reloadData()
            
        }
        
    }
    
    func initializaPlayer(authSession:SPTSession){
        if self.player == nil {
            
            
            //            self.player = SPTAudioStreamingController.sharedInstance()
            //            self.player!.playbackDelegate = self
            //            self.player!.delegate = self
            //            try! player?.start(withClientId: auth.clientID)
            //            self.player!.login(withAccessToken: authSession.accessToken)
            
        }
        
    }
    
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        // after a user authenticates a session, the SPTAudioStreamingController is then initialized and this method called
        print("logged in")
        self.player?.playSpotifyURI("burzz://spotify-login-callback", startingWith: 0, startingWithPosition: 0, callback: { (error) in
            if (error != nil) {
                print("playing!")
            }
        })
        
    }
    
    
    @objc func ShowpopUp(notification:Notification)
    {
        let notifiData = notification.object as! Int
        
        if notifiData == 1
        {
            self.addAlertView("", message: "Sorry you can't delete your last photo. You're required to have a profile photo so people can see you", buttonTitle: "ok")
        }
        else if notifiData == 2
        {
            let myAlert = UIAlertController(title: "Delete this photo?", message:"Are you sure you want to delete this photo?", preferredStyle: UIAlertController.Style.alert);
            let okAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler:nil)
            
            let delete = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler:  { action in
                
                
            })
            
            // this action can add to more button
            myAlert.addAction(okAction);
            myAlert.addAction(delete);
            
            self.present(myAlert, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    @objc func ShowLoader(notification:Notification)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
    }
    
    
    @objc func HideLoader(notification:Notification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        ISVerified()
        
        if currentUser.result?.interest == "1"
        {
            APiCalling()
        }
        else
        {
            tableVEditInfo.isHidden = true
            self.btnSave.isHidden = true
        }
        
    }
    
    
    
    
    @IBAction func click_to_BuzzDate(_ sender : Any)
    {
        self.setIntrestType()
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 5{
            jobName = textField.text!
        }
        else{
            companyName = textField.text!
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        aboutme = textView.text
    }
    
    @objc func getBrandName(_ sender:Notification){
        
        currentInd = sender.object as? Int ?? 0
        
        let myAlert = UIAlertController(title: "Warning", message:"You are required to use a real photo with your face and nothing else.If we suspect you are an impostor, your  account will be permanently deleted and phone IP blocked.  ", preferredStyle: UIAlertController.Style.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
            self.getImageSource()
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func getImageSource(){
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func ptototickerContollerCall() {
        print("get call.....get call......get call")
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    @IBAction func onClickUserUpdateDetail(_ sender: Any) {
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        var gender = String()
        if selectGender == "Male"{
            gender = "1"
        }
        else{
            gender = "2"
        }
        let dict : [String:String] = ["id":(currentUser.result?.id)!,"about_me" :aboutme ,"job": jobName,"company":companyName,"gender":gender]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url, dictData: dict )
    }
    
    
    func APiCalling()
    {
        let strURL =  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.kMasterAPI
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let dicURL : [String : String] = ["type":currentUser.result!.interest!,"user_id": currentUser.result!.id!,"update_value":"", "update_key":""]
        
        APIHelper.sharedInstance.requestPOSTWithouAUTHURL(strURL, params :dicURL,headers :nil, success: {
            (JSONResponse, resp) -> Void in
            
            // print(JSONResponse)
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responseDict = JSONResponse.dictionary
            let statusCode = responseDict!["status"]?.bool
            
            
            if(statusCode == false)
            {
                self.addAlertView(appName, message: "Something went wrong." , buttonTitle: ALERTS.kAlertOK)
            }
            
            self.arrEditInfo = []
            self.arrEditInfo = resp["data"] as! NSArray
            
            
            if(statusCode == true)
            {
                
                let category = (responseDict!["data"]?.array)!
                
                self.CategoryArray = []
                for dic in category
                {
                    
                    if let title = dic["title"].string
                    {
                        self.CategoryArray.append(title)
                    }
                    
                }
                
                self.tableVEditInfo.reloadData()
                
            }
            
            DispatchQueue.main.async {
                self.tableVEditInfo.reloadData()
            }
            
        },failure:{
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
            
            
            DispatchQueue.main.async {
                
            }
        })
        
    }
    
    
    func setIntrestType()
    {
        let strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        let dict : [String:Any] = [ "id": currentUser.result!.id!, "interest": "1"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPILogin(APIurl: strURL, dictData: dict as? [String : String])
        APiCalling()
    }
    
    func CallAPIProfile(APIurl: URL,dictData : [String:String]?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            self.responseProfileData = JSON(response.result.value!)
                            print(self.responseProfileData)
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        currentUser = User1.init(dictionary:JSON)
                                        self.tableVEditInfo.reloadData()
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
    
    func CallAPILogin(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        currentUser = User1.init(dictionary:JSON)
                                        self.btnSave.isHidden = false
                                        if currentUser.result?.interest == "1"{
                                            self.tableVEditInfo.isHidden = false
                                        }
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as? NSString as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 14
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 1
        }
        else if section == 2
        {
            return 1
        }
        else if section == 3
        {
            return 1
        }
        else if section == 4
        {
            return 1
        }
        else if section == 5
        {
            return 2
        }
            //        else if section == 6
            //        {
            //            return 1
            //        }
            //        else if section == 7
            //        {
            //            return 1
            //        }
        else if section == 6
        {
            return 1
        }
        else if section == 7
        {
            return 1
        }
        else if section == 8
        {
            return CategoryArray.count
        }
        else if section == 9
        {
            return 0//1 for instagram button
        }
        else if section == 10
        {
            return 0//1 for instagram images
        }
        else if section == 11
        {
            return 1
        }
        else if section == 12
        {
            let  sessionData =  UserDefaults.standard.value(forKey: "SpotifySession")
            if sessionData != nil{
                return 0
            }else {
                return 1
            }
        }
        else
        {
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell1", for: indexPath) as? tableEditInfoCell1
        cell?.arrImg = arrImg
        
        let cell2 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell2", for: indexPath) as? tableEditInfoCell2
        
        let cell3 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell3", for: indexPath) as? tableEditInfoCell3
        
        let cell4 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell4", for: indexPath) as? tableEditInfoCell4
        
        let cell5 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell5", for: indexPath) as? tableEditInfoCell5
        
        
        let arr = ["About me", "Work And Education","Company","My basic info"]
        
        
        if indexPath.section == 0
        {
            cell?.collectionViewEdit.tag = indexPath.row + 100
            cell?.collectionViewEdit.reloadData()
            cell?.collectionHeight.constant =  (cell?.collectionViewEdit.frame.size.width)!*0.70
            return cell!
        }
        else if indexPath.section == 1
        {
            /*let is_verified = UserDefaults.standard.value(forKey: "is_verified") as? String ?? "0"
            if is_verified == "0"{
                cell2?.lblAccountVerify.text = "Your Account is not Verified!"
                cell2?.isVerifiedView.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                cell2?.imgIcon.image = UIImage(named: "cross.png")
                cell2?.lblAccountVerify.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell2?.isVerifiedView.backgroundColor = #colorLiteral(red: 0.9928157926, green: 0.932531178, blue: 0, alpha: 1)
                cell2?.lblAccountVerify.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell2?.imgIcon.image = UIImage(named: "check.png")
                cell2?.lblAccountVerify.text = "Account is Verified"
            }*/
            cell2?.isVerifiedView.backgroundColor = #colorLiteral(red: 0.9928157926, green: 0.932531178, blue: 0, alpha: 1)
            cell2?.lblAccountVerify.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell2?.imgIcon.image = UIImage(named: "check.png")
            cell2?.lblAccountVerify.text = "Account is Verified"
            
            return cell2!
            
        }
            
        else if indexPath.section == 2
        {
            cell3?.lblTitle.text = arr[0]
            return cell3!
        }
        else if indexPath.section == 3
        {
            cell4!.lblTextView.delegate = self
            cell4!.lblTextView.text = currentUser.result!.about_me!
            aboutme = currentUser.result!.about_me!
            
            return cell4!
        }
        else if indexPath.section == 4
        {
            cell3?.lblTitle.text = arr[1]
            return cell3!
        }
        else if indexPath.section == 5
        {
            cell5!.txtFieldtext.tag = 5
            
            if indexPath.row == 0
            {
                let jobArray = currentUser.result?.job_data?.count
                
                if jobArray == 0
                {
                    cell5!.txtFieldtext.text = "Add job"
                    cell5!.txtFieldtext.textColor = UIColor.lightGray
                }
                else
                {
                    cell5!.txtFieldtext.text = "\(jobArray ?? 0) job shown"
                    cell5!.txtFieldtext.textColor = UIColor.black
                }
                
            }
            else
            {
                let eductionCount = currentUser.result?.education_data?.count
                if eductionCount == 0
                {
                    cell5!.txtFieldtext.text = "Add Education"
                    cell5!.txtFieldtext.textColor = UIColor.lightGray
                }
                else
                {
                    cell5!.txtFieldtext.text = "\(eductionCount ?? 0) institutions shown"
                    cell5!.txtFieldtext.textColor = UIColor.black
                }
            }
            
            return cell5!
        }
            
            //        else if indexPath.section == 6
            //        {
            //            cell3?.lblTitle.text = arr[2]
            //            return cell3!
            //
            //        }
            //        else if indexPath.section == 7
            //        {
            //            cell5!.txtFieldtext.tag = 7
            //            cell5!.txtFieldtext.text = currentUser.result!.company!
            //            cell5!.txtFieldtext.text = "Add Company"
            //            return cell5!
            //        }
            
        else if indexPath.section == 6
        {
            cell3?.lblTitle.text = arr[3]
            return cell3!
        }
        else if indexPath.section == 7
        {
            cell5!.txtFieldtext.text = selectGender
            cell5!.txtFieldtext.isUserInteractionEnabled = false
            return cell5!
        }
        else if indexPath.section == 8
        {
            let cell6 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell6", for: indexPath) as? tableEditInfoCell6
            
            cell6?.selectionStyle = .none
            cell6!.backView.backgroundColor = UIColor.white
            cell6?.lbltext.text = CategoryArray[indexPath.row]
            cell6?.lbltext.textColor = UIColor.init(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 1.0)
            cell6?.lbltext.font = UIFont.systemFont(ofSize: 15.0)
            let dicPreference = arrEditInfo[indexPath.row] as! NSDictionary
            
            let optionArray = dicPreference["options"] as! NSArray
            let preference = dicPreference["pref"] as! String
            
            for dic in optionArray
            {
                if (dic as! NSDictionary).value(forKey: "id") as! String == preference
                {
                    cell6?.lblPreference.text = (dic as! NSDictionary).value(forKey: "title") as? String
                }
            }
            
            let iconUrl = dicPreference["icon"] as? String
            cell6?.imageV.sd_setImage(with: URL(string: iconUrl!), placeholderImage: UIImage(named: ""))
            cell6?.imageV.isHidden = false
            cell6?.lblPreference.isHidden = false
            cell6?.backGroundImage.isHidden = true
            cell6?.nextImage.image = #imageLiteral(resourceName: "forward_arrow")
            cell6?.lbltext.reloadInputViews()
            return cell6!
        }
        else if indexPath.section == 9
        {
            let cell6 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell6", for: indexPath) as? tableEditInfoCell6
            
            cell6!.backView.backgroundColor = UIColor.clear
            
            cell6!.backView.layer.cornerRadius = 20
            cell6?.backView.clipsToBounds = true
            cell6?.lbltext.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell6?.lbltext.textColor = UIColor.white
            cell6?.backGroundImage.image = UIImage(named: "insta_btn")
            
            
            let InstaImages = currentUser.result?.insta_images
            if InstaImages?.count == 0 || InstaImages![0].insta_img_url! == ""{
                cell6?.lbltext.text = "Connect your instagram"
                cell6?.nextImage.image = #imageLiteral(resourceName: "white_plus")
            }else{
                cell6?.lbltext.text = "Connected to instagram"
                cell6?.nextImage.image = #imageLiteral(resourceName: "checkbox-pressed")
            }
            
            
            
            cell6?.imageV.image = #imageLiteral(resourceName: "instagram")
            cell6?.lblPreference.isHidden = true
            cell6?.backGroundImage.isHidden = false
            return cell6!
        }
        else if indexPath.section == 10
        {
            cell?.collectionViewEdit.tag = indexPath.row + 101
            cell?.collectionViewEdit.reloadData()
            cell?.collectionHeight.constant =  (cell?.collectionViewEdit.frame.size.width)!*0.70
            return cell!
        }
        else if indexPath.section == 11
        {
            let cell6 = tableVEditInfo.dequeueReusableCell(withIdentifier: "tableEditInfoCell6", for: indexPath) as? tableEditInfoCell6
            
            cell6!.backView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 182.0/255.0, blue: 94.0/255.0, alpha: 1.0)
            cell6?.lbltext.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell6?.lbltext.textColor = UIColor.white
            let spotifyData = UserDefaults.standard.value(forKey: "SpotifySession")
            if spotifyData != nil{
                cell6?.lbltext.text = "Connected to spotify"
                cell6?.nextImage.image = #imageLiteral(resourceName: "success")
            }else{
                cell6?.lbltext.text = "Connect your spotify"
                cell6?.nextImage.image = #imageLiteral(resourceName: "white_plus")
            }
            
            cell6?.imageV.image = #imageLiteral(resourceName: "spotify")
            cell6?.lblPreference.isHidden = true
            cell6?.backGroundImage.isHidden = true
            return cell6!
        }
        else if indexPath.section == 12
        {
            cell?.collectionViewEdit.tag = indexPath.row
            cell?.collectionViewEdit.reloadData()
            cell?.collectionHeight.constant =  (cell?.collectionViewEdit.frame.size.width)!*0.35
            return cell!
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 5
        {
            if indexPath.row == 0
            {
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "MyWork_EducationVC") as! MyWork_EducationVC
                newViewController.jobOrEducation = "job"
                newViewController.refreshData = self
                self.navigationController?.pushViewController(newViewController, animated: true)
            }
            else
            {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "MyWork_EducationVC") as! MyWork_EducationVC
                newViewController.jobOrEducation = "education"
                newViewController.refreshData = self
                
                self.navigationController?.pushViewController(newViewController, animated: true)
                
            }
        }
        
        
        if indexPath.section == 6{
            
            
        }
        
        
        
        if indexPath.section == 7
        {
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                print("Cancel")
            }
            actionSheetControllerIOS8.addAction(cancelActionButton)
            
            let saveActionButton = UIAlertAction(title: "Male", style: .default)
            { _ in
                self.selectGender = "Male"
                self.tableVEditInfo.reloadData()
            }
            actionSheetControllerIOS8.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "Female", style: .default)
            { _ in
                self.selectGender = "Female"
                self.tableVEditInfo.reloadData()
                
            }
            actionSheetControllerIOS8.addAction(deleteActionButton)
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
        }
            
        else if indexPath.section == 8
        {
            let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditInfoQuestionVC") as! EditInfoQuestionVC
            newViewController.arrAllQuestion = arrEditInfo as! [Any]
            newViewController.newDict  = arrEditInfo[indexPath.row] as! NSDictionary
            newViewController.arrAllQuestion = arrEditInfo as! [Any]
            newViewController.index = indexPath.row
            newViewController.type = "1"
            newViewController.apiCall = self
            self.navigationController?.pushViewController(newViewController, animated: true)
            
        }
        else if indexPath.section == 9{
            
            let InstaImages = currentUser.result?.insta_images
            
            if InstaImages?.count == 0 || InstaImages![0].insta_img_url! == ""{
                
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableVEditInfo.scrollToRow(at: indexPath, at: .top, animated: true)
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                DispatchQueue.main.async {
                    let alert = UIAlertController.init(title: "Instagram", message: "Are you sure want to Disconnect from Instagram?", preferredStyle: .alert)
                    let yes = UIAlertAction.init(title: "Yes", style: .default) { (action) in
                        
                        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
                        let dict : [String:String] = ["id":(currentUser.result?.id)!,"insta_images" : "","instagram":""]
                        DispatchQueue.main.async {
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.CallAPIProfile(APIurl: url, dictData: dict )
                        }
                    }
                    let no = UIAlertAction.init(title: "No", style: .default) { (action) in
                        
                    }
                    alert.addAction(no)
                    alert.addAction(yes)
                    self.present(alert , animated: true)
                }
                
            }
        }
        else if indexPath.section == 11
        {
            let sessionData =  UserDefaults.standard.value(forKey: "SpotifySession")
            
            if sessionData == nil{
                if UIApplication.shared.openURL(loginUrl!) {
                    if auth.canHandle(auth.redirectURL) {
                        // To do - build in error handling
                    }
                }
            }else{
                
                DispatchQueue.main.async {
                    let alert = UIAlertController.init(title: "Spotify", message: "Are you sure want to Disconnect from Spotify?", preferredStyle: .alert)
                    let yes = UIAlertAction.init(title: "Yes", style: .default) { (action) in
                        // call API
                        UserDefaults.standard.removeObject(forKey: "SpotifySession")
                        UserDefaults.standard.synchronize()
                        self.tableVEditInfo.reloadData()
                    }
                    let no = UIAlertAction.init(title: "No", style: .default) { (action) in
                    }
                    alert.addAction(no)
                    alert.addAction(yes)
                    self.present(alert , animated: true)
                }
            }
        }
        
    }
    
}

extension tableEditInfoCell1 : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,DrapDropCollectionViewDelegate{
    
    
    func dragDropCollectionViewDidMoveCellFromInitialIndexPath(_ initialIndexPath: IndexPath, toNewIndexPath newIndexPath: IndexPath) {
        
        let colorToMove = arrImg[initialIndexPath.row]
        arrImg.remove(at: initialIndexPath.row)
        arrImg.insert(colorToMove, at: newIndexPath.row)
        
        var strImages = String()
        for dic in arrImg
        {
            if dic != nil
            {
                strImages.append(dic as! String + ",")
            }
        }
        
        strImages = String(strImages.dropLast())
        let dict : [String:AnyObject] = ["id":currentUser.result!.id as AnyObject,"images": strImages as AnyObject]
        PostAPIHit(dict)
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Ccell = collectionViewEdit.dequeueReusableCell(withReuseIdentifier: "collectionEditinfoCell1", for: indexPath) as? collectionEditinfoCell1
        Ccell?.btnImgPic.tag = indexPath.row
        Ccell?.btnImgPic.addTarget(self, action: #selector(didtapPhotoPicker), for: .touchUpInside)
        
        Ccell?.btnDelete.tag = indexPath.row
        Ccell?.btnDelete.addTarget(self, action: #selector(didTapRemoveImage), for: .touchUpInside)
        
        Ccell?.imagView.contentMode = .scaleAspectFill
        
        Ccell?.imagView.layer.cornerRadius = (Ccell?.imagView.bounds.height ?? 1)/2
        Ccell?.imagView.clipsToBounds = true
        
        collectionViewEdit.draggingDelegate = self
        collectionViewEdit.enableDragging(true)
        
        let imageArray = currentUser.result?.images
        
        for (ind, _) in arrImg.enumerated()
        {
            arrImg[ind] = nil
        }
        
        for (index,dic) in (imageArray?.enumerated())!
        {
            arrImg[index] = dic.image_url
            
        }
        
        
        if collectionView.tag == 100{
            
            if arrImg[indexPath.row] != nil
            {
                Ccell?.btnImgPic.isHidden = true
                Ccell?.deleteView.isHidden = false
                let image = arrImg[indexPath.row]
                if image is String
                {
                    Ccell?.imagView.sd_setImage(with: URL(string:image as! String), placeholderImage: UIImage(named: "profile_one_default"))
                }
                
            }
            else if imageArray!.count  == indexPath.row
            {
                Ccell?.deleteView.isHidden = true
                Ccell?.btnImgPic.isHidden = false
                Ccell?.imagView.image = UIImage(named: "add_image_active")
                
            }
            else
            {
                Ccell?.deleteView.isHidden = true
                Ccell?.btnImgPic.isHidden = true
                Ccell?.imagView.image = UIImage(named: "add_image_default")
                
            }
            
            
        }
        if collectionView.tag == 101{
            
            
            collectionViewEdit.enableDragging(false)
            
            let instaImageArray = currentUser.result?.insta_images
            
            if instaImageArray?.count != 0 && instaImageArray![0].insta_img_url! != ""
            {
                Ccell?.btnImgPic.isHidden = true
                Ccell?.deleteView.isHidden = true
                
                Ccell?.imagView.sd_setImage(with: URL(string:instaImageArray![indexPath.row].insta_img_url!), placeholderImage: UIImage(named: "profile_one_default"))
                
            }
            else if instaImageArray!.count  == indexPath.row
            {
                Ccell?.deleteView.isHidden = true
                Ccell?.btnImgPic.isHidden = true
                Ccell?.imagView.image = UIImage(named: "add_image_active")
                
            }
            else
            {
                Ccell?.deleteView.isHidden = true
                Ccell?.btnImgPic.isHidden = true
                Ccell?.imagView.image = UIImage(named: "add_image_default")
                
            }
        }
        
        
        
        return Ccell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100{
            return 6
        }
        else if collectionView.tag == 101 {
            return 6
        }
        else{
            let sessionData =  UserDefaults.standard.value(forKey: "SpotifySession")
            if sessionData != nil{
                return 0
            }else{
                return 3
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewEdit.frame.size.width*0.33 - 5, height: collectionViewEdit.frame.size.width*0.33 - 5)
    }
    
    
    func hitUploadImageToSSS(dataDict:  [String : Any]) {
        
        NotificationCenter.default.post(name: Notification.Name("ShowLoader"), object: 1)
        
        NotificationCenter.default.removeObserver(self, name: .dataDownloadCompleted, object: nil)
        
        AmazonSSS.uploadDocumentToS3Server(dataDict: dataDict as NSDictionary) { (result, response, error, errorMessage) in
            
            if( error==nil && result == true){
                
                print(response!)
                var imageUrl = String(describing: response!)
                imageUrl = imageUrl.replacingOccurrences(of: "burzz/", with: "burzz//")
                
                
                let arrAllImages = currentUser.result?.images
                var str = String()
                for dic in arrAllImages!
                {
                    str.append(dic.image_url! + ",")
                    self.ImageURl.append(dic.image_url!)
                    
                }
                
                str.append(imageUrl)
                self.ImageURl.append(imageUrl)
                
                let dict : [String:AnyObject] = ["id":currentUser.result!.id as AnyObject,"images": str as AnyObject]
                
                self.PostAPIHit(dict)
            }
            else{
                
            }
        }
    }
    
    func UploadImg (APIurl: URL,dictData : Parameters){
        //        Alamofire.request(APIurl, method: .post, parameters: dictData, encoding: JSONEncoding.default, headers: nil).responseJSON
        //                    {
        //                        (responseObject) -> Void in
        //                     //   MBProgressHUD.hide(for: self.view, animated: true)
        //                        print(responseObject)
        ////                       let json = responseObject.result.value as? NSDictionary
        ////                        currentUser = User.init(dictionary:json!)
        ////                        self.collectionViewEdit.reloadData()
        //
        //
        //
        //                     //   self.addAlertView(appName, message: "Image Uploaded." , buttonTitle: ALERTS.kAlertOK)
        //                }
        
        
        
    }
    
    
    func PostAPIHit(_ param : Parameters) {
        var url = String()
        
        url = APIManager.sharedInstance.KBASEURL+APIManager.sharedInstance.KUPDATEPROFILE
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if key == "images" {
                    //   for value in self.ImageURl {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
                    //    }
                }
                else
                {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
                }
            }
        }, usingThreshold: UInt64(), to: url, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    
                    NotificationCenter.default.post(name: Notification.Name("HideLoader"), object: 1)
                    
                })
                upload.responseJSON(completionHandler: { (response) in
                    debugPrint(response)
                    switch response.result {
                    case .success(_):
                        if let JSON = response.result.value as? NSDictionary
                        {
                            if let statusData = JSON["status"] as? Int{
                                if statusData == 1{
                                    print(JSON)
                                    
                                    
                                    let data = JSON["data"] as! NSDictionary
                                    let is_verified = data["is_verified"] as! String
                                    UserDefaults.standard.setValue(is_verified, forKey: "is_verified")
                                    
                                    
                                    let dict = (response.result.value as! NSDictionary)
                                    QLSharedPreference.sharedInstance.saveUserData(dict)
                                    currentUser = User1.init(dictionary:JSON)
                                    let AllImageArray = currentUser.result?.images
                                    
                                    for (index,_) in self.arrImg.enumerated()
                                    {
                                        if AllImageArray?.count ?? 0 > index
                                        {
                                            self.arrImg[index] = AllImageArray?[index].image_url
                                        }
                                    }
                                    
                                    DispatchQueue.main.async {
                                        self.collectionViewEdit.reloadData()
                                    }
                                    
                                }else{
                                    
                                }
                            }
                        }
                    case .failure(let encodingError):
                        // MKNSpinner.hide()
                        
                        if let err = encodingError as? URLError, err.code == .notConnectedToInternet || err.code == .timedOut {
                            
                        } else {
                            DispatchQueue.main.async(execute: { })
                            
                        }
                    }
                })
            case .failure(let encodingError):
                
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet || err.code == .timedOut {
                    
                } else {
                    
                }
            }
        })
    }
    
    
    @objc func uploadImage(sender: Notification){
        
        let arr = sender.object as! [String: Any]
        let arrimage = arr["image"] as? NSArray
        let index = arr["index"] as? Int
        let image = arrimage?[index!] as? UIImage
        
        let dic : [String : Any] = ["name" : image,"type":"image","identifier" :"image","path" : APIManager.sharedInstance.S3BucketNameProfileImage]
        
        self.hitUploadImageToSSS(dataDict: dic)
        
    }
    
    
    
    
    @objc func didtapPhotoPicker(sender: UIButton){
        
        if arrImg[sender.tag] == nil
        {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ForBrandName"), object:sender.tag)
            
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.uploadImage(sender:)),
                                                   name: .dataDownloadCompleted,
                                                   object: nil)
        }
        
    }
    
    @objc func didTapRemoveImage(sender: UIButton)
    {
        if currentUser.result?.images?.count == 1
        {
            NotificationCenter.default.post(name: Notification.Name("ShowPopUp"), object: 1)
            
        }
        else
        {
            arrImg[sender.tag] = nil
            
            var strImages = String()
            
            for dic in arrImg
            {
                if dic != nil
                {
                    strImages.append(dic as! String + ",")
                }
            }
            
            strImages = String(strImages.dropLast())
            let dict : [String:AnyObject] = ["id":currentUser.result!.id as AnyObject,"images": strImages as AnyObject]
            PostAPIHit(dict)
            
            
        }
        
        
    }
}

extension EditInfoVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func camera()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate;
        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPicController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        arrImg[currentInd] = image
        
        let ImageDate : [String:Any] = ["image": arrImg,"index": currentInd ]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: ImageDate, userInfo: nil)
        
        UserDefaults.standard.set(0, forKey: "rotetedBy")
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

extension UIImageView {
    func roundedImage() {
        self.layer.cornerRadius = (self.frame.size.width) / 2;
        self.clipsToBounds = true
        self.layer.borderWidth = 3.0
        self.layer.borderColor = UIColor.orange.cgColor
    }
    func roundedImageGray() {
        self.layer.cornerRadius = (self.frame.size.width) / 2;
        self.clipsToBounds = true
        self.layer.borderWidth = 3.0
        self.layer.borderColor = UIColor.gray.cgColor
    }
}

extension Notification.Name {
    static let dataDownloadCompleted = Notification.Name(
        rawValue: "dataDownloadCompleted")
}

extension EditInfoVC: ApiCall{
    func pageDismiss() {
        if currentUser.result?.interest == "1"
        {
            APiCalling()
        }
        else
        {
            tableVEditInfo.isHidden = true
            self.btnSave.isHidden = true
        }
    }
}

extension EditInfoVC : refreshUserData{
    func pageRefresh() {
        
        if currentUser.result?.interest == "1"
        {
            APiCalling()
            self.setIntrestType()
        }
        else
        {
            tableVEditInfo.isHidden = true
            self.btnSave.isHidden = true
        }
    }
    
}
extension EditInfoVC{
    func ISVerified()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.IS_VERIFIED)
        let dicURL : [String : String] = ["id": currentUser.result!.id!]
        self.ISVerifiedAPI(APIurl: url!, dictData: dicURL )
        
    }
    
    func ISVerifiedAPI(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        
                                        let data = dict["data"] as! NSDictionary
                                        
                                        let is_verified = data["is_verified"] as? String ?? "0"
                                        UserDefaults.standard.setValue(is_verified, forKey: "is_verified")
                                        
                                    }
                                    else{
                                        
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
}
