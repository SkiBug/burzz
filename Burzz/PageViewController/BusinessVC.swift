//
//  BusinessVC.swift
//  Burzz
//
//  Created by MAC MINI on 16/07/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire

class BusinessVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    var auth = SPTAuth.defaultInstance()!
    var session:SPTSession!
    
    // Initialzed in either updateAfterFirstLogin: (if first time login) or in viewDidLoad (when there is a check for a session object in User Defaults
    var player: SPTAudioStreamingController?
    var loginUrl: URL?
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnSwitch: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    //var dic2 = {"":""}
    var CategoryArray : [String] = []
    // var delegate : Photopicker?
    var CompleteDict : NSDictionary?
    var viewController1 : UIViewController!
    var selectGender = "Please select your gender"
    var arrEditInfo  = NSArray()
    
    var arrImg:[UIImage?] = [UIImage(named: "ScreenS5"), nil, nil, nil, nil,nil]
    var currentInd: Int = 0
    var companyName = String()
    var jobName = String()
    var aboutme = String()
    
    var headline = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSwitch.layer.cornerRadius = btnSwitch.frame.height / 2.0
        btnSwitch.layer.masksToBounds = true
        
        if currentUser.result?.gender == "1"{
            selectGender = "Male"
        }
        else{
            selectGender = "Female"
        }
        
        self.headerView.dropShadow()
        
        APiCalling()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getBrandName(_:)), name: NSNotification.Name(rawValue: "ForBrandName"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ShowpopUp(notification:)), name: Notification.Name("ShowPopUp"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.ShowLoader(notification:)), name: Notification.Name("ShowLoader"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HideLoader(notification:)), name: Notification.Name("HideLoader"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAfterFirstLogin), name: NSNotification.Name(rawValue: "loginSuccessfull"), object: nil)
        
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup () {
        // insert redirect your url and client ID below
        let redirectURL = "burzz://spotify-login-callback" // put your redirect URL here
        let clientID = "4199654919064c03ac7d737c75c56d1b" // put your client ID here
        auth.redirectURL     = URL(string: redirectURL)
        auth.clientID        = "4199654919064c03ac7d737c75c56d1b"
        auth.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthPlaylistModifyPublicScope, SPTAuthPlaylistModifyPrivateScope]
        loginUrl = auth.spotifyWebAuthenticationURL()
        
    }
    
    @objc func updateAfterFirstLogin () {
        
        // loginButton.isHidden = true
        let userDefaults = UserDefaults.standard
        
        if let sessionObj:AnyObject = userDefaults.object(forKey: "SpotifySession") as AnyObject? {
            
            let sessionDataObj = sessionObj as! Data
            let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
            
            self.session = firstTimeSession
            initializaPlayer(authSession: session)
            //   self.loginButton.isHidden = true
            // self.loadingLabel.isHidden = false
            tableView.reloadData()
        }
        
    }
    
    func initializaPlayer(authSession:SPTSession){
        if self.player == nil {
            
            
            //            self.player = SPTAudioStreamingController.sharedInstance()
            //            self.player!.playbackDelegate = self
            //            self.player!.delegate = self
            //            try! player?.start(withClientId: auth.clientID)
            //            self.player!.login(withAccessToken: authSession.accessToken)
            
        }
        
    }
    
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        // after a user authenticates a session, the SPTAudioStreamingController is then initialized and this method called
        print("logged in")
        self.player?.playSpotifyURI("burzz://spotify-login-callback", startingWith: 0, startingWithPosition: 0, callback: { (error) in
            if (error != nil) {
                print("playing!")
            }
            
        })
        
    }
    
    @objc func ShowpopUp(notification:Notification)
    {
        let notifiData = notification.object as! Int
        
        if notifiData == 1
        {
            self.addAlertView("", message: "Sorry you can't delete your last photo. You're required to have a profile photo so people can see you", buttonTitle: "ok")
        }
        else if notifiData == 2
        {
            let myAlert = UIAlertController(title: "Delete this photo?", message:"Are you sure you want to delete this photo?", preferredStyle: UIAlertController.Style.alert);
            let okAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler:nil)
            
            let delete = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler:  { action in
                
                
            })
            
            // this action can add to more button
            myAlert.addAction(okAction);
            myAlert.addAction(delete);
            
            self.present(myAlert, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    @objc func ShowLoader(notification:Notification)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
    }
    
    
    @objc func HideLoader(notification:Notification)
    {
        MBProgressHUD.hide(for: self.view, animated: true)
        
    }
    
    
    @IBAction func onClickSaveUserDetail(_ sender: Any) {
        self.view.endEditing(true)
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        var gender = String()
        if selectGender == "Male"{
            gender = "1"
        }
        else{
            gender = "2"
        }
        let dict : [String:String] = ["id":(currentUser.result?.id)!,"about_me" :aboutme ,"job": jobName,"company":companyName,"gender":gender,"headline":headline]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url, dictData: dict )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ISVerified()
        tableView.reloadData()
        
        if currentUser.result?.interest == "3"
        {
            APiCalling()
        }
        else
        {
            tableView.isHidden = true
            self.btnSave.isHidden = true
        }
        
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 5{
            jobName = textField.text!
        }
        else{
            companyName = textField.text!
        }
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == 1{
            aboutme = textView.text
        }else{
            headline = textView.text
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.tag != 1{
            let cell = self.tableView.viewWithTag(59) as! UITableViewCell
            let label =  cell.viewWithTag(23) as! UILabel
            
            if range.location<=100{
                label.text = "\(range.location)/100"
                return true
            }else{
                return false
            }
        }
        
        return true
    }
    
    
    @IBAction func click_to_BuzzBizz(_ sender : Any)
    {
        self.setIntrestType()
    }
    
    @IBAction func click_to_back(_ sender : Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getImageSource(){
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func getBrandName(_ sender:Notification){
        
        currentInd = sender.object as? Int ?? 0
        
        let myAlert = UIAlertController(title: "Warning", message:"You are required to use a real photo with your face and nothing else.If we suspect you are an impostor, your  account will be permanently deleted and phone IP blocked.  ", preferredStyle: UIAlertController.Style.alert);
       
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
            self.getImageSource()
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    
    
    
    
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    
    
    func ptototickerContollerCall() {
        print("get call.....get call......get call")
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
    }
    func APiCalling()
    {
        let strURL =  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.kMasterAPI
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let dicURL : [String : String] = ["type":"3","user_id": currentUser.result!.id!]
        
        APIHelper.sharedInstance.requestPOSTWithouAUTHURL(strURL, params :dicURL,headers :nil, success: {
            (JSONResponse, resp) -> Void in
            
            // print(JSONResponse)
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responseDict = JSONResponse.dictionary
            let statusCode = responseDict!["status"]?.bool
            
            
            if(statusCode == false)
            {
                self.addAlertView(appName, message: "Something went wrong." , buttonTitle: ALERTS.kAlertOK)
            }
            
            self.arrEditInfo = resp["data"] as! NSArray
            print(self.arrEditInfo)
            
            
            
            if(statusCode == true)
            {
                
                let category = (responseDict!["data"]?.array)!
                
                self.CategoryArray = []
                for dic in category
                {
                    
                    if let title = dic["title"].string
                    {
                        self.CategoryArray.append(title)
                    }
                    
                }
                
                self.tableView.reloadData()
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        },failure:{
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
            
            
            DispatchQueue.main.async {
                
            }
        })
        
    }
    func CallAPIProfile(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        currentUser = User1.init(dictionary:JSON)
                                        self.tableView.reloadData()
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    func setIntrestType()
    {
        let strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        let dict : [String:Any] = [ "id": currentUser.result!.id!, "interest": "3"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPILogin(APIurl: strURL, dictData: dict as? [String : String])
    }
    
    
    func CallAPILogin(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        
                                        
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        self.btnSave.isHidden = false
                                        currentUser = User1.init(dictionary:JSON)
                                        
                                        if currentUser.result?.interest == "3"{
                                            self.tableView.isHidden = false
                                        }
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as? NSString as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
}



extension BusinessVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 16
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 1
        }
        else if section == 2
        {
            return 1
        }
        else if section == 3
        {
            return 1
        }
        else if section == 4
        {
            return 2
        }
        else if section == 5
        {
            return 1
        }
        else if section == 6
        {
            return 1
        }
        else if section == 7
        {
            return 1
        }
        else if section == 8
        {
            return 1
        }
        else if section == 9
        {
            return 1
        }
        else if section == 10
        {
            return CategoryArray.count
        }
        else if section == 11
        {
            return 0 //1 for instagram button
        }
        else if section == 12
        {
            return 0 //1 for instagram images
        }
        else if section == 13
        {
            return 1
        }
        else if section == 14
        {
            let  sessionData =  UserDefaults.standard.value(forKey: "SpotifySession")
            if sessionData != nil{
                return 0
            }else {
                return 1
            }
        }
        else
        {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell1", for: indexPath) as? tableEditInfoCell1
        cell?.arrImg = arrImg
        
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell2", for: indexPath) as? tableEditInfoCell2
        
        let cell3 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell3", for: indexPath) as? tableEditInfoCell3
        
        let cell4 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell4", for: indexPath) as? tableEditInfoCell4
        
        let cell5 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell5", for: indexPath) as? tableEditInfoCell5
        
        let cell6 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell6", for: indexPath) as? tableEditInfoCell6
        
        let cell7 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell7", for: indexPath) as? tableEditInfoCell7
        
        let cell8 = tableView.dequeueReusableCell(withIdentifier: "tableEditInfoCell8", for: indexPath) as? tableEditInfoCell8
        
        let arr = ["About Me", "Work and Education","Company","My basic info"]
        
        
        if indexPath.section == 0
        {
            
            cell?.collectionViewEdit.tag = indexPath.row + 100
            cell?.collectionViewEdit.reloadData()
            cell?.arrImg = arrImg
            cell?.collectionHeight.constant =  (cell?.collectionViewEdit.frame.size.width)!*0.70
            return cell!
        }
        else if indexPath.section == 1
        {
            /*let is_verified = UserDefaults.standard.value(forKey: "is_verified") as? String ?? "0"
            if is_verified == "0"{
                cell2?.isVerifiedView.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                cell2?.lblAccountVerify.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell2?.imgIcon.image = UIImage(named: "cross.png")
                cell2?.lblAccountVerify.text = "Your Account is not Verified!"
            }else{
                cell2?.isVerifiedView.backgroundColor = #colorLiteral(red: 0.9928157926, green: 0.932531178, blue: 0, alpha: 1)
                cell2?.lblAccountVerify.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell2?.imgIcon.image = UIImage(named: "check.png")
                cell2?.lblAccountVerify.text = "Account is Verified"
            }*/
            cell2?.isVerifiedView.backgroundColor = #colorLiteral(red: 0.9928157926, green: 0.932531178, blue: 0, alpha: 1)
            cell2?.lblAccountVerify.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell2?.imgIcon.image = UIImage(named: "check.png")
            cell2?.lblAccountVerify.text = "Account is Verified"
            return cell2!
            
        }
            
        else if indexPath.section == 2
        {
            return cell7!
        }
        else if indexPath.section == 3
        {
            cell8?.lblViewText.delegate = self
            
            cell8?.lblViewText.text = currentUser.result!.headline
            headline = currentUser.result!.headline
            
            return cell8!
            
            
        }
        else if indexPath.section == 4{
            if indexPath.row == 0{
                cell3?.lblTitle.text = arr[0]
                return cell3!
            }
            else{
                
                cell4!.lblTextView.delegate = self
                cell4!.lblTextView.text = currentUser.result!.about_me!
                aboutme = currentUser.result!.about_me!
                return cell4!
            }
            
        }
        else if indexPath.section == 5
        {
            cell3?.lblTitle.text = arr[1]
            return cell3!
        }
        else if indexPath.section == 6
        {
            
            cell5?.txtFieldtext.isUserInteractionEnabled = false
            let jobArray = currentUser.result?.job_data?.count
            
            if jobArray == 0
            {
                cell5!.txtFieldtext.text = "Add job"
                cell5!.txtFieldtext.textColor = UIColor.lightGray
            }
            else
            {
                cell5!.txtFieldtext.text = "\(jobArray ?? 0) job shown"
                cell5!.txtFieldtext.textColor = UIColor.black
            }
            
            return cell5!
        }
            //        else if indexPath.section == 6
            //        {
            //            cell3?.lblTitle.text = arr[2]
            //            return cell3!
            //
            //        }
        else if indexPath.section == 7
        {
            cell5?.txtFieldtext.isUserInteractionEnabled = false
            let eductionCount = currentUser.result?.education_data?.count
            if eductionCount == 0
            {
                cell5!.txtFieldtext.text = "Add Education"
                cell5!.txtFieldtext.textColor = UIColor.lightGray
            }
            else
            {
                cell5!.txtFieldtext.text = "\(eductionCount ?? 0) institutions shown"
                cell5!.txtFieldtext.textColor = UIColor.black
            }
            
            return cell5!
        }
        else if indexPath.section == 8
        {
            cell3?.lblTitle.text = arr[3]
            return cell3!
        }
        else if indexPath.section == 9
        {
            cell5!.txtFieldtext.text = selectGender
            cell5!.txtFieldtext.isUserInteractionEnabled = false
            return cell5!
        }
        else if indexPath.section == 10
        {
            cell6?.selectionStyle = .none
            cell6!.backView.backgroundColor = UIColor.white
            cell6?.lbltext.text = CategoryArray[indexPath.row]
            cell6?.lbltext.textColor = UIColor.init(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 1.0)
            cell6?.lbltext.font = UIFont.systemFont(ofSize: 15.0)
            let dicPreference = arrEditInfo[indexPath.row] as! NSDictionary
            
            let optionArray = dicPreference["options"] as! NSArray
            let preference = dicPreference["pref"] as! String
            
            for dic in optionArray
            {
                if (dic as! NSDictionary).value(forKey: "id") as! String == preference
                {
                    cell6?.lblPreference.text = (dic as! NSDictionary).value(forKey: "title") as? String
                }
            }
            
            let iconUrl = dicPreference["icon"] as? String
            cell6?.imageV.sd_setImage(with: URL(string: iconUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: UIImage(named :""), options: .refreshCached, completed: { (image, error, cacheType, imageURL) in
                // Perform operation.
                if error == nil{
                    cell6?.imageV.image = cell6?.imageV.image?.withRenderingMode(.alwaysTemplate)
                    cell6?.imageV.tintColor = UIColor (rgb: 0x6b6bff)
                }
                else{
                    // self.profileImageView!.image = UIImage(named: "profile_default")
                }
            })
            cell6?.imageV.isHidden = false
            cell6?.lblPreference.isHidden = false
            cell6?.backGroundImage.isHidden = true
            cell6?.nextImage.image = #imageLiteral(resourceName: "forward_arrow")
            cell6?.lbltext.reloadInputViews()
            return cell6!
        }
        else if indexPath.section == 11
        {
            
            cell6!.backView.backgroundColor = UIColor.clear
            /* let gradient = CAGradientLayer()
             gradient.frame = cell6!.backView.bounds
             gradient.colors = [UIColor.init(red: 064.0/255.0, green: 93.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 088.0/255.0, green: 81.0/255.0, blue: 219.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 131.0/255.0, green: 58.0/255.0, blue: 180.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 193.0/255.0, green: 53.0/255.0, blue: 132.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 225.0/255.0, green: 48.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 253.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 245.0/255.0, green: 96.0/255.0, blue: 64.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 247.0/255.0, green: 119.0/255.0, blue: 55.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 252.0/255.0, green:175.0/255.0, blue: 69.0/255.0, alpha: 1.0).cgColor,
             UIColor.init(red: 255.0/255.0, green: 220.0/255.0, blue: 128.0/255.0, alpha: 1.0).cgColor]
             gradient.startPoint = CGPoint(x:1.0, y: 0.5)
             gradient.endPoint = CGPoint(x: 0, y: 0.5)
             cell6?.selectionStyle = .none
             cell6!.backView.layer.insertSublayer(gradient, at: 0)*/
            cell6!.backView.layer.cornerRadius = 20
            cell6?.backView.clipsToBounds = true
            cell6?.lbltext.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell6?.lbltext.textColor = UIColor.white
            cell6?.backGroundImage.image = UIImage(named: "insta_btn")
            
            let InstaImages = currentUser.result?.insta_images
            if InstaImages?.count == 0 || InstaImages![0].insta_img_url! == ""{
                cell6?.lbltext.text = "Connect your instagram"
                cell6?.nextImage.image = #imageLiteral(resourceName: "white_plus")
            }else{
                cell6?.lbltext.text = "Connected to instagram"
                cell6?.nextImage.image = #imageLiteral(resourceName: "checkbox-pressed")
            }
            cell6?.imageV.image = #imageLiteral(resourceName: "instagram")
            cell6?.lblPreference.isHidden = true
            cell6?.backGroundImage.isHidden = false
            // cell6?.nextImage.image = #imageLiteral(resourceName: "white_plus")
            return cell6!
        }
        else if indexPath.section == 12
        {
            cell?.collectionViewEdit.tag = indexPath.row + 101
            cell?.collectionViewEdit.reloadData()
            cell?.collectionHeight.constant =  (cell?.collectionViewEdit.frame.size.width)!*0.70
            return cell!
            
        }
        else if indexPath.section == 13
        {
            cell6!.backView.backgroundColor = UIColor.init(red: 0.0/255.0, green: 182.0/255.0, blue: 94.0/255.0, alpha: 1.0)
            cell6?.lbltext.font = UIFont.boldSystemFont(ofSize: 15.0)
            cell6?.lbltext.textColor = UIColor.white
            cell6?.imageV.image = #imageLiteral(resourceName: "spotify")
            cell6?.lblPreference.isHidden = true
            cell6?.backGroundImage.isHidden = true
            
            let spotifyData = UserDefaults.standard.value(forKey: "SpotifySession")
            if spotifyData != nil{
                cell6?.lbltext.text = "Connected to spotify"
                cell6?.nextImage.image = #imageLiteral(resourceName: "success")
            }else{
                cell6?.lbltext.text = "Connect your spotify"
                cell6?.nextImage.image = #imageLiteral(resourceName: "white_plus")
            }
            return cell6!
        }
        else if indexPath.section == 14
        {
            cell?.collectionViewEdit.tag = indexPath.row
            cell?.collectionViewEdit.reloadData()
            cell?.collectionHeight.constant =  (cell?.collectionViewEdit.frame.size.width)!*0.35
            return cell!
        }
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditInfoQuestionVC") as! EditInfoQuestionVC
        
        
        if indexPath.section == 6
        {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MyWork_EducationVC") as! MyWork_EducationVC
            newViewController.jobOrEducation = "job"
            newViewController.refreshData = self
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
        
        if indexPath.section == 7
        {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "MyWork_EducationVC") as! MyWork_EducationVC
            newViewController.jobOrEducation = "education"
            newViewController.refreshData = self
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
        
        if indexPath.section == 9
        {
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                print("Cancel")
            }
            actionSheetControllerIOS8.addAction(cancelActionButton)
            
            let saveActionButton = UIAlertAction(title: "Male", style: .default)
            { _ in
                self.selectGender = "Male"
                self.tableView.reloadData()
            }
            actionSheetControllerIOS8.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "Female", style: .default)
            { _ in
                self.selectGender = "Female"
                self.tableView.reloadData()
                
            }
            actionSheetControllerIOS8.addAction(deleteActionButton)
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
        }
        
        
        if indexPath.section == 10
        {
            newViewController.arrAllQuestion = arrEditInfo as! [Any]
            newViewController.newDict  = arrEditInfo[indexPath.row] as! NSDictionary
            newViewController.arrAllQuestion = arrEditInfo as! [Any]
            newViewController.index = indexPath.row
            newViewController.type = "3"
            newViewController.apiCall = self
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
        if indexPath.section == 11 {
            
            
            
            let InstaImages = currentUser.result?.insta_images
            
            if InstaImages?.count == 0 || InstaImages![0].insta_img_url! == ""{
                
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }
            else{
                DispatchQueue.main.async {
                    let alert = UIAlertController.init(title: "Instagram", message: "Are you sure want to Disconnect from Instagram?", preferredStyle: .alert)
                    let yes = UIAlertAction.init(title: "Yes", style: .default) { (action) in
                        
                        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
                        let dict : [String:String] = ["id":(currentUser.result?.id)!,"insta_images" : "","instagram":""]
                        DispatchQueue.main.async {
                            MBProgressHUD.showAdded(to: self.view, animated: true)
                            self.CallAPIProfile(APIurl: url, dictData: dict )
                        }
                    }
                    let no = UIAlertAction.init(title: "No", style: .default) { (action) in
                        
                    }
                    alert.addAction(no)
                    alert.addAction(yes)
                    self.present(alert , animated: true)
                }
                
            }
        }
        
        if indexPath.section == 13
        {
            let sessionData =  UserDefaults.standard.value(forKey: "SpotifySession")
            
            if sessionData == nil{
                if UIApplication.shared.openURL(loginUrl!) {
                    if auth.canHandle(auth.redirectURL) {
                        // To do - build in error handling
                    }
                }
            }else{
                
                DispatchQueue.main.async {
                    let alert = UIAlertController.init(title: "Spotify", message: "Are you sure want to Disconnect from Spotify?", preferredStyle: .alert)
                    let yes = UIAlertAction.init(title: "Yes", style: .default) { (action) in
                        // call API
                        UserDefaults.standard.removeObject(forKey: "SpotifySession")
                        UserDefaults.standard.synchronize()
                        self.tableView.reloadData()
                    }
                    let no = UIAlertAction.init(title: "No", style: .default) { (action) in
                    }
                    alert.addAction(no)
                    alert.addAction(yes)
                    self.present(alert , animated: true)
                }
            }
        }
    }
    
    
    
}

extension BusinessVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func camera()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate;
        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPicController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        arrImg[currentInd] = image
        
        let ImageDate : [String:Any] = ["image": arrImg,"index": currentInd ]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: ImageDate, userInfo: nil)
        
        UserDefaults.standard.set(0, forKey: "rotetedBy")
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
}

extension BusinessVC: ApiCall{
    func pageDismiss() {
        
        if currentUser.result?.interest == "3"
        {
            APiCalling()
        }
        else
        {
            tableView.isHidden = true
            self.btnSave.isHidden = true
        }
    }
}


extension BusinessVC : refreshUserData{
    func pageRefresh() {
        
        if currentUser.result?.interest == "3"
        {
            APiCalling()
        }
        else
        {
            tableView.isHidden = true
            self.btnSave.isHidden = true
        }
    }
    
}


extension BusinessVC{
    func ISVerified()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.IS_VERIFIED)
        let dicURL : [String : String] = ["id": currentUser.result!.id!]
        self.ISVerifiedAPI(APIurl: url!, dictData: dicURL )
        
    }
    
    func ISVerifiedAPI(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        
                                        let data = dict["data"] as! NSDictionary
                                        
                                        let is_verified = data["is_verified"] as? String ?? "0"
                                        UserDefaults.standard.setValue(is_verified, forKey: "is_verified")
                                        
                                    }
                                    else{
                                        
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
}
