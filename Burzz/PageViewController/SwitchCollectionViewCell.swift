

//
//  SwitchCollectionViewCell.swift
//  Burzz
//
//  Created by mac on 15/10/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class SwitchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
}
