//
//  QLSharedPreference.swift
//  QAWEIL
//
//  Created by Sudeep  on 10/26/17.
//  Copyright © 2017 MAC MINI. All rights reserved.
//

import UIKit

class QLSharedPreference: NSObject {

    class var sharedInstance: QLSharedPreference {
        struct Static {
            static let instance = QLSharedPreference()
        }
        return Static.instance
    }
    
    let sharedPreference = UserDefaults.standard
    
    //MARK:- Clear All User Data.
    func clearAllPreferenceData()  {
        if let bundle = Bundle.main.bundleIdentifier {
           sharedPreference.removePersistentDomain(forName: bundle)
        }
    }
    
    //MARK:- Setter Methods
    func setLoginStatus(_ status: Bool)  {
        sharedPreference.set(status, forKey: "userLoggedIn")
    }
    func setUserName(_ status: String)  {
        sharedPreference.set(status, forKey: "name")
    }
    func setGender(_ status: String)  {
        sharedPreference.set(status, forKey: "gender")
    }
    func setDistance(_ status: Int)  {
        sharedPreference.set(status, forKey: "distance")
    }
    func setMinAge(_ status: Int)  {
        sharedPreference.set(status, forKey: "minAge")
    }
    func setMaxAge(_ status: Int)  {
        sharedPreference.set(status, forKey: "maxAge")
    }
    
    func saveUserId(_ userId: String) {
        sharedPreference.set(userId, forKey: "id")
    }
    func saveUserVerifed(_ userId: String) {
        sharedPreference.set(userId, forKey: "user_verified")
    }
    
    func saveUserData(_ data: NSDictionary) {
        let myData = NSKeyedArchiver.archivedData(withRootObject: data)
        sharedPreference.set(myData, forKey: "data")
    }
    
    func saveQuickBloxId(_ status: String) {
        sharedPreference.set(status, forKey: "quickBloxId")
    }
    
    func intrestType(_ status: String) {
        sharedPreference.set(status, forKey: "intrestType")
    }
   
    
    
    //MARK:- Getter Methods
    func getLoginStatus() -> Bool? {
        return sharedPreference.value(forKey: "userLoggedIn") as? Bool
    }
    
    func getUserId() -> String? {
        return sharedPreference.value(forKey: "id") as? String
    }
    
    func getGender() -> String? {
        return sharedPreference.value(forKey: "gender") as? String
    }
    
    func getDistance() -> Int? {
        return sharedPreference.value(forKey: "distance") as? Int
    }
    
    func getMinAge() -> Int? {
        return sharedPreference.value(forKey: "minAge") as? Int
    }
    
    func getMaxAge() -> Int? {
        return sharedPreference.value(forKey: "maxAge") as? Int
    }
    
    func getAllUserData() -> NSDictionary? {
        let recovedUserJsonData = UserDefaults.standard.object(forKey: "data")
        
        if recovedUserJsonData != nil{
             return NSKeyedUnarchiver.unarchiveObject(with: recovedUserJsonData as! Data) as? NSDictionary
        }
        return [:]
    }
    
    func getUserName() -> String? {
        return sharedPreference.value(forKey: "name") as? String
    }
    
    func getQuickBlox() -> String? {
        return sharedPreference.value(forKey: "quickBloxId") as? String
    }
    
    //intrestType
    
    func getIntrestType() -> String?{
        return sharedPreference.value(forKey: "intrestType") as? String
    }
  
}
