//
//  User.swift
//  Bricks
//
//  Created by Raul Menendez on 01/03/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import Foundation
public class User1 {
    
    public var isSuccess : Bool?
    public var message : String?
    public var result : Result?
    
    
    required public init?(dictionary: NSDictionary) {
        
        isSuccess = dictionary["status"] as? Bool
        message = dictionary["message"] as? String
        if (dictionary["data"] != nil) {
            if let data = dictionary["data"] as? NSDictionary{
                result = Result(dictionary: data)
                
            }
            
        }
    }
}
