//
//  Result.swift
//  Bricks
//
//  Created by Raul Menendez on 01/03/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import Foundation
public class Result {
    
    var c_code : String? = ""
    var dob : String? = ""
    var city_id : String? = ""
    var country_id : String? = ""
    var creation_time : String? = ""
    var dams_tokken : String? = ""
    var device_token : String? = ""
    var device_type : String? = ""
    var email : String? = ""
    var email_subscription : String? = ""
    var fb_id : String? = ""
    var first_name : String? = ""
    var g_id : String? = ""
    var gender : String? = ""
    var id : String? = ""
    var is_social : String? = ""
    var last_login : String? = ""
    var otp: Int? = 0
    var last_name : String? = ""
    var location : String? = ""
    var login_type : String? = ""
    var mobile : String? = ""
    var my_referal_code : String? = ""
    var name : String? = ""
    var otp_verify : String? = ""
    var password : String? = ""
    var post_count : String? = ""
    var profile_picture : String? = ""
    var reward_points : String? = ""
    var referal_code : String? = ""
    var social_tokken : String? = ""
    var social_type : String? = ""
    var state_id : String? = ""
    var status : String? = ""
    var username : String? = ""
    var interest_in : String? = ""
    var interest : String? = ""
    var about_me : String? = ""
    var company : String? = ""
    var job : String? = ""
    var images : Array<Images>?
    var insta_images : Array<Insta_images>?
    var max_age : String? = ""
    var min_age : String? = ""
    var distance : String = "1000200020"
    var quickblox_id : String = ""
    var headline : String = ""
    var notification : String = ""
//    var job_data : String = ""
//    var education_data : String = ""
    var job_data : Array<Any>?
    var education_data : Array<Any>?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let result_list = Result.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Result Instances.
     */
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Result]
    {
        var models:[Result] = []
        for item in array
        {
            models.append(Result(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let result = Result(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Result Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        c_code = dictionary["c_code"] as? String ?? ""
        city_id = dictionary["city_id"] as? String ?? ""
        country_id = dictionary["country_id"] as? String ?? ""
        creation_time = dictionary["creation_time"] as? String ?? ""
        dams_tokken = dictionary["dams_tokken"] as? String ?? ""
        device_token = dictionary["device_token"] as? String ?? ""
        device_type = dictionary["device_type"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        email_subscription = dictionary["email_subscription"] as? String ?? ""
        fb_id = dictionary["fb_id"] as? String ?? ""
        first_name = dictionary["first_name"] as? String ?? ""
        g_id = dictionary["g_id"] as? String ?? ""
        gender = dictionary["gender"] as? String ?? ""
        id = dictionary["id"] as? String ?? ""
        is_social = dictionary["is_social"] as? String ?? ""
        last_login = dictionary["last_login"] as? String ?? ""
        otp = dictionary["otp"] as? Int ?? 0
        last_name = dictionary["last_name"] as? String ?? ""
        location = dictionary["location"] as? String ?? ""
        notification = dictionary["notification"] as? String ?? ""
        login_type = dictionary["login_type"] as? String ?? ""
        mobile = dictionary["mobile"] as? String ?? ""
        name = dictionary["name"] as? String ?? ""
        otp_verify = dictionary["otp_verify"] as? String ?? ""
        password = dictionary["password"] as? String ?? ""
        post_count = dictionary["post_count"] as? String ?? ""
        profile_picture = dictionary["profile_picture"] as? String ?? ""
        reward_points = dictionary["reward_points"] as? String ?? ""
        social_tokken = dictionary["social_tokken"] as? String ?? ""
        social_type = dictionary["social_type"] as? String ?? ""
        state_id = dictionary["state_id"] as? String ?? ""
        status = dictionary["status"] as? String ?? ""
        username = dictionary["username"] as? String ?? ""
        interest_in = dictionary["interest_in"] as? String ?? ""
        interest = dictionary["interest"] as? String ?? ""
        dob = dictionary["dob"] as? String ?? ""
        about_me = dictionary["about_me"] as? String ?? ""
        company = dictionary["company"] as? String ?? ""
        job = dictionary["job"] as? String ?? ""
        max_age = dictionary["max_age"] as? String ?? ""
        min_age = dictionary["min_age"] as? String ?? ""
        distance = dictionary["distance"] as? String ?? ""
        quickblox_id = dictionary["quickblox_id"] as? String ?? ""
        headline = dictionary["headline"] as? String ?? ""
        job_data = dictionary["job_data"] as? Array<Any>
        education_data = dictionary["education_data"] as? Array<Any>
        my_referal_code = dictionary["my_referal_code"] as? String ?? ""
        referal_code = dictionary["referal_code"] as? String ?? ""

        
//        let job_dataArray = dictionary["job_data"] as! Array<Any>
//        print(job_dataArray)
//        job_data = String(job_dataArray.count)
//        
//        let eductionData_Array = dictionary["education_data"] as! Array<Any>
//        print(eductionData_Array)
//        education_data = String(eductionData_Array.count)

        
        if (dictionary["images"] != nil) { images = Images.modelsFromDictionaryArray(array: dictionary["images"] as! NSArray) }
        
        if (dictionary["insta_images"] != nil) { insta_images = Insta_images.modelsFromDictionaryArray(array: dictionary["insta_images"] as! NSArray) }
        
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.c_code, forKey: "c_code")
        dictionary.setValue(self.city_id, forKey: "city_id")
        dictionary.setValue(self.country_id, forKey: "country_id")
        dictionary.setValue(self.creation_time, forKey: "creation_time")
        dictionary.setValue(self.dams_tokken, forKey: "dams_tokken")
        dictionary.setValue(self.device_token, forKey: "device_token")
        dictionary.setValue(self.device_type, forKey: "device_type")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.email_subscription, forKey: "email_subscription")
        dictionary.setValue(self.fb_id, forKey: "fb_id")
        dictionary.setValue(self.g_id, forKey: "g_id")
        dictionary.setValue(self.first_name, forKey: "first_name")
        dictionary.setValue(self.gender, forKey: "gender")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.is_social, forKey: "is_social")
        dictionary.setValue(self.last_login, forKey: "last_login")
        dictionary.setValue(self.otp, forKey: "otp")
        dictionary.setValue(self.last_name, forKey: "last_name")
        dictionary.setValue(self.location, forKey: "location")
        dictionary.setValue(self.login_type, forKey: "login_type")
        dictionary.setValue(self.mobile, forKey: "mobile")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.otp_verify, forKey: "otp_verify")
        dictionary.setValue(self.password, forKey: "password")
        dictionary.setValue(self.post_count, forKey: "post_count")
        dictionary.setValue(self.profile_picture, forKey: "profile_picture")
        dictionary.setValue(self.reward_points, forKey: "reward_points")
        dictionary.setValue(self.social_tokken, forKey: "social_tokken")
        dictionary.setValue(self.social_type, forKey: "social_type")
        dictionary.setValue(self.state_id, forKey: "state_id")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.username, forKey: "username")
        dictionary.setValue(self.dob, forKey: "dob")
        dictionary.setValue(self.about_me, forKey: "about_me")
        dictionary.setValue(self.company, forKey: "company")
        dictionary.setValue(self.job, forKey: "job")
        dictionary.setValue(self.max_age, forKey: "max_age")
        dictionary.setValue(self.min_age, forKey: "min_age")
        dictionary.setValue(self.distance, forKey: "distance")
        dictionary.setValue(self.quickblox_id, forKey: "quickblox_id")
        dictionary.setValue(self.job_data, forKey: "job_data")
        dictionary.setValue(self.education_data, forKey: "education_data")
        dictionary.setValue(self.headline, forKey: "headline")
        dictionary.setValue(self.my_referal_code, forKey: "my_referal_code")
        dictionary.setValue(self.referal_code, forKey: "referal_code")
        
        return dictionary
    }
    
}

