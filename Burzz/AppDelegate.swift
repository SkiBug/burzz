//
//  AppDelegate.swift
//  Burzz
//
//  Created by Raul Menendez on 06/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import AWSS3
import Quickblox
import QuickbloxWebRTC
import SwiftyStoreKit
import Crashlytics
import Fabric
import GoogleMobileAds

var sharedSecret = "959c47c9070146c5892eaba6a785a007"

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate{
    
    var window: UIWindow?
    var auth = SPTAuth()
    
    struct CredentialsConstant {
        static let applicationID:UInt = 78030
        static let authKey = "af72FtfUNJAkyL9"
        static let authSecret = "B4xkMzhbc8MVLt7"
        static let accountKey = "7yvNe17TnjNUqDoPwfqp"
    }
    
    struct TimeIntervalConstant {
        static let answerTimeInterval: TimeInterval = 60.0
        static let dialingTimeInterval: TimeInterval = 5.0
    }
    
    
    struct AppDelegateConstant {
        static let enableStatsReports: UInt = 1
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Fabric.sharedSDK().debug = true
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
       let expiresDate =  UserDefaults.standard.value(forKey: "expiresDate") as? String ?? ""
        
        if expiresDate == "" {
            expiryDate()
        }

        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    self.expiryDate()
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                        print(purchase.transaction)

                    }
                    // Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enable = true
        registerForPushNotifications()
        
        QBSettings.applicationID = CredentialsConstant.applicationID;
        QBSettings.authKey = CredentialsConstant.authKey
        QBSettings.authSecret = CredentialsConstant.authSecret
        QBSettings.accountKey = CredentialsConstant.accountKey
        
        
        // Activate zone
        QBSettings.autoReconnectEnabled = true
        QBSettings.logLevel = QBLogLevel.debug
        QBSettings.enableXMPPLogging()
        QBRTCConfig.setAnswerTimeInterval(TimeIntervalConstant.answerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(TimeIntervalConstant.dialingTimeInterval)
        QBRTCConfig.setLogLevel(QBRTCLogLevel.verbose)
        
        if AppDelegateConstant.enableStatsReports == 1 {
            QBRTCConfig.setStatsReportTimeInterval(1.0)
        }
        
        
        QBRTCClient.initializeRTC()
        
        // loading settings
        
        
    let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USEast1, identityPoolId:"us-east-1:2ef0d6b7-e833-445d-a5b4-959c1a30d5c9")
           let configuration = AWSServiceConfiguration(region:.USEast1, credentialsProvider:credentialsProvider)
           AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        
        
        
        auth.redirectURL     = URL(string: "burzz://spotify-login-callback") // insert your redirect URL here
        auth.sessionUserDefaultsKey = "current session"
        

          let intrestType = QLSharedPreference.sharedInstance.getIntrestType()
            if intrestType == "0" || intrestType == ""{
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CreateProfileTableVC") as! CreateProfileTableVC
                let navigationController = UINavigationController(rootViewController: nextViewController)
                navigationController.navigationBar.isHidden = true
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window!.rootViewController = navigationController
                
            }else{
                
                let status = QLSharedPreference.sharedInstance.getLoginStatus()
                      if status ?? false {
                              //UIViewController().navigatToHomeScreen()
                              let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                              let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                              let navigationController = UINavigationController(rootViewController: nextViewController)
                              navigationController.navigationBar.isHidden = true
                              let appdelegate = UIApplication.shared.delegate as! AppDelegate
                              appdelegate.window!.rootViewController = navigationController
            }
                
        }
        
      
        
        return true
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        // called when user signs into spotify. Session data saved into user defaults, then notification posted to call updateAfterFirstLogin in ViewController.swift. Modeled off recommneded auth flow suggested by Spotify documentation
        
        if auth.canHandle(auth.redirectURL) {
            auth.handleAuthCallback(withTriggeredAuthURL: url, callback: { (error, session) in
                
                if error != nil {
                    print("error!")
                }
                
                
                
                let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                print(sessionData)
                
                
                UserDefaults.standard.set(sessionData, forKey: "SpotifySession")
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loginSuccessfull"), object: nil)
            })
            return true
        }
        
        return false
        
    }
    
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            // 1. Check if permission granted
            guard granted else { return }
            // 2. Attempt registration for remote notifications on the main thread
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        print("Device Token: \(token)")
        if token.isEmpty == true {
            //deviceTokan = "12345"
           // myUser_defaults.set("\(12345)", forKey:KdeviceToken)
            UserDefaults.standard.setValue("\(12345)", forKey: "newDeviceToken")
        }else {
           UserDefaults.standard.setValue(token, forKey: "newDeviceToken")
           // deviceTokan = deviceTokenString
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // 1. Print out error if PNs registration not successful
        print("Failed to register for remote notifications with error: \(error)")
        myUser_defaults.set("qwertyuiopasdfghjklzxcvbnm1234567890", forKey: KdeviceToken)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        //        let firebaseAuth = Auth.auth()
        //        if (firebaseAuth.canHandleNotification(userInfo)){
        //            print(userInfo)
        //            return
        //        }
        
        let state = UIApplication.shared.applicationState
                if state == .active {
                    let aps = userInfo[AnyHashable("aps")] as? NSDictionary
                  
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    nextViewController.notificationData = aps ?? ["String":"0"]
                    let navigationController = UINavigationController(rootViewController: nextViewController)
                    
                    navigationController.navigationBar.isHidden = true
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    appdelegate.window!.rootViewController = navigationController
                    // pushType = Int(pushtype)!
                    UIApplication.shared.applicationIconBadgeNumber = 0
                }
        print(userInfo)
        
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .alert, .sound])
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        let state = UIApplication.shared.applicationState
        
        if state == .active {
            let aps = userInfo[AnyHashable("aps")] as? NSDictionary
            print(aps)
            //            let json = aps!["json"]as! NSDictionary
            //            let pushMessage = json["message"]as! String
            //            let pushtype = String(describing:json["push_type"]!)
            // pushType = Int(pushtype)!
            UIApplication.shared.applicationIconBadgeNumber = 0
            // if (pushtype == "2" || pushtype == "4" || pushtype == "6")//&& openHomeScreen ==
        }
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
    }
  
    func expiryDate(){
           let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
           SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
               switch result {
               case .success(let receipt):

                guard let data = receipt as? NSDictionary else {return}
                guard let latestReceipt = data["receipt"] as? NSDictionary else {return}
                guard let in_app = latestReceipt["in_app"] as? NSArray else {return}

                guard let dict = in_app.lastObject as? NSDictionary else {return}
                guard let productId = dict["product_id"] as? String else {return}
                guard let duration = dict["purchase_date_ms"] as? String else {return}
 
                   UserDefaults.standard.setValue(duration, forKey: "expiresDate")
                   UserDefaults.standard.setValue(productId, forKey: "productId")

                   let purchaseResult = SwiftyStoreKit.verifyPurchase(
                       productId: productId,
                       inReceipt: receipt)

                   switch purchaseResult {
                   case .purchased(let receiptItem):
                       print("\(productId) is purchased: \(receiptItem)")
    
                   case .notPurchased:
                       print("The user has never purchased \(productId)")
                   }
               case .error(let error):
                   print("Receipt verification failed: \(error)")
               }
        
           }
       }
       

    
    
}

