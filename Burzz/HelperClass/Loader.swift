//
//  Loader.swift
//  Yacht Now 
//
//  Copyright © 2019 Yacht Now. All rights reserved.
//


import UIKit

class Loader: NSObject {

//    var load  = MBProgressHUD()
    func show(views: UIView)
    {
        let loadingNotification = MBProgressHUD.showAdded(to: views, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        
    }
    
    func hide(delegate:UIViewController) {
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: delegate.view, animated: true)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
       
    }
}
