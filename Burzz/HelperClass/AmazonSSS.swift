//
//  AmazonSSS.swift
//  DAMS
//
//  Created by Pintu on 6/21/17.
//  Copyright © 2017 Appsquadz. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AWSS3
//import AWSDynamoDB
//import AWSSQS
//import AWSSNS

protocol s3Protocol: class {
    func s3Failure()
}


class AmazonSSS: NSObject{
    weak var delegateS3: s3Protocol?

    class func configureSSSForDocumentUploading(dataDict: NSDictionary)->AWSS3TransferManagerUploadRequest{
        let fileManager = FileManager.default
        var path : String!
        var contentType : String!
        var extention : String!
        var fileUrl : NSURL!
        var fileSize : UInt64

        let uploadRequest = AWSS3TransferManagerUploadRequest()

        if dataDict["identifier"] as! String == "image" {
            path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("image.png")
            
            let imageData = (dataDict.value(forKey: "name") as! UIImage).jpegData(compressionQuality: 0.99)
            fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
            uploadRequest?.bucket = dataDict.value(forKey: "path") as? String
            
            do {
                let attr = try FileManager.default.attributesOfItem(atPath: path)
                fileSize = attr[FileAttributeKey.size] as! UInt64
                //if you convert to NSDictionary, you can get file size old way as well.
                let dict = attr as NSDictionary
                fileSize = dict.fileSize()
                uploadRequest?.contentLength = Int(fileSize) as NSNumber
            } catch {
                print("Error: \(error)")
            }
            fileUrl = NSURL(fileURLWithPath: path)
            contentType = "image/png"
            extention = "png"
        }
        else  if dataDict["identifier"] as! String == "video" {

            
            print(dataDict.value(forKey: "name")!)
            path = (dataDict.value(forKey: "name") as! URL).absoluteString
            
            let  videoData = path.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            fileManager.createFile(atPath: path as String, contents: videoData , attributes: nil)
            
            fileUrl = (dataDict.value(forKey: "name") as! NSURL) // NSURL(fileURLWithPath: path)
            uploadRequest?.bucket = dataDict.value(forKey: "path") as? String
            
            contentType = "video/mp4"
            extention = "mp4"
        }
//        else if dataDict.value(forKey: "type") as! String == "pdf"{
//             path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("document.pdf")
//            var imageData : NSData!
//            do {
//                 imageData = try NSData(contentsOf: dataDict.value(forKey: "name") as! URL, options: .mappedIfSafe)
//            } catch {
//                print(error)
//            }
//
////            let imageData = UIDocument(dataDict.value(forKey: "name") as! UIDocument, 0.99)
//            fileManager.createFile(atPath: path as String, contents: imageData! as Data, attributes: nil)
//            fileUrl = NSURL(fileURLWithPath: path)
//            uploadRequest?.bucket = dataDict.value(forKey: "path") as? String
//
//            contentType = "application/pdf"
//            extention = "pdf"
//        }
        else if dataDict.value(forKey: "type") as! String == "doc"{
            
            path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("document.doc")
            var imageData : NSData!
            do {
                imageData = try NSData(contentsOf: dataDict.value(forKey: "name") as! URL, options: .mappedIfSafe)
            } catch {
                print(error)
            }
            
            //            let imageData = UIDocument(dataDict.value(forKey: "name") as! UIDocument, 0.99)
        //    fileManager.createFile(atPath: path as String, contents: imageData! as Data, attributes: nil)
            
            fileUrl = NSURL(fileURLWithPath: path)
            uploadRequest?.bucket = dataDict.value(forKey: "path") as? String

            contentType = "application/doc"
            extention = "doc"
        }
        else if dataDict.value(forKey: "type") as! String == "xlsx"{
            
            path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("document.xlsx")
            var imageData : NSData!
            do {
                imageData = try NSData(contentsOf: dataDict.value(forKey: "name") as! URL, options: .mappedIfSafe)
            } catch {
                print(error)
            }
            
            //            let imageData = UIDocument(dataDict.value(forKey: "name") as! UIDocument, 0.99)
         //   fileManager.createFile(atPath: path as String, contents: imageData! as Data, attributes: nil)
            
            fileUrl = NSURL(fileURLWithPath: path)
            uploadRequest?.bucket = dataDict.value(forKey: "path") as? String

            contentType = "application/xlsx"
            extention = "xlsx"
        }
        
        else{
            fileUrl = NSURL(string : "")
            extention = ""
            NotificationCenter.default.post(name: Notification.Name("DucumentFormator"), object: nil)
        }
        
        if let keyExist = dataDict["fileName"] {
            uploadRequest?.key = keyExist as! String + "." + extention
        }
        else{
            uploadRequest?.key = ProcessInfo.processInfo.globallyUniqueString + "." + extention
        }
        uploadRequest?.contentType = contentType
        uploadRequest?.body = fileUrl as URL!
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
        }
        return uploadRequest!
    }
    
    class func uploadDocumentToS3Server(dataDict:NSDictionary, completionHandler: @escaping (_ result: Bool, _ response: URL?, _ error: NSError?, _ errorMessage: String?) -> Void) {
    
    let uploadRequest : AWSS3TransferManagerUploadRequest = AmazonSSS.configureSSSForDocumentUploading(dataDict: dataDict)
    let transferManager = AWSS3TransferManager.default()
    transferManager.upload(uploadRequest).continueWith(executor: AWSExecutor.mainThread(), block: { (taskk: AWSTask) -> Any? in
        if let error = taskk.error {
            print("Uploading failed ❌ (\(error))")
            completionHandler(false,nil, error as NSError,"response")
        }
        else if taskk.result != nil {
            let url = AWSS3.default().configuration.endpoint.url
            let publicURL = url?.appendingPathComponent((uploadRequest.bucket!)).appendingPathComponent((uploadRequest.key!))
            completionHandler(true,publicURL ,nil,"response")
            print("Uploaded to:\(String(describing: publicURL))")
        }
        return nil
    })
}
    

}
