//
//  Constants.swift
//  Burzz
//
//  Created by Raul Menendez on 21/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
let myUser_defaults = UserDefaults.standard
var currentUser     : User1!
let storyBoard = UIStoryboard(name: "Main", bundle: nil)
var currentResult     : Result!
let appName         : String = "Burzz"
let KdeviceToken    = "6a597b582078e637f5ae911b9234c9fc620544c4b568a1cf59a01b049d897e0f"
let appDelegate     = UIApplication.shared.delegate as! AppDelegate
struct CONTROLLERNAMES {
    
    static let KVIEWCONTROLLER      = "ViewController"
    static let KSIGNUPVC            = "SignUpVC"
    static let KHOMEVC              = "HomeVC"  //HomeVC"
    static let KMENUVC              = "QLSideMenuViewController"
    
}
struct ALERTS {
    static let kAlertOK            : String = "OK"
    static let kAlertCancel        : String = "Cancel"
    static let kAlertNo            : String = "NO"
    static let KMediaMessages      : String = "Media Messages"
    static let kAlertYes           : String = "YES"
    static let KAGREE              : String = "Agree"
    static let KDISAGREE           : String = "Disagree"
    static let KDone               : String = "Done"
    static let KERROR              : String = "ERROR"
    static let KCOMMENTPLACEHOLDEr : String = "Please write your Comment here..."
    static let KSUBMIT             : String = "Submit"
    static let KPlEASEENTERUSERNAME: String = "Please Enter UserName."
    
    static let KUnableToUpload : String = "Unable to upload."
    static let KPLEASEENTERGender : String  = "Please Enter Gender."
     static let KPLEASEENTERBirthday : String  = "Please Enter Birthday."
    static let KPLEASEReason : String  = "Please Enter reason."
    static let KPLEASEENTERLikes : String  = "Please Enter Likes Details."
    static let KPLEASEENTERDisLikes : String  = "Please Enter Dislikes Details."
    static let KPLEASEENTERIntrestedIn : String  = "Please Enter Interested."
    static let KMiminumBal : String = "Please Enter Valid Amount."
    static let KDiscountMax : String = "Discount Value Not More Than Total Amount."
    static let KPasswordConfirmPasswordNotMatch : String = "password and confirm password does not match."
    static let termTermCondition                : String = "Please read and accept terms and condition."
    static let kAlertEditCart               : String = "Do you want to edit cart ? "
    static let kPleaseSelectPayMode         : String = "Please Select Payment Mode."
    static let kNoInterNetConnection        : String = "Unable to connect to internet"
    static let kPleaseEnterName             : String = "Please enter name."
    static let kPleaseSelectSubject         : String = "Please select the subject."
    static let kPleaseEnterMobile           : String = "Please enter valid mobile number."
    static let KPleaseEnterVaildMbileNumber : String = "Please enter valid mobile number."
    static let kPleaseEnterPassword         : String = "Please enter your password."
    static let kPleaseEnterEmailID          : String = "Please enter email address."
    static let KPleaseEnterMobileNumber     : String = "Please enter your mobile number."
    static let KPleaseEnter6To15            : String = "Password should contains 6 to 15 digits."
    static let KPleaseEnterEmailORPassword  : String = "Please enter valid email or mobile number."
    static let KPleaseEnterVaildEmailID     : String = "Please enter valid email address."
    static let KPleaseEnterNewPassword      : String = "Please enter new password."
    static let KPleaseEnterConfirmPassword  : String = "Please enter confirm password."
    static let KPleaseEnterOTP              : String =  "Please enter valid OTP."
    static let KSomthingWentWorng           : String = "Something went wrong, please try after some time."
    static let KLoaderMessage               : String = "Please wait.."
    static let KLogoutMessgae               : String = "Are you sure to Log Out."
    static let KOTPDoesnotVArify            : String = "OTP does not verify."
    static let KPasswordResetSUccess        : String = "Password reset successful please login."
    static let KEmailIsNotRegistered        : String = "email is not found registered."
    static let KPLeaseEnterTitle            : String = "Please enter Title."
    static let KCountryCode                 : String = "Country code cannot be empty."
    static let KPleaseSelectaCategory       : String = "Please select a category."
    static let KPLeaseEnterBudget           : String = "Please enter your Budget."
    static let KPleaseSelectPickLocation    : String = "Please select pickup location."
    static let KPleaseSelectDropLocation    : String = "Please select drop off location."
    static let KPickAndDropSame             : String = "Pickup location and Dropoff Location cannot be same."
    static let KPleaseEnterSubject          : String = "Please enter Subject."
    static let KPleaseEnterMessage          : String = "Please enter Message."
    static let KSortByType                  : String = "Please Select a sort by type."
    static let KModelType                   : String = "Please select a model type."
    static let KPleaseAddPhoto              : String = "Please select profile image."
    static let KLogout                      : String = "Do you really want to log out?"
    static let DeleteMsg                      : String = "Do you really want to delete your account?"
    static let termCondition                : String = "A Terms and Conditions agreement, also known as a T&C or Terms of Use or Terms of Service, is the legal backbone of the relationship between your mobile app and your users."
    static let KNameCantBeEmpty              : String = "Name cannot be empty."
    static let KEmailCannotBeEmpty           : String = "email cannot be empty."
    static let KProfileUpdate                : String = "Profile update successfully."
    static let KDescriptionCantBeEmpty       : String = "Description field is mandatory."
    static let KPleaseEnteraVaildPrice       : String = "Please enter vaild price."
    static let KPriceRangeNotVaild           : String = "Price range not vaild."
    static let KStatus                       : String = "If status is stopped, your post will be no longer visible to others."
    static let KAlertMedia                   : String = "Please Select A Media."
    static let KENTERYOURQUERY               : String = "Please Enter Your Query."
    static let KDATENOTVAILD                 : String = "Please select a vaild date range."
    static let KNOINTERNETCONNECTION         : String = "No Internet Connection !"
    static let KNOINTERNETCONNECTIONTEXT     : String = "You are not connected to the internet,Make sure Wi-Fi is on,Airplane mode is off and try again."
    static let KSOMETHINGWENTWRONG           : String = "Something went wrong !"
    static let KSOMTHINGWENTWRONGTEXT        : String = "Brace yourself till we get the error fixed. You may also refresh the page or try again later."
    static let KREPORTPOST                   : String = "For report a post you must login/signUp first!"
    static let KWANNATOREPORT                : String = "Are you really want to report this post to us ?"
    static let KORDERCANCEL                : String = "Are you sure, you want to cancel this order ?"
    static let KENTERCOMMENT                 : String = "Comment field cannot be empty, please try again."
    static let kPLEASEENTEROTP               : String = "Pleae enter this otp."
    static let KPaymentThanks                : String = "Thank you for your order , we will contact with you soon."
    
    static let Idea                          : String = "Suggest an idea"
    static let Technical                     : String = "Report a technical issue"
    static let Question                      : String = "Ask your own question"
    static let Billing                       : String = "Billing Complaint"
}
extension UIViewController{
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func addAlertView(_ title: String, message: String, buttonTitle: String) {
        let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title:  NSLocalizedString(buttonTitle, comment: ""), style: UIAlertAction.Style.default, handler: nil))
        alert.setValue(getAttributtedString(NSLocalizedString(message, comment: "")), forKey: "attributedMessage")
        _ = UIAlertAction(title: NSLocalizedString(buttonTitle, comment: ""), style: .default, handler: nil)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true, completion: nil)
        })
    }
    func getAttributtedString(_ string: String) -> NSMutableAttributedString {
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: string as String, attributes: nil)
        // myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location:0,length: string.count))
        return myMutableString
    }


    
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}



