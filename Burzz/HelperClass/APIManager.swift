//
//  APIManager.swift
//  Burzz
//
//  Created by Raul Menendez on 22/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire

class APIManager: NSObject {
    class var sharedInstance : APIManager{
        struct Static {
            static let instance = APIManager()
        }
        return Static.instance
    }
    
 //"http://3.215.188.72/index.php/data_model/" live
    
//http://3.6.121.111/burzz/index.php/data_model/ dev
//    http://demos.mydevfactory.com/debarati/burzz/index.php/data_model/
    let S3BucketNameProfileImage = "burzz/"
    let KBASEURL           = "http://3.215.188.72/index.php/data_model/"
//    let KBASEURL           = "https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/"
    let KSIGNUP            = "user/registration/signup"
    let KSIGNIN            = "user/registration/login"
    let KForgot            = "user/registration/forget_password"
    let KUPLOADIMG         = "user/registration/save_images"
    let KUPLOADIMG1        = "user/registration/save_images_ios"
    let KUPDATEPROFILE     = "user/registration/update_profile"
    let KSUBSCRIPTION      = "plans/Plans/get_plan_list"
    let IS_VERIFIED        = "user/registration/is_verified"
    let VERIFY_OTP         = "user/registration/verifyOTP"
    let APPLEPAYMENT       = "plans/Plans/apple_payment"
    let KCONTACTUS         = "Static_pages/contact_us"
    let KHomePageAPI       = "fanwall/Fan_wall/get_fan_wall_for_user"
    let kMasterAPI         = "user/registration/get_masters"
    let KFAQ               = "Static_pages/get_faqs"
    let KTCPP              = "Static_pages/get_policy"
    let INFORM_SWIPE_LIMIT = "fanwall/Fan_wall/getSwapLimit"
    let IS_SWIPE_ALLOWED   = "fanwall/Fan_wall/nextSwapIsAllowOrNot"
    let FAQURL             = "http://3.215.188.72/faq.php"
    let DELETEACCOUNT      = "user/registration/user_deactivate"
    let BLOCKUSER          = "fanwall/Fan_wall/block_user"
    let GETEDUCATIONDETAILS = "user/registration/get_user_education_detail"
    let GETJOBDETAILS = "user/registration/get_user_job_detail"
    let PostEducationDetails = "user/registration/post_user_education_detail"
    let editUserEducationDetail = "user/registration/edit_user_education_detail"
    let deleteUserEducationDetail = "user/registration/delete_user_education_detail"
    let editUserJobDetail = "user/registration/edit_user_job_detail"
    let deleteUserJobDetail = "user/registration/delete_user_job_detail"
    
    let PostJobDetails = "user/registration/post_user_job_detail"
    let matches = "fanwall/Fan_wall/matches"
    let getFriendProfile = "user/registration/get_user_detail"
    let unMatch = "fanwall/Fan_wall/unmatch"
    let initializeTransaction = "plans/Plans/initialize_transaction"
    let completeTransaction = "plans/Plans/complete_transaction"
    let notInterestedInUser = "fanwall/Fan_wall/not_interested_in_user"
    let interestedInUser = "fanwall/Fan_wall/interested_in_user"
    let checkProUser = "fanwall/fan_wall/check_pro_user"
    let updateMatch = "fanwall/Fan_wall/update_match"
    let chatNotification  = "fanwall/fan_wall/chat_notification"
    
  
//    func requestPOSTWithHeader(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON, [String: AnyObject]) -> Void, failure:@escaping (Error) -> Void){
//
//
//        Alamofire.request(strURL, method: .post, parameters: params, headers: headers).responseJSON
//            {
//                (responseObject) -> Void in
//
//                print(responseObject)
//
//                if responseObject.result.isSuccess {
//                    let resJson = JSON(responseObject.result.value!)
//                    let resp = responseObject.result.value as! [String: AnyObject]
//                    success(resJson, resp)
//                }
//                if responseObject.result.isFailure {
//                    let error : Error = responseObject.result.error!
//                    failure(error)
//                }
//        }
//    }e
    
    
    
    func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl as! URLConvertible, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                //QLSharedPreference.sharedInstance.setLoginStatus(true)
                                //QLSharedPreference.sharedInstance.saveUserData(JSON)
                                //QLSharedPreference.sharedInstance.saveUserId(String(describing: result.value(forKey: "id")!))
//                                currentUser = User.init(dictionary:JSON)
//                                if currentUser.isSuccess!{
//                                    self.navigatToHomeScreen()
//                                }else{
//                                    self.addAlertView(appName, message: currentUser.message!, buttonTitle: ALERTS.kAlertOK)
//                                }
                            }else{
                                 let JSON = response.result.value as? NSDictionary
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("<#T##items: Any...##Any#>")
            }
        })
        }

    
}

