//
//  APIResponseParentModel.swift
//  MARITIME YACHT SERVICES
//
//  Created by Amstech on 4/23/19.
//  Copyright © 2019 OctalSoftware. All rights reserved.
//

import Foundation

class APIResponseParentModel: Codable {
    let statusCode: Int?
    let message: String?
    let data: DataClass?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case message, data
    }
    
    init(statusCode: Int?, message: String?, data: DataClass?) {
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}

class DataClass: Codable {
    
    init() {
    }
}
