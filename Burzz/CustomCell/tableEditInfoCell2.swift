//
//  tableEditInfoCell2.swift
//  Burzz
//
//  Created by Raul Menendez on 03/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableEditInfoCell2: UITableViewCell {

    @IBOutlet weak var viewCorner: UIView!
    
    @IBOutlet weak var isVerifiedView: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblAccountVerify: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCorner.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        viewCorner.layer.cornerRadius = viewCorner.frame.size.height*0.50
        viewCorner.layer.borderWidth = 2.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
