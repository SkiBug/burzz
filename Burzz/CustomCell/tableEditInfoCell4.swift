//
//  tableEditInfoCell4.swift
//  Burzz
//
//  Created by Raul Menendez on 03/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableEditInfoCell4: UITableViewCell {

    @IBOutlet weak var lblTextView: PlaceholderTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTextView.toolbarPlaceholder = "Write something about your self"
        lblTextView.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        
        //init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        lblTextView.layer.cornerRadius = 10
        lblTextView.layer.borderWidth = 2.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
