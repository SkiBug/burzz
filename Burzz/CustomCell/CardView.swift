//
//  CardView.swift
//  ZLSwipeableViewSwiftDemo
//
//  Created by Zhixuan Lai on 5/24/15.
//  Copyright (c) 2015 Zhixuan Lai. All rights reserved.
//

import UIKit

class CardView: UIView,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var tableView: UITableView!
    
    let arraystr = ["Pets","Kids","Often","Friends","Party","160 CM"] //"Exerice","Education level","Drinking","Smoking","Zodiac","Religion"
    var imageArray  = ["ScreenS1","ScreenS2","ScreenS3"]
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        
        // Shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        // Corner Radius
        layer.cornerRadius = 10.0
        
       // tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count + 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableHomeCell1", for: indexPath) as! tableHomeCell1
        if indexPath.row == 0{
            cell.lblNameAge.isHidden = false
            cell.lblDesigCompany.isHidden = false
            cell.imgCell.image =  UIImage(named: "\(imageArray[indexPath.row])")
            return cell
        }else if indexPath.row == 1{
            cell.lblNameAge.isHidden = true
            cell.lblDesigCompany.isHidden = true
            cell.imgCell.image = UIImage(named: "\(imageArray[indexPath.row])")
            return cell
        }else if indexPath.row == 2{
            cell.lblNameAge.isHidden = true
            cell.lblDesigCompany.isHidden = true
            cell.imgCell.image = UIImage(named: "\(imageArray[indexPath.row])")
            return cell
        }else if indexPath.row == 3{
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "tableHomeCell3", for: indexPath) as! tableHomeCell3
            return cell3
        }else if indexPath.row == 4{
            let cellColl2 = tableView.dequeueReusableCell(withIdentifier: "tableHomeCollecCell2", for: indexPath) as! tableHomeCollecCell2
            if cellColl2.collectionView.contentSize.height > 0{
                cellColl2.collectionHeight.constant =  cellColl2.collectionView.contentSize.height
            }else{
                cellColl2.collectionView.reloadData()
                cellColl2.collectionHeight.constant =  cellColl2.collectionView.contentSize.height
            }
            return cellColl2
        }else if indexPath.row == 5{
            let cell4 = tableView.dequeueReusableCell(withIdentifier: "tableHomeCell4", for: indexPath) as! tableHomeCell4
            return cell4
        }else{
            let cell5 = tableView.dequeueReusableCell(withIdentifier: "tableHomeCell5", for: indexPath) as! tableHomeCell5
            return cell5
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < imageArray.count{
            let str = imageArray[indexPath.row]
            let imageCurr =   UIImage(named: "\(str)")
            let calHeight = ((tableView.frame.size.width - 50)/(imageCurr?.size.width)!)*(imageCurr?.size.height)!
            print(calHeight)
            return calHeight
        }else{
            return UITableView.automaticDimension
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arraystr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collcell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionTableHomeCell", for: indexPath) as! collectionTableHomeCell
        collcell.lblText.text = "\(arraystr[indexPath.row])"
        return collcell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let strwidth = arraystr[indexPath.row]
        if strwidth.count > 5{
            return CGSize(width: 60 + strwidth.count*10 , height: 40)
        }else{
            return CGSize(width: 100, height: 40)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
