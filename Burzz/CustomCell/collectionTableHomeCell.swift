//
//  collectionTableHomeCell.swift
//  Burzz
//
//  Created by Raul Menendez on 10/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class collectionTableHomeCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var lblText: UILabel!
}
