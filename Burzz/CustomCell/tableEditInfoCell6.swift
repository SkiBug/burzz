//
//  tableEditInfoCell6.swift
//  Burzz
//
//  Created by Raul Menendez on 03/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableEditInfoCell6: UITableViewCell {
   
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var lbltext: UILabel!
    @IBOutlet weak var nextImage: UIImageView!
    @IBOutlet weak var lblPreference: UILabel!

    @IBOutlet weak var backGroundImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        backView.layer.cornerRadius = backView.frame.size.height*0.50
        backView.layer.borderWidth = 2.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
