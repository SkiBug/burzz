//
//  tableChatHomeCell1.swift
//  Burzz
//
//  Created by Raul Menendez on 09/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableChatHomeCell1: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
//    @IBOutlet weak var txtDesC: UILabel!
    @IBOutlet weak var txtTitleName: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
    @IBOutlet weak var lblLastSeenTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width / 2
        self.imgProfile.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
