//
//  tableHomeCell7.swift
//  Burzz
//
//  Created by Govind Singh on 26/09/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableHomeCell7: UITableViewCell {

    @IBOutlet weak var lblDegree: UILabel!
    @IBOutlet weak var lblCollage: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
