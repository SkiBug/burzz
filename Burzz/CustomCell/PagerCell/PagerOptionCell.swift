//
//  PagerOptionCell.swift
//  Burzz
//
//  Created by Raul Menendez on 08/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import FSPagerView
class PagerOptionCell: FSPagerViewCell {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnSwitch: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
  
    
}
