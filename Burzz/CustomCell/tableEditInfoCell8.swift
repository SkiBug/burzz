//
//  tableEditInfoCell8.swift
//  Burzz
//
//  Created by mac on 15/10/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableEditInfoCell8: UITableViewCell {
    
    
    @IBOutlet weak var lblViewText: PlaceholderTextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblViewText.toolbarPlaceholder = "Use this space to give a brief outline of who you are and why you are on burzzpro"
        lblViewText.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        
        //init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        lblViewText.layer.cornerRadius = 10
        lblViewText.layer.borderWidth = 2.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
