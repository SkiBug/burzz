//
//  SenderImageMessageCell.swift
//  Burzz
//
//  Created by MAC on 9/4/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class SenderImageMessageCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageSender: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
