//
//  tableHomeCell1.swift
//  Burzz
//
//  Created by Raul Menendez on 09/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class tableHomeCell1: UITableViewCell {
    
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblNameAge: UILabel!
    @IBOutlet weak var lblDesigCompany: UILabel!
    @IBOutlet weak var viewTransparent: UIView!
    
    @IBOutlet weak var lblEducation: UILabel!
    @IBOutlet weak var lblHeadline: UILabel!
    @IBOutlet weak var lblAboutUser: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
