//
//  collectionProfileCell.swift
//  Burzz
//
//  Created by Raul Menendez on 08/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class collectionProfileCell: UICollectionViewCell {
    
    @IBOutlet weak var addImageSign: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var deleteBtn: UIButton!
    
}
