//
//  CoinCell.swift
//  Burzz
//
//  Created by Pradipta Maitra on 14/07/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit

class CoinCell: UITableViewCell {

    @IBOutlet weak var labelCoinValue: UILabel!
    @IBOutlet weak var labelCoinPurchaseAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
