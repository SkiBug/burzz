//
//  SettingsVC.swift
//  Burzz
//
//  Created by Raul Menendez on 17/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import RangeSeekSlider
//import MBProgressHUD
import Alamofire
import GoogleMobileAds

class SettingsVC: UIViewController,RangeSeekSliderDelegate {
    
    @IBOutlet weak var bannerAdsView: GADBannerView!
    
    @IBOutlet weak var everyoneIV: UIImageView!
    @IBOutlet weak var femaleIV: UIImageView!
    @IBOutlet weak var maleIV: UIImageView!
    @IBOutlet weak var viewSubPlan: UIView!
    var gender = String()
    @IBOutlet weak var ageSliderView: UIView!
    @IBOutlet weak var viewInviteFriend: UIView!
    @IBOutlet weak var viewMaleFemaleH: NSLayoutConstraint!
    @IBOutlet weak var viewAgeRangeH: NSLayoutConstraint!
    @IBOutlet weak var viewInviteFriendH: NSLayoutConstraint!
    @IBOutlet weak var viewSubscriPlanH: NSLayoutConstraint!
    @IBOutlet weak var sliderAge: RangeSeekSlider!
    //  @IBOutlet weak var ageSliderView: UIView!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    @IBOutlet weak var sliderKM: UISlider!
    @IBOutlet weak var lblKMValue: UILabel!
    @IBOutlet weak var lblAgeMin: UILabel!
    @IBOutlet weak var lblAgeMax: UILabel!
    @IBOutlet weak var viewCoinPackageVC: UIView!
    @IBOutlet weak var buttonShareApp: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
        bannerAdsView.rootViewController = self
        bannerAdsView.load(GADRequest())
        
        
        btnDeleteAccount.layer.borderColor = UIColor.yellow.cgColor
        sliderAge.handleColor = UIColor.yellow
        sliderAge.delegate = self
        self.buttonShareApp.layer.cornerRadius = self.buttonShareApp.frame.size.height/2
        //        viewMaleFemaleH.constant = 0.0
        //        viewAgeRangeH.constant = 0
        //        ageSliderView.isHidden = true
        //        viewSubPlan.isHidden = true
        //        viewInviteFriend.isHidden = true
        //        viewInviteFriendH.constant = 0
        //        viewSubscriPlanH.constant = 0
        
        // Do any additional setup after loading the view.
        
        
        //viewSubscriPlanH
        
        //        let is_ios = (UserDefaults.standard.value(forKey: "is_ios") as? Int) ?? 1
        //
        //                      if is_ios == 1 {
        //                       viewSubscriPlanH.constant = 0
        //                        viewSubPlan.isHidden = true
        //                      }else{
        //                          viewSubscriPlanH.constant = 50
        //                          viewSubPlan.isHidden = false
        //                      }
        
        
        
        
        if let interest = currentUser.result!.interest_in{
            if interest == "1"{
                maleIV.image = #imageLiteral(resourceName: "radio_on")
                femaleIV.image = #imageLiteral(resourceName: "radio_off")
                everyoneIV.image = #imageLiteral(resourceName: "radio_off")
            }
            else if interest == "2"{
                maleIV.image = #imageLiteral(resourceName: "radio_off")
                femaleIV.image = #imageLiteral(resourceName: "radio_on")
                everyoneIV.image = #imageLiteral(resourceName: "radio_off")
            }
            else{
                maleIV.image = #imageLiteral(resourceName: "radio_off")
                femaleIV.image = #imageLiteral(resourceName: "radio_off")
                everyoneIV.image = #imageLiteral(resourceName: "radio_on")
            }
        }
        if let distance = QLSharedPreference.sharedInstance.getDistance() {
            sliderKM.value = Float(distance)
            lblKMValue.text = "\(distance)"
        }
        if let minAge = QLSharedPreference.sharedInstance.getMinAge() {
            lblAgeMin.text = "\((Int)(minAge))"
            sliderAge.selectedMinValue = CGFloat(minAge)
        }
        if let maxAge = QLSharedPreference.sharedInstance.getMaxAge() {
            sliderAge.selectedMaxValue = CGFloat(70)
            lblAgeMax.text = "\((Int)(maxAge))"
        }
        
        
    }
    
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapKMSlider(_ sender: Any) {
        let value = (Int)(sliderKM.value)
        lblKMValue.text = "\(value)"
    }
    
    @IBAction func didTapSubscriptionPlan(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapCoinPackages(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "CoinPackagesView") as! CoinPackagesView
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapWallet(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "WalletView") as! WalletView
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    @IBAction func didTapNotification(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapShareApp(_ sender: UIButton) {
        self.getReferelCode(sender)
    }
    
    func getReferelCode(_ sender: UIButton) {
        let settingsRepository = SettingsRepository()
        settingsRepository.getReferelCode(vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let data = response.data
                DispatchQueue.main.async {
                    guard let myReferralCode : String = data else { return }
                    let textToShare = "Use \(myReferralCode) as referral code to get 2 free coins"
                    
                       if let myWebsite = NSURL(string: "https://apps.apple.com/in/app/burzz-dating-networking/id1484960045") {
                           let objectsToShare: [Any] = [textToShare, myWebsite]
                           let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    
                        activityVC.popoverPresentationController?.sourceView = sender as UIView
                           self.present(activityVC, animated: true, completion: nil)
                       }
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func didTapAgeSlider(_ sender: Any) {
        
        lblAgeMin.text = "\((Int)(sliderAge.minValue))"
        lblAgeMax.text = "\((Int)(sliderAge.maxValue))"
        
        print(sliderAge.minValue)
        print(sliderAge.maxValue)
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        lblAgeMin.text = "\((Int)(minValue))"
        lblAgeMax.text = "\((Int)(maxValue))"
    }
    @IBAction func didTapSaveBtn(_ sender: Any) {
        APiCalling()
    }
    
    @IBAction func didTapLogOut(_ sender: Any) {
        //QLSharedPreference.sharedInstance.setLoginStatus(false)
        let actionSheet = UIAlertController(title: appName, message: ALERTS.KLogout, preferredStyle: UIAlertController.Style.alert)
        
        actionSheet.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            QLSharedPreference.sharedInstance.clearAllPreferenceData()
            UserDefaults.standard.removeObject(forKey: "SpotifySession")
            UserDefaults.standard.removeObject(forKey: "SwappingCount")
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.navigationBar.isHidden = true
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = navigationController
            
            //self.navigationController?.popToRootViewController(animated: true)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            //self.photoLibrary()
        }))
        
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func onClickDeleteAccount(_ sender: Any) {
        let actionSheet = UIAlertController(title: appName, message: ALERTS.DeleteMsg, preferredStyle: UIAlertController.Style.alert)
        
        actionSheet.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.APiCallingForDeleteAccount()
            
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            //self.photoLibrary()
        }))
        
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    func APiCalling()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)
        
        
        let dicURL : [String : String] = ["id":currentUser.result!.id! ,"interest_in":gender, "distance":lblKMValue.text!, "min_age":lblAgeMin.text!, "max_age":lblAgeMax.text!]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url!, dictData: dicURL )
        
    }
    func CallAPIProfile(APIurl: URL,dictData : [String:String]?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.setMinAge((Int)(self.lblAgeMin.text!)!)
                                        QLSharedPreference.sharedInstance.setMaxAge((Int)(self.lblAgeMax.text!)!)
                                        QLSharedPreference.sharedInstance.setDistance((Int)(self.sliderKM.value))
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        currentUser = User1.init(dictionary:JSON)
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        
    }
    @IBAction func inviteAFriend(_ sender: Any) {
        let text = "https://apps.apple.com/us/app/burzz-dating-network-app/id1484960045?ls=1"
        let activity = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        present(activity, animated: true, completion: nil)
        
    }
    @IBAction func onClickMaleFemale(_ sender: UIButton) {
        if sender.tag == 10{
            maleIV.image = #imageLiteral(resourceName: "radio_on")
            femaleIV.image = #imageLiteral(resourceName: "radio_off")
            everyoneIV.image = #imageLiteral(resourceName: "radio_off")
            gender = "1"
            
        }
        else if sender.tag == 20 {
            maleIV.image = #imageLiteral(resourceName: "radio_off")
            femaleIV.image = #imageLiteral(resourceName: "radio_on")
            everyoneIV.image = #imageLiteral(resourceName: "radio_off")
            gender = "2"
            
        }
        else{
            maleIV.image = #imageLiteral(resourceName: "radio_off")
            femaleIV.image = #imageLiteral(resourceName: "radio_off")
            everyoneIV.image = #imageLiteral(resourceName: "radio_on")
            gender = "3"
        }
    }
}


extension SettingsVC
{
    func APiCallingForDeleteAccount()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.DELETEACCOUNT)
        
        
        let dicURL : [String : String] = ["user_id":currentUser.result!.id!]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIforDeleteAccount(APIurl: url!, dictData: dicURL )
        
    }
    
    
    
    func CallAPIforDeleteAccount(APIurl: URL,dictData : [String:String]?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        
                                        QLSharedPreference.sharedInstance.clearAllPreferenceData()
                                        
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                        let navigationController = UINavigationController(rootViewController: nextViewController)
                                        navigationController.navigationBar.isHidden = true
                                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                        appdelegate.window!.rootViewController = navigationController
                                        
                                        
                                        
                                        
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
}
