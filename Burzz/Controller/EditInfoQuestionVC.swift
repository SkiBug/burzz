//
//  EditInfoQuestionVC.swift
//  Burzz
//
//  Created by Raul Menendez on 04/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
//import MBProgressHUD

protocol ApiCall {
    func pageDismiss()
}

class EditInfoQuestionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var questionDetail : NSDictionary?
    var newDict  = NSDictionary()
    var arrQuestion = NSArray()
    var arrAllQuestion = Array<Any>()
    var index = Int()
    var preference = String()
    var type = String()
    
    var apiCall : ApiCall?
    
    @IBOutlet weak var questionImage: UIImageView!
    var GetindexPath = Int()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtQuestion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtQuestion.text = String(describing: self.newDict["question"]!).htmlToString
        if type == "1"{
            self.txtQuestion.textColor = UIColor (rgb: 0xb06bc6)
        }
        else if type == "2"{
            self.txtQuestion.textColor = UIColor (rgb: 0xff6b6b)
        }
        else{
            self.txtQuestion.textColor = UIColor (rgb: 0x6b6bff)
        }
        
        arrQuestion = newDict["options"] as! NSArray
        self.questionImage.sd_setImage(with: URL(string: (newDict["icon"] as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: UIImage(named :""), options: .refreshCached, completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if error == nil{
                self.questionImage.image = self.questionImage.image?.withRenderingMode(.alwaysTemplate)
                if self.type == "1"{
                    self.questionImage.tintColor = UIColor (rgb: 0xb06bc6)
                }
                else if self.type == "2"{
                    self.questionImage.tintColor = UIColor (rgb: 0xff6b6b)
                }
                else{
                    self.questionImage.tintColor = UIColor (rgb: 0x6b6bff)
                }
            }
            else{
                // self.profileImageView!.image = UIImage(named: "profile_default")
            }
        })
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    @IBAction func didtapCancel(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
        
        apiCall?.pageDismiss()
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestion.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell", for: indexPath) as! questionCell
        
        if arrQuestion.count == indexPath.row
        {
            cell.lblQuestion.text = "Skip"
            cell.cornerView.layer.borderColor = UIColor.white.cgColor
        }
        else
        {
            let dic = arrQuestion[indexPath.row] as? NSDictionary
            cell.lblQuestion.text = dic?["title"] as? String
            cell.cornerView.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        }
        
        return cell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if arrQuestion.count == indexPath.row
        {
            apiCall?.pageDismiss()
            navigationController?.popViewController(animated: true)
           // self.dismiss(animated: true, completion: nil)
        }
        else
        {
            preference = newDict["field_name"] as! String
            let dic = arrQuestion[indexPath.row] as? NSDictionary
            let id = dic?["id"] as? String
            setIntrestType(preference: preference, id: id!,indexpath: indexPath.row)
        }
        
    }
    
  
    
    func setIntrestType(preference : String , id : String ,indexpath : Int)
    {
        let strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.kMasterAPI)")!
        var dict = NSDictionary()
        dict = [ "user_id": currentUser.result!.id!, "update_value": id, "update_key":preference,"type" : type]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPILogin(APIurl: strURL, dictData: dict,indexpath : indexpath )
    }
    
    
    func CallAPILogin(APIurl: URL,dictData : NSDictionary,indexpath : Int){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String )
                
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                       // let dict = (response.result.value as! NSDictionary)
                                        if self.arrQuestion.count == indexpath
                                        {
                                            
                                       
                                            self.apiCall?.pageDismiss()
                                            self.navigationController?.popViewController(animated: true)
                                            
                                            //self.dismiss(animated: true, completion: nil)
                                        }
                                        else
                                        {
                                            let dict = self.arrQuestion[indexpath] as! NSDictionary
                                            var dict1 = self.arrAllQuestion[self.index] as! NSDictionary
                                            var dict2 = NSMutableDictionary()
                                            dict2 = dict1.mutableCopy() as! NSMutableDictionary
                                            dict2.setValue(String(describing: dict["id"]!), forKey: "pref")
                                            dict1 = dict2.mutableCopy() as! NSDictionary
                                            self.arrAllQuestion[self.index] = dict1
                                            
                                            var tempValue = 0
                                            
                                            
                                            for (Index, dic) in self.arrAllQuestion.enumerated()
                                            {
                                                
                                                if  (dic as! NSDictionary).value(forKey: "pref") as! String == "0"
                                                {
                                                    tempValue = 1
                                                    self.index = Index
                                                    self.newDict = dic as! NSDictionary
                                                    self.txtQuestion.text = String(describing: self.newDict["question"]!).htmlToString
                                                    if self.type == "1"{
                                                        self.txtQuestion.textColor = UIColor (rgb: 0xb06bc6)
                                                    }
                                                    else if self.type == "2"{
                                                        self.txtQuestion.textColor = UIColor (rgb: 0xff6b6b)
                                                    }
                                                    else{
                                                        self.txtQuestion.textColor = UIColor (rgb: 0x6b6bff)
                                                    }
                                                    self.arrQuestion = []
                                                    self.arrQuestion = self.newDict["options"] as! NSArray
                                                    self.questionImage.sd_setImage(with: URL(string: (self.newDict["icon"] as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: UIImage(named :""), options: .refreshCached, completed: { (image, error, cacheType, imageURL) in
                                                        // Perform operation.
                                                        if error == nil{
                                                            self.questionImage.image = self.questionImage.image?.withRenderingMode(.alwaysTemplate)
                                                            if self.type == "1"{
                                                                self.questionImage.tintColor = UIColor (rgb: 0xb06bc6)
                                                            }
                                                            else if self.type == "2"{
                                                                self.questionImage.tintColor = UIColor (rgb: 0xff6b6b)
                                                            }
                                                            else{
                                                                self.questionImage.tintColor = UIColor (rgb: 0x6b6bff)
                                                            }
                                                        }
                                                        else{
                                                            // self.profileImageView!.image = UIImage(named: "profile_default")
                                                        }
                                                    })                                                    
                                                    
                                                }
                                            }
                                            if tempValue != 1
                                            {
                                                self.apiCall?.pageDismiss()
                                                self.navigationController?.popViewController(animated: true)
                                                //self.dismiss(animated: true, completion: nil)
                                            }
                                            
                                        }
                                        
                                        
                                        
                                        self.tableView.reloadData()

                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
    
    
    func APiCalling()
    {
        let strURL =  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.kMasterAPI
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let dicURL : [String : String] = [ "id": currentUser.result!.id!, "update_value":"", "update_key":"","type" : type]
        
        APIHelper.sharedInstance.requestPOSTWithouAUTHURL(strURL, params :dicURL,headers :nil, success: {
            (JSONResponse, resp) -> Void in
            
            // print(JSONResponse)
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responseDict = JSONResponse.dictionary
            let statusCode = responseDict!["status"]?.bool
            
            
            if(statusCode == false)
            {
                self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
            }
            self.arrAllQuestion = []
            self.arrAllQuestion = (resp["data"] as! NSArray) as! [Any]
            
            
            
            if(statusCode == true)
            {
                
                for dic in self.arrAllQuestion
                {
                    
                    if  (dic as! NSDictionary).value(forKey: "pref") as! String == "0"
                    {
                        
                        self.newDict = dic as! NSDictionary
                        self.questionImage.sd_setImage(with: URL(string: (self.newDict["icon"] as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: UIImage(named :""), options: .refreshCached, completed: { (image, error, cacheType, imageURL) in
                            // Perform operation.
                            if error == nil{
                                self.questionImage.image = self.questionImage.image?.withRenderingMode(.alwaysTemplate)
                                if self.type == "1"{
                                    self.questionImage.tintColor = UIColor (rgb: 0xb06bc6)
                                }
                                else if self.type == "2"{
                                    self.questionImage.tintColor = UIColor (rgb: 0xff6b6b)
                                }
                                else{
                                    self.questionImage.tintColor = UIColor (rgb: 0x6b6bff)
                                }
                            }
                            else{
                                // self.profileImageView!.image = UIImage(named: "profile_default")
                            }
                        })
                        self.txtQuestion.text = String(describing: self.newDict["question"]!).htmlToString
                        if self.type == "1"{
                            self.txtQuestion.textColor = UIColor (rgb: 0xb06bc6)
                        }
                        else if self.type == "2"{
                            self.txtQuestion.textColor = UIColor (rgb: 0xff6b6b)
                        }
                        else{
                            self.txtQuestion.textColor = UIColor (rgb: 0x6b6bff)
                        }
                        self.arrQuestion = []
                        self.arrQuestion = self.newDict["options"] as! NSArray
                        break
                        
                    }
                }
                
                self.tableView.reloadData()
               
                
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        },failure:{
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.addAlertView(appName, message: "Something went wrong." , buttonTitle: ALERTS.kAlertOK)
            
            
            DispatchQueue.main.async {
                
            }
        })
        
    }
    
    
    
}


class questionCell : UITableViewCell{
    
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var lblQuestion: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cornerView.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        cornerView.layer.cornerRadius = cornerView.frame.size.height*0.50
        cornerView.layer.borderWidth = 2.0
        lblQuestion.textColor = .darkGray
        
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
