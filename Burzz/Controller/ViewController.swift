//
//  ViewController.swift
//  Burzz
//
//  Created by Raul Menendez on 06/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import FSPagerView
//import MBProgressHUD
import Alamofire
import AuthenticationServices
import Fabric
import Crashlytics
import FBSDKLoginKit

class ViewController: UIViewController,FSPagerViewDataSource,FSPagerViewDelegate{
    
    
    let demoViewControllers = [("Should Swipe", ShouldSwipeDemoViewController.self)]
    @IBOutlet weak var gradientViewBack: UIView!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            self.pagerView.register(UINib(nibName: "PagerTextCell", bundle: nil), forCellWithReuseIdentifier: "PagerTextCell")
        }
    }
    
    @IBOutlet weak var Swip_headingLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        Fabric.sharedSDK().debug = true
         
        pageControl.numberOfPages = 3
        //onboarding gradient overlay(top :- #efaf45 mid:- #eae92f bottom :- #efaf45 with 45 degree angle)
        //let view = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [ UIColor.orange.cgColor,UIColor.yellow.cgColor, UIColor.orange.cgColor]
        gradient.startPoint = CGPoint(x: 1, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradientViewBack.layer.insertSublayer(gradient, at: 0)
        gradientViewBack.alpha = 0.5
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func onClickTermsConditions(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
        nextViewController.fromScreen = 4
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func didTapAppleSign_in(_ sender: UIButton) {
        
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            let alert = UIAlertController(title: "Message", message: "This feature is available in iOS 13 and Above", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    @available(iOS 13.0, *)
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
        
        // Create an authorization controller with the given requests.
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    
    @IBAction func didTapSignUp(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "loginSignUpVC") as! loginSignUpVC
        newviewController.loginPage = false
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    func titleForRowAtIndexPath() -> String {
        let (title, _) = demoViewControllers[0]
        return title
    }
    func viewControllerForRowAtIndexPath() -> ZLSwipeableViewController {
        let (_, vc) = demoViewControllers[0]
        return vc.init()
    }
    
    @IBAction func didTapLogin(_ sender: Any) {
        
        //        let title = titleForRowAtIndexPath()
        //        let vc = viewControllerForRowAtIndexPath()
        //        vc.title = title
        //        navigationController?.pushViewController(vc, animated: true)
        //  let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        // self.navigationController?.pushViewController(newviewController, animated: true)
        
        
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "loginSignUpVC") as! loginSignUpVC
        newviewController.loginPage = true
        self.navigationController?.pushViewController(newviewController, animated: true)
        //
        //        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "EditInfoVC") as! EditInfoVC
        //        self.navigationController?.pushViewController(newviewController, animated: true)
        
        
        
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 3
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "PagerTextCell", at: index) as! PagerTextCell
        
        if index == 0{
            Swip_headingLbl.text = "Matches near you."
        }
        else if index == 1{
            Swip_headingLbl.text = "Find your soul mate."
        }
        else{
            Swip_headingLbl.text = "Find your love."
        }
        
        
        cell.lblText.text = "We make it easy to navigate the online dating world by making it a comfortable and fun experience."
        return cell
    }
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int){
        pageControl.currentPage = targetIndex
        
    }
    
    
    @IBAction func didTapFacebookLogin(_ sender: Any) {
       
        let facebookLoginManager: LoginManager = LoginManager()
        facebookLoginManager.logIn(permissions: ["email", "public_profile"], from: self) { [unowned self] (loginManagerLoginResult, error) in
            if error != nil {
//                self.showAlertWithTitleOkAction("", message: error!.localizedDescription, okTitle: "OK", okAction: nil)
            } else if loginManagerLoginResult!.isCancelled {
                //self.showAlertWithTitleOkAction(Strings.blankString, message: "Login cancelled by the user", okTitle: Strings.ok, okAction: nil)
            } else {
                if(loginManagerLoginResult!.grantedPermissions.contains("email")) {
                    self.getUserDataFromFacebook()
                }
            }
            facebookLoginManager.logOut()
        }
        
    }

    func getUserDataFromFacebook() {
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    if let resultDictionary = result as? [String : AnyObject?] {
                        
                    }
                }
            })
        }
    }
    
    func CallAPI(APIurl: URL,dictData : [String:String]?,fbDict :NSDictionary){
        let dict : Parameters = dictData!
        Alamofire.request(APIurl, method: .post, parameters: dict, encoding: URLEncoding.default, headers: nil).responseJSON {  (response: DataResponse<Any>) in
            debugPrint(response)
            MBProgressHUD.hide(for: self.view, animated: true)
            if let responseData = response.response {
                if let JSON = response.result.value as? NSDictionary{
                    print(JSON)
                    if let statusData = JSON["status"] as? Int{
                        if statusData == 1{
                            QLSharedPreference.sharedInstance.setLoginStatus(true)
                            currentUser = User1.init(dictionary:JSON)
                            
                            let dict = (response.result.value as! NSDictionary)
                            
                            QLSharedPreference.sharedInstance.saveUserData(dict)
                            QLSharedPreference.sharedInstance.setMinAge((Int)(currentUser.result!.min_age!) ?? 18)
                            QLSharedPreference.sharedInstance.setMaxAge((Int)(currentUser.result!.max_age!) ?? 70 )
                            
                            QLSharedPreference.sharedInstance.setDistance((Int)(currentUser.result!.distance) ?? 1000)
                            
                            
                            
                            
                            var number = 0
                            if currentUser.result?.interest == "0"{
                                number = 5
                                let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateProfileTableVC") as! CreateProfileTableVC
                                if currentUser.result?.location == ""{
                                    number = 4
                                    if currentUser.result?.interest_in == "0"{
                                        number = 3
                                        if currentUser.result?.profile_picture != nil{
                                            number = 1
                                        }
                                    }
                                }
                                newviewController.screenNo = number
                                self.navigationController?.pushViewController(newviewController, animated: true)
                            }else{
                                let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                self.navigationController?.pushViewController(newviewController, animated: true)
                            }
                            
                        }else{
                            let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "loginSignUpVC") as! loginSignUpVC
                            newviewController.loginPage = false
                            newviewController.FBLogin = true
                            newviewController.name = fbDict.value(forKey: "name")! as! String
                            newviewController.socialID = fbDict.value(forKey: "id")! as! String
                            if (fbDict.value(forKey:"email") != nil){
                                newviewController.emailID = fbDict.value(forKey: "email")! as! String
                            }else{
                                newviewController.emailID =  ""
                            }
                            
                            self.navigationController?.pushViewController(newviewController, animated: true)
                        }
                    }
                }
            }
            
            
            
        }
        
    }
}
@available(iOS 13.0, *)
extension ViewController : ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Create an account in your system.
            // For the purpose of this demo app, store the these details in the keychain.
            //  KeychainItem.currentUserIdentifier = appleIDCredential.user
            //  KeychainItem.currentUserFirstName = appleIDCredential.fullName?.givenName
            //  KeychainItem.currentUserLastName = appleIDCredential.fullName?.familyName
            // KeychainItem.currentUserEmail = appleIDCredential.email
            
            print("User Id - \(appleIDCredential.user)")
            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
            print("User Email - \(appleIDCredential.email ?? "N/A")")
            print("Real User Status - \(appleIDCredential.realUserStatus.rawValue)")
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                print("Identity Token \(identityTokenString)")
            }
            
            //Show Home View Controller
            //  HomeViewController.Push()
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
@available(iOS 13.0, *)
extension ViewController : ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
