

//
//  UserProfileViewController.swift
//  Burzz
//
//  Created by mac on 23/09/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import MBProgressHUD
import SwiftyJSON
import SDWebImage
import SimpleImageViewer

class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var btnEdir: UIButton!
    @IBOutlet weak var tblProfile: UITableView!
    
    
    var responseFriendProfileData: JSON = []
    var arrImage = [String]()
    var arrName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //instagram Posts
        tblProfile.delegate = self
        tblProfile.dataSource = self
        
        let profileData = QLSharedPreference.sharedInstance.getAllUserData()
        
        self.responseFriendProfileData = JSON(profileData!)
        print(currentUser.result?.interest)
        
        print(self.responseFriendProfileData)
        self.arrImage.removeAll()
        self.arrName.removeAll()
        
        for collectionData in self.responseFriendProfileData["data"]["preference"].arrayValue{
            let text = collectionData["text"].stringValue
            
            if text != ""{
                self.arrName.append(text)
                self.arrImage.append(collectionData["icon"].stringValue)
            }
            
        }
        
        self.tblProfile.reloadData()
        
        viewTransparent.layer.cornerRadius = 10
        viewTransparent.clipsToBounds = true
        
        btnEdir.layer.cornerRadius = 25
        btnEdir.clipsToBounds = true
        
        //        let url =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.getFriendProfile)")!
        //        let dictData = ["id" : "\(currentUser.result!.id!)"]
        //        CallAPIForGot(APIurl: url, dictData: dictData)
        
        // Do any additional setup after loading the view.
        
    }
    
    func CallAPIForGot(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        MBProgressHUD.showAdded(to: view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            
                            let JsonData = JSON(response.result.value!)
                            self.responseFriendProfileData = JsonData
                            
                            print(self.responseFriendProfileData)
                            
                            self.arrImage.removeAll()
                            self.arrName.removeAll()
                            
                            for collectionData in self.responseFriendProfileData["data"]["preference"].arrayValue{
                                let text = collectionData["text"].stringValue
                                
                                if text != ""{
                                    self.arrName.append(text)
                                    self.arrImage.append(collectionData["icon"].stringValue)
                                }
                                
                            }
                            
                            self.tblProfile.reloadData()
                            print(JsonData)
                            MBProgressHUD.hide(for: self.view, animated: false)
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        
    }
    
    @IBAction func btnEditProfileTap(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editProfile"), object: nil, userInfo: nil)
        
        self.dismiss(animated: true, completion : nil)
    }
    
}

extension UserProfileViewController : UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return responseFriendProfileData["data"]["images"].count + 4 + responseFriendProfileData["data"]["education_data"].count + 1 + responseFriendProfileData["data"]["job_data"].count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let intrestType = Int(currentUser.result!.interest!)
        
        if intrestType == 3 || intrestType == 2 || intrestType == 1{
            
            if (indexPath.row == 0 && indexPath.row < responseFriendProfileData["data"]["images"].count) || (indexPath.row >= 3 && indexPath.row-2 < responseFriendProfileData["data"]["images"].count){
                return viewTransparent.bounds.height - 100
                
                //profile pic and other images cell height after (indexpath.row = 2)
                
            }
            else if indexPath.row == 1{
                
                return UITableView.automaticDimension
                //About me cell
                
            }
            else if indexPath.row == 2{
                
                return UITableView.automaticDimension
                
                // collection cell tags related about me data
                
            }
            else if indexPath.row == responseFriendProfileData["data"]["images"].count + 2{
                
                let instaImages = responseFriendProfileData["data"]["insta_images"].count
                
                if instaImages == 0 {
                    return 0
                    
                }else{
                    return 260
                }
                //instagram photos collection and cell
            }
                
            else if indexPath.row == responseFriendProfileData["data"]["images"].count + 3{
                
                let jobExp = responseFriendProfileData["data"]["job_data"].count
                
                if intrestType == 3{
                    
                    if jobExp == 0{
                        return 0
                    }else{
                        return  UITableView.automaticDimension
                    }
                }else{
                    return 0
                }
                //Experience  Headding Cell
                
            }
         //   let jobExp = responseFriendProfileData["data"]["job_data"].count
            
            else if indexPath.row <= responseFriendProfileData["data"]["images"].count + responseFriendProfileData["data"]["job_data"].count + 3{
                
                let jobExp = responseFriendProfileData["data"]["job_data"].count
                if intrestType == 3{
                    if jobExp == 0{
                        return 0
                    }else{
                        return  UITableView.automaticDimension
                    }
                }else{
                    return 0
                }
                //Experience  Data cell
                
            }
            else if indexPath.row == responseFriendProfileData["data"]["images"].count + 4 + responseFriendProfileData["data"]["job_data"].count{
                
                let education = responseFriendProfileData["data"]["education_data"].count
                if intrestType == 3{
                    if education == 0{
                        return 0
                    }
                    else{
                        return UITableView.automaticDimension
                    }
                }else{
                    return 0
                }
                //Education Heading cell
            }
            else if indexPath.row <= responseFriendProfileData["data"]["images"].count + 4 + responseFriendProfileData["data"]["job_data"].count + responseFriendProfileData["data"]["education_data"].count{
                
                let education = responseFriendProfileData["data"]["education_data"].count
                if intrestType == 3{
                    if education == 0{
                        return 0
                    }
                    else{
                        return UITableView.automaticDimension
                    }
                }else{
                    return 0
                }
                //Education Data cell
                
            }
            else{
                //location Data cell
                return UITableView.automaticDimension
            }
            
        }
        return 0
    }
    

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0 && indexPath.row < responseFriendProfileData["data"]["images"].count) || (indexPath.row >= 3 && indexPath.row-2 < responseFriendProfileData["data"]["images"].count){
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeCell1P", for: indexPath) as! tableHomeCell1
            print("indexPath---------\(indexPath.row)")
            
            let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
            cell.imgCell.addGestureRecognizer(tapGesture)
            
            if indexPath.row > 0{
                cell.viewTransparent.isHidden = true
            }else{
                cell.viewTransparent.isHidden = false
            }
            
            
            if String(describing:currentUser.result!.dob!) != ""{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let dobDate = dateFormatter.date(from:String(describing:currentUser.result!.dob!))!
                let date = NSDate()
                let year = (date as Date).years(from : dobDate as Date)
                print(year)
                cell.lblNameAge.text = (responseFriendProfileData["data"]["username"].stringValue + ", " + String(describing: year))
            }
            
            let jobData = responseFriendProfileData["data"]["job_data"].arrayValue
            
            
            if jobData.count > 0{
                let  lastJobData = jobData.last
                cell.lblDesigCompany.text = "\(lastJobData?["title"].stringValue ?? "") at \(lastJobData!["company"].stringValue)"
            }
            
            let educationData = responseFriendProfileData["data"]["education_data"].arrayValue
            
            if educationData.count > 0{
                let lasteducationData = educationData.last
                cell.lblEducation.text = lasteducationData!["institution"].stringValue
            }
            
            let intrestType = responseFriendProfileData["data"]["interest"].intValue
            
            if intrestType == 3{
                cell.lblHeadline.text = responseFriendProfileData["data"]["headline"].stringValue
                
            }else{
                cell.lblHeadline.text = ""
            }
            
            if indexPath.row == 0 {
                let imageUrl = responseFriendProfileData["data"]["images"][indexPath.row]["image_url"].stringValue
                let yourString = imageUrl
                let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
                cell.imgCell.sd_setImage(with: URL(string: URimgUrl1))
                
            }
            else{
                let imageUrl = responseFriendProfileData["data"]["images"][indexPath.row-2]["image_url"].stringValue
                let yourString = imageUrl
                let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
                cell.imgCell.sd_setImage(with: URL(string: URimgUrl1))
            }
            
            return cell
        }
        else if indexPath.row == 1{
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeCell3P", for: indexPath) as! tableHomeCell3
            
            let aboutMe = responseFriendProfileData["data"]["about_me"].stringValue
            
            if aboutMe == ""{
                cell.aboutmeLabel.text = ""
                
            }else{
                cell.aboutmeLabel.text = responseFriendProfileData["data"]["about_me"].stringValue
            }
            return cell
            
        }
        else if indexPath.row == 2{
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeCollecCell2P", for: indexPath) as! tableHomeCollecCell2
            
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = responseFriendProfileData["data"]["images"].count + 2
            
            let height = cell.collectionView.contentSize.height
            cell.collectionView.reloadData()
            
            if arrName.count == 0 {
                cell.collectionHeight.constant =  0
            } else {
                cell.collectionHeight.constant =  height
            }
            
            return cell
            
        }
            
        else if indexPath.row == responseFriendProfileData["data"]["images"].count+2{
            
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "InstaImageTableViewCell", for: indexPath) as! InstaImageTableViewCell
            
            cell.collectionInstaImages.tag = 80
            cell.collectionInstaImages.delegate = self
            cell.collectionInstaImages.dataSource = self
            cell.collectionInstaImages.reloadData()
            cell.layer.cornerRadius = 3
            cell.clipsToBounds = true
            cell.layer.shadowOpacity = 0.1
            cell.layer.shadowRadius = 2.0
            cell.layer.masksToBounds = false
            cell.layer.shadowOffset = CGSize.zero
            
            return cell
            
        }
        else if indexPath.row == responseFriendProfileData["data"]["images"].count + 3{
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeJobCell", for: indexPath) as! tableHomeJobCell
            
            let userName = responseFriendProfileData["data"]["username"].stringValue
            cell.lblExperance.text = "\(userName)'s Experience"
            
            return cell
            
        }
        else if indexPath.row < responseFriendProfileData["data"]["images"].count + 4 + responseFriendProfileData["data"]["job_data"].count{
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeCell6P", for: indexPath) as! tableHomeCell6
            
            let jobArray = responseFriendProfileData["data"]["job_data"].array
            let newJobData = jobArray?.reversed()
            
            cell.lblProfile.text = responseFriendProfileData["data"]["job_data"][indexPath.row - (responseFriendProfileData["data"]["images"].count + 4)]["title"].stringValue
            
            cell.lblCompany.text = responseFriendProfileData["data"]["job_data"][indexPath.row - (responseFriendProfileData["data"]["images"].count + 4)]["company"].stringValue
            
            cell.lblDate.text = responseFriendProfileData["data"]["job_data"][indexPath.row - (responseFriendProfileData["data"]["images"].count + 4)]["duration"].stringValue
            return cell
            
        }
        else if indexPath.row == responseFriendProfileData["data"]["images"].count + 4 + responseFriendProfileData["data"]["job_data"].count{
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "TableHomeEducationCell", for: indexPath) as! TableHomeEducationCell
            let userName = responseFriendProfileData["data"]["username"].stringValue
            cell.lblExparence.text = "\(userName)'s Education"
            
            return cell
            
        }
        else if indexPath.row <= responseFriendProfileData["data"]["images"].count + 4 + responseFriendProfileData["data"]["job_data"].count + responseFriendProfileData["data"]["education_data"].count{
            
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeCell7P", for: indexPath) as! tableHomeCell7
            
            cell.lblDegree.text = responseFriendProfileData["data"]["education_data"][indexPath.row - (responseFriendProfileData["data"]["images"].count + 3 + responseFriendProfileData["data"]["job_data"].count + 2)]["study"].stringValue
            
            cell.lblCollage.text = responseFriendProfileData["data"]["education_data"][indexPath.row - (responseFriendProfileData["data"]["images"].count + 3 + responseFriendProfileData["data"]["job_data"].count + 2)]["institution"].stringValue
            
            cell.lblData.text = responseFriendProfileData["data"]["education_data"][indexPath.row - (responseFriendProfileData["data"]["images"].count + 3 + responseFriendProfileData["data"]["job_data"].count + 2)]["year"].stringValue
            
            return cell
            
        }
            //else if indexPath.row == responseFriendProfileData["data"]["images"].count + 7
        else {
            let cell = tblProfile.dequeueReusableCell(withIdentifier: "tableHomeCell4P", for: indexPath) as! tableHomeCell4
            
            cell.locationTitleLAbel.text = "\(responseFriendProfileData["data"]["username"].stringValue)'s Location"
            let location = responseFriendProfileData["data"]["location"].stringValue
            if location == ""{
                cell.kmCityLbl.text = "Not Available"
                cell.kmCityLbl.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
               
            }else{
                cell.kmCityLbl.text = location
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == responseFriendProfileData["data"]["images"].count{
            
            return responseFriendProfileData["data"]["insta_images"].count
        }
        else if collectionView.tag == 80{
            return 6
        }
        else{
            return arrName.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == responseFriendProfileData["data"]["images"].count{
            
            let collcell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
            
            let instaImage = responseFriendProfileData["data"]["insta_images"][indexPath.row]["insta_img_url"].stringValue
            collcell1.imgColl.sd_setImage(with: URL(string:String(describing: instaImage)), placeholderImage: UIImage(named: "ScreenS1"))
            
            let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
            collcell1.imgColl.addGestureRecognizer(tapGesture)
            
            return collcell1
            
        }
        else if collectionView.tag == 80{
            let collcell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
            
            
            let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
            collcell1.imgColl.addGestureRecognizer(tapGesture)
            
            
            let instaImageArray = currentUser.result?.insta_images
            
            if instaImageArray?.count != 0 && instaImageArray![0].insta_img_url! != ""
            {
                collcell1.imgColl.sd_setImage(with: URL(string:instaImageArray![indexPath.row].insta_img_url!), placeholderImage: UIImage(named: "profile_one_default"))
            }
            
            return collcell1
        }
            
        else{
            let collcell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionTableHomeCellP", for: indexPath) as! collectionTableHomeCell
            collcell.imgIcon.sd_setImage(with: URL(string:String(describing: arrImage[indexPath.row])), placeholderImage: UIImage(named: "ScreenS1"))
            
            collcell.lblText.text = String(describing:arrName[indexPath.row])
            
            print(arrName.count)
            
            return collcell
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == responseFriendProfileData["data"]["images"].count{
            
            return CGSize(width: 100, height: 100)
            
        }
        else if collectionView.tag == 80{
            return CGSize(width: collectionView.frame.width/3-10, height: collectionView.frame.width/3-10)
        }
        else{
            let strwidth = String(describing:arrName[indexPath.row])
            if strwidth.count > 5{
                return CGSize(width: 60 + strwidth.count*10 , height: 40)
            }else{
                return CGSize(width: 100, height: 40)
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        let imgView = tapGesture.view as! UIImageView
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imgView
        }
        let imageViewerController = ImageViewerController(configuration: configuration)
        present(imageViewerController, animated: true)
    }
    
}
