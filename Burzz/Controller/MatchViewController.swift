

//
//  MatchViewController.swift
//  Burzz
//
//  Created by mac on 26/09/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftyJSON

class MatchViewController: UIViewController {
    
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var imgIntrestedImage: UIImageView!
    @IBOutlet weak var btnStartChart: UIButton!
    @IBOutlet weak var btnKeepSwiping: UIButton!
    
    var dictData = [String : JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        imgUserProfile.layer.borderWidth = 1.0
        imgUserProfile.layer.masksToBounds = false
        imgUserProfile.layer.borderColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.size.height/2
        imgUserProfile.clipsToBounds = true
        
        imgIntrestedImage.layer.borderWidth = 1.0
        imgIntrestedImage.layer.masksToBounds = false
        imgIntrestedImage.layer.borderColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        imgIntrestedImage.layer.cornerRadius = imgIntrestedImage.frame.size.height/2
        imgIntrestedImage.clipsToBounds = true
        
        btnStartChart.layer.cornerRadius = 20
        btnStartChart.clipsToBounds = true
        
        btnKeepSwiping.layer.borderWidth = 1.0
        btnKeepSwiping.layer.masksToBounds = false
        btnKeepSwiping.layer.borderColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        btnKeepSwiping.layer.cornerRadius = 20
        btnKeepSwiping.clipsToBounds = true
        
        print(dictData)
        
        let userProfileURL = currentUser.result?.profile_picture
        let URimgUrl1 = userProfileURL!.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
         imgUserProfile.sd_setImage(with: URL(string: URimgUrl1), placeholderImage: UIImage(named:"looking_for2"))
       // currentUser.result!.id!
        
        let imgIntrestedImage1 = dictData["profile_picture"]?.stringValue
        let URimgUrl = imgIntrestedImage1!.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
        imgIntrestedImage.sd_setImage(with: URL(string: URimgUrl), placeholderImage: UIImage(named:"looking_for2"))
        
    }
    
    @IBAction func btnStartChatTap(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatHomeVC") as! ChatHomeVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    
    @IBAction func btnKeepSwipeTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
