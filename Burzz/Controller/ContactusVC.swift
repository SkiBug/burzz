//
//  ContactusVC.swift
//  Burzz
//
//  Created by mac on 22/07/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
import SkyFloatingLabelTextField
//import MBProgressHUD
class ContactusVC: UIViewController {

    @IBOutlet weak var messageTF: SkyFloatingLabelTextField!
    @IBOutlet weak var subjectTF: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func onClickSubjectListing(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Subject", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: ALERTS.Idea, style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.subjectTF.text = ALERTS.Idea
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERTS.Technical, style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.subjectTF.text = ALERTS.Technical
        }))
        actionSheet.addAction(UIAlertAction(title: ALERTS.Question, style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.subjectTF.text = ALERTS.Question
        }))
        actionSheet.addAction(UIAlertAction(title: ALERTS.Billing, style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            
            self.subjectTF.text = ALERTS.Billing
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func onClickSubmit(_ sender: Any) {
        self.view.endEditing(true)
        if subjectTF.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.kPleaseSelectSubject, buttonTitle: ALERTS.kAlertOK)
        }else if messageTF.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.KPLEASEReason, buttonTitle: ALERTS.kAlertOK)
        }
        else{
            let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KCONTACTUS)
            
            
            let dicURL : [String : String] = ["user_id":currentUser.result!.id!,"subject":subjectTF.text!,"message":messageTF.text!]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CallAPISubscription(APIurl: url!, dictData: dicURL )
        }
        
       //self.navigationController?.popViewController(animated: true)
    }
    
    func CallAPISubscription(APIurl: URL,dictData : [String:String]?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                       self.subjectTF.text = ""
                                        self.messageTF.text = ""
                                        self.addAlertView(appName, message: "Subject submitted successfully" , buttonTitle: ALERTS.kAlertOK)
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    
    }
}
