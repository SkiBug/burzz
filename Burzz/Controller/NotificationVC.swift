//
//  NotificationVC.swift
//  Burzz
//
//  Created by Raul Menendez on 14/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire
import GoogleMobileAds

class NotificationVC: UIViewController {
    
    @IBOutlet weak var bannerAdsView: GADBannerView!
    
    @IBOutlet weak var viewNewMatch: UIView!
    @IBOutlet weak var viewNewmessage: UIView!
    @IBOutlet weak var viewExpring: UIView!
    @IBOutlet weak var switchNewMsg: UISwitch!
    @IBOutlet weak var switchNewMatch: UISwitch!
    @IBOutlet weak var switchExpireMatch: UISwitch!
    
    var newMsg = ""
    var newMatch = ""
    var expiringMatch = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
       bannerAdsView.rootViewController = self
       bannerAdsView.load(GADRequest())
        
        
        
        viewNewMatch.layer.borderColor = UIColor.init(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0).cgColor
        viewNewMatch.layer.cornerRadius = viewNewMatch.frame.size.height*0.50
        viewNewMatch.layer.borderWidth = 2.0
        
        viewNewmessage.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        viewNewmessage.layer.cornerRadius = viewNewmessage.frame.size.height*0.50
        viewNewmessage.layer.borderWidth = 2.0
        
        viewExpring.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        viewExpring.layer.cornerRadius = viewExpring.frame.size.height*0.50
        viewExpring.layer.borderWidth = 2.0
        
      
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if currentUser.result?.notification == "0"{
            switchNewMsg.isOn = true
            switchNewMatch.isOn = true
        }else{
            switchNewMsg.isOn = false
            switchNewMatch.isOn = false
        }
    }
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func newMesgActionSwitch(_ sender: UISwitch){
        if sender.isOn {
            newMsg = "0"
        }else{
            newMsg = "1"
        }
        Notification()
    }
    @IBAction func newMatchActionSwitch(_ sender: UISwitch){
        if sender.isOn {
            newMsg = "0"
        }else{
            newMsg = "1"
        }
        Notification()
    }
    @IBAction func expiringMatchActionSwitch(_ sender: UISwitch){
        if sender.isOn {
            expiringMatch = "0"
        }else{
            expiringMatch = "1"
        }
    }
    //  Notification()
}
extension NotificationVC{
    
    func Notification(){
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        
        
        let dict : [String:String] = ["id":(currentUser.result?.id)!, "notification":newMsg]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url, dictData: dict )
        
    }
    
    func CallAPIProfile(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        currentUser = User1.init(dictionary:JSON)
                                        self.viewWillAppear(true)
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        
    }
}
