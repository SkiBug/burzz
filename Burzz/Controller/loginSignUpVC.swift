//
//  loginSignUpVC.swift
//  Burzz
//
//  Created by Raul Menendez on 07/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import CountryPickerView
import SkyFloatingLabelTextField
//import MBProgressHUD
import Alamofire
import SwiftyJSON
class loginSignUpVC: UIViewController,CountryPickerViewDelegate,UITextFieldDelegate {
    @IBOutlet weak var countryCodePicker: CountryPickerView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var txtLoginUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLoginPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSignUpUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSignUpEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSignUpMobileNum: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSignUpPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var textFieldReferralCode: UITextField!
    
    var FBLogin: Bool = false
    var loginPage = false
    //var BtnCheck : Bool = false
    var name : String = ""
    var emailID : String = ""
    var socialID : String = ""
    var countryCode = "+234"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let token = UserDefaults.standard.value(forKey: "newDeviceToken")
//        self.addAlertView(appName, message: token as! String, buttonTitle: ALERTS.kAlertOK)
        
        countryCodePicker.delegate = self
        countryCodePicker.showCountryCodeInView = true
        countryCodePicker.showPhoneCodeInView = true
        countryCode = countryCodePicker.selectedCountry.phoneCode
        
        if loginPage{
            btnLogin.backgroundColor = UIColor.yellow
            btnSignUp.backgroundColor = UIColor.white
            loginView.isHidden = false
            signUpView.isHidden = true
        }else{
            txtSignUpEmail.text = emailID
            txtSignUpUsername.text = name
            btnSignUp.backgroundColor = UIColor.yellow
            btnLogin.backgroundColor = UIColor.white
            loginView.isHidden = true
            signUpView.isHidden = false
        }
    }
    @IBAction func onClickTermsCondition(_ sender: Any) {
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
        nextViewController.fromScreen = 4
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txtSignUpMobileNum.text != ""{
            if txtSignUpMobileNum.text?.first == "0"{
                txtSignUpMobileNum.text?.removeFirst()
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        
        if textField == txtSignUpMobileNum{
            
            if string == " "{
                return false
            }
            
            if txtSignUpMobileNum.text == nil || txtSignUpMobileNum.text == "" && string == "0"{
                return false
            }
            if txtSignUpMobileNum.text?.count == 16 && string != ""{
                return false
            }else{
                
                if txtSignUpMobileNum.text != ""{
                    if txtSignUpMobileNum.text?.first == "0"{
                        txtSignUpMobileNum.text?.removeFirst()
                    }
                }
                
                
                return true
            }
        }else{
            return true
        }
    }
    
    @IBAction func didTapSignUp(_ sender: Any) {
        btnSignUp.backgroundColor = UIColor.yellow
        btnLogin.backgroundColor = UIColor.white
        loginView.isHidden = true
        signUpView.isHidden = false
    }
    @IBAction func didTapLogin(_ sender: Any) {
        btnLogin.backgroundColor = UIColor.yellow
        btnSignUp.backgroundColor = UIColor.white
        loginView.isHidden = false
        signUpView.isHidden = true
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country.code)
        
        countryCode = country.phoneCode
        print(country)
    }
    
    
    
    
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapLoginSubmit(_ sender: Any) {
        if txtLoginUserName.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.KPlEASEENTERUSERNAME, buttonTitle: ALERTS.kAlertOK)
        }else if txtLoginPassword.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.kPleaseEnterPassword, buttonTitle: ALERTS.kAlertOK)
        }else{
            let deviceToken =  UserDefaults.standard.value(forKey: "newDeviceToken") ?? "1234"
            print(deviceToken)
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSIGNIN)")!
            let dict : [String:Any] = [ "username":txtLoginUserName.text!,"password":txtLoginPassword.text!,"device_type":"2","device_token":deviceToken]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CallAPILogin(APIurl: url, dictData: dict as? [String : String])
        }
    }
    
    @IBAction func didTapSignUpSubmit(_ sender: Any) {
        
        self.view.endEditing(true)
        var referral_code: String = ""
        if txtSignUpUsername.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.KPlEASEENTERUSERNAME, buttonTitle: ALERTS.kAlertOK)
        }else if txtSignUpEmail.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.kPleaseEnterEmailID, buttonTitle: ALERTS.kAlertOK)
        }else if !isValidEmail(testStr: txtSignUpEmail.text!){
            self.addAlertView(appName, message: ALERTS.KPleaseEnterVaildEmailID, buttonTitle: ALERTS.kAlertOK)
        }else if txtSignUpMobileNum.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.kPleaseEnterMobile, buttonTitle: ALERTS.kAlertOK)
        }else if (txtSignUpMobileNum.text!.count < 4) && (txtSignUpMobileNum.text!.count > 17){
            self.addAlertView(appName, message: ALERTS.kPleaseEnterMobile, buttonTitle: ALERTS.kAlertOK)
        }else if txtSignUpPassword.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.kPleaseEnterPassword, buttonTitle: ALERTS.kAlertOK)
        }else if ((txtSignUpPassword.text?.count)! < 5) && ((txtSignUpPassword.text?.count)! > 16){
            self.addAlertView(appName, message: ALERTS.KPleaseEnter6To15 , buttonTitle: ALERTS.kAlertOK)
        }
        else if btnCheck.isSelected == false{
            self.addAlertView(appName, message: ALERTS.termTermCondition , buttonTitle: ALERTS.kAlertOK)
        }
        else{
            var deviceToken =  myUser_defaults.value(forKey: KdeviceToken) as? String
            if deviceToken == nil{
                deviceToken = "12345"
            }
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSIGNUP)")!
            if textFieldReferralCode.text == "" {
                referral_code = "NONE"
            }else{
                referral_code = self.textFieldReferralCode.text ?? ""
            }
            var dict : [String:Any] = ["name":txtSignUpUsername.text!,"email":txtSignUpEmail.text! , "password":txtSignUpPassword.text!,"mobile":txtSignUpMobileNum.text!  ,"login_type" :"0","device_type":"2","device_token":deviceToken!,"otp":"","otp_verify":"0","username":txtSignUpUsername.text!, "c_code" : self.countryCode, "referal_code" : referral_code, "profile_picture" : ""]
            if FBLogin{
                dict = ["name":txtSignUpUsername.text!,"email":txtSignUpEmail.text! , "password":txtSignUpPassword.text!,"mobile":txtSignUpMobileNum.text!  ,"login_type" :"1","device_type":"2","device_token":deviceToken!,"otp":"","otp_verify":"0","social_tokken": socialID,"username":txtSignUpUsername.text!, "c_code" : self.countryCode, "profile_picture" : ""]
            }
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CallAPI(APIurl: url, dictData: dict as? [String : String])
        }
    }
    func CallAPI(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    
                                    let isFreeUser = JSON["is_ios"] as! Int
                                    
                                    UserDefaults.standard.setValue(isFreeUser, forKey: "is_ios")
                                    print(isFreeUser)
                                    
                                    if statusData == 1{
                                        let newDict = JSON["data"] as? NSDictionary
                                        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                                        var referralCode: String = ""
                                        if self.textFieldReferralCode.text == "" {
                                            referralCode = "NONE"
                                        }else{
                                            referralCode = self.textFieldReferralCode.text ?? ""
                                        }
                                        newviewController.dictData = ["name":self.txtSignUpUsername.text!,"email":self.txtSignUpEmail.text! , "password":self.txtSignUpPassword.text!,"mobile":self.txtSignUpMobileNum.text!,"login_type" :"0","device_type":"2","device_token":KdeviceToken,"otp":"\((newDict!["otp"] as! NSString) as String)","otp_verify":"1","username":self.txtSignUpUsername.text!, "c_code" : self.countryCode, "profile_picture" : "", "referal_code": referralCode]
                                        if self.FBLogin {
                                            newviewController.dictData = ["name":self.txtSignUpUsername.text!,"email":self.txtSignUpEmail.text! , "password":self.txtSignUpPassword.text!,"mobile":self.txtSignUpMobileNum.text!  ,"login_type" :"1","device_type":"2","device_token":KdeviceToken,"otp":"\((newDict!["otp"] as! NSString) as String)","otp_verify":"1","username":self.txtSignUpUsername.text!,"social_tokken": self.socialID, "c_code" : self.countryCode, "profile_picture" : ""]
                                        }
                                        newviewController.otpGet = Int((newDict!["otp"] as! NSString) as String)!
                                        self.navigationController?.pushViewController(newviewController, animated: true)
                                        //}
                                    }else{
                                        self.addAlertView(appName, message: (JSON["message"] as? String)!, buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                        }
                    }
                    else if response.result.error != nil {
                        self.addAlertView("Invalid mobile number", message: "Please enter valid mobile number" , buttonTitle: ALERTS.kAlertOK)
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
    func CallAPILogin(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    let isFreeUser = JSON["is_ios"] as? Int
                                    print(isFreeUser ?? 0)
                                    UserDefaults.standard.setValue(isFreeUser, forKey: "is_ios")
                                    
                                    if statusData == 1{
                                        let dict = (response.result.value as! NSDictionary)
                                        print(dict)
                                        let data = JSON["data"] as! NSDictionary
                                        
                                        let is_verified = data["is_verified"] as! String 
                                        UserDefaults.standard.setValue(is_verified, forKey: "is_verified")
                                        
                                        let id  = data["id"] as! String
                                        let userName = data["name"] as! String
                                        
                                        
                                        UserDefaults.standard.setValue(id, forKey: "user_id1")
                                        UserDefaults.standard.setValue(userName, forKey: "user_Name1")
                                        
                                        print(UserDefaults.standard.value(forKey: "user_id1") as Any)
                                        print(UserDefaults.standard.value(forKey: "user_Name1") as Any)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        QLSharedPreference.sharedInstance.setLoginStatus(true)
                                        
                                        currentUser = User1.init(dictionary:JSON)
                                        print(currentUser)
                                        
                                        QLSharedPreference.sharedInstance.setMinAge((Int)(currentUser.result!.min_age ?? "0") ?? 0)
                                        QLSharedPreference.sharedInstance.setMaxAge((Int)(currentUser.result!.max_age ?? "") ?? 0)
                                        QLSharedPreference.sharedInstance.setDistance((Int)(currentUser.result!.distance) ?? 100000000 )
                                        QLSharedPreference.sharedInstance.saveQuickBloxId((String)(currentUser.result!.quickblox_id))
                                        
                                        print(currentUser.result!.quickblox_id)
                                        
                                        var number = 0
                                        print(dict)
                                        if currentUser.result?.interest == "0"{
                                            number = 5
                                            let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateProfileTableVC") as! CreateProfileTableVC
                                            if currentUser.result?.location == ""{
                                                number = 4
                                                if currentUser.result?.interest_in == "0"{
                                                    number = 3
                                                    if currentUser.result?.profile_picture == ""{
                                                        number = 1
                                                    }
                                                }
                                            }
                                            newviewController.screenNo = number
                                            self.navigationController?.pushViewController(newviewController, animated: true)
                                        }else{
                                            let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                            self.navigationController?.pushViewController(newviewController, animated: true)
                                        }
                                        
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as? NSString as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
    @IBAction func didTapForgotPassword(_ sender: Any) {
        
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    
    @IBAction func didTapCheck(_ sender: Any) {
        
        if btnCheck.isSelected{
            //BtnCheck = true
            // checkBoxImg.image = #imageLiteral(resourceName: )
            // btnCheck.setImage(#imageLiteral(resourceName: "check_box_default"), for: .normal)
            btnCheck.isSelected = false
        }else{
            //BtnCheck = false
            // checkBoxImg.image = UIImage(named: "checkbox_on")
            //btnCheck.setImage(UIImage(named: "checkbox_on"), for: .normal)
            
            // btnCheck.setImage(#imageLiteral(resourceName: "checkbox_on"), for: .normal)
            btnCheck.isSelected = true
            // btnCheck.isSelected = true
        }
        
    }
    
}



