//
//  CoinPackagesRepository.swift
//  Burzz
//
//  Created by Pradipta Maitra on 06/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit

class CoinPackagesRepository: NSObject {
    
    var utility = Utility()
    
    func getCoinPackages(vc: UIViewController, completion: @escaping (CoinPackages, Bool, NSError?) -> Void) {
        let request = CoinPackagesRequest()
        request.getCoinPackages(vc: vc, hud: false, codableType: APIResponseParentModel.self) { (response, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(CoinPackages.self, from: response! as! Data)
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
