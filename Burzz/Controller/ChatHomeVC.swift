//
//  ChatHomeVC.swift
//  Burzz
//
//  Created by Raul Menendez on 09/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import SDWebImage
import Quickblox
import Alamofire
import SwiftyJSON
//import MBProgressHUD
import Firebase
import GoogleMobileAds


class ChatHomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var collectionChatHome: UICollectionView!
    @IBOutlet weak var tableChatHome: UITableView!
    @IBOutlet weak var bannerAdsView: GADBannerView!
    @IBOutlet weak var peopleLikedCollectionView: UICollectionView!
    
    @IBOutlet weak var noLikesFound: UILabel!
    
    @IBOutlet weak var collectPeopleLikeHeightConstrant: NSLayoutConstraint!
    var peopleLikedArr : [likedPeopleModel] = []
    
    
    let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.matches)")!
    
    let dict : [String:String] = ["user_id": currentUser.result!.id!]
    
    var userId = ""
    
    
    var responseData : JSON = []
    var referenceFriendList : DatabaseReference!
    var newFriendsArray : [String] = []
    var newMessageDict : [[String: AnyObject]] = []
    var arrMatchFriend : [[String: AnyObject]] = []
    
    var pro_User = 0
    
    var quickblox_id = ""
    var profile_picture = String()
    
    
    
    
    // MARK: - Handle errors
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
        bannerAdsView.rootViewController = self
        bannerAdsView.load(GADRequest())
        
        
        userId = currentUser.result!.id!
        tableChatHome.isHidden = false
        observeForFriends()
        
        burzzesSetup()
        
        
    }
    
    
    func burzzesSetup(){
        
        let ValidDate = Date()
        var serverDate = Date()
        var ValidDate2 = Calendar.current.date(byAdding: .day, value: -1, to: ValidDate)
        
        if UserDefaults.standard.value(forKey: "ValidDate") != nil{
            ValidDate2 =  UserDefaults.standard.value(forKey: "ValidDate") as? Date
            serverDate =  UserDefaults.standard.value(forKey: "serverDate") as! Date
        }
        
        if ValidDate2! >= serverDate{
            let url = URL(string: "http://3.215.188.72/index.php/data_model/fanwall/Fan_wall/my_profile_like_users")
            peopleLikeAPI(APIurl: url!,dictData : dict)
        }else{
            
            noLikesFound.isHidden = false
            peopleLikedCollectionView.isHidden = true
            self.collectPeopleLikeHeightConstrant.constant = 30
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        newMessageDict.removeAll()
        observerForLastMessage()
        
        self.CallAPIProfile2(APIurl: url, dictData: dict)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        
    }
    
    func observeForFriends()
    {
        Database.database().reference().child("friends").child("chat").child("\(userId)").observe(.childAdded, with: { (snapshot) in
            
            let chatUserIdArray = snapshot.key
            self.newFriendsArray.append(chatUserIdArray)
            //  as? [String: AnyObject]
            print(self.newFriendsArray)
            
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.observerForUser()
            self.observerForLastMessage()
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    
    func observerForUser(){
        
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            
            let dict = snapshot.value as? [String: AnyObject]
            
            for friendId in self.newFriendsArray{
                print(friendId)
                
                let friend_User_Id = dict?["user_id"] as? Int
                
                if Int(friendId) == friend_User_Id{
                    self.arrMatchFriend.append(dict!)
                    
                }
            }
        })
        
    }
    
    
    func observerForLastMessage(){
        
        for matchId in newFriendsArray{
            Database.database().reference().child("chat_module").child("social_private").child(userId).child(matchId).queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot) in
                // Get user value
                let dict = snapshot.value as? [String: AnyObject]
                
                
                self.newMessageDict.append(dict!)
                //                print(self.newMessageDict)
                //
                //                self.newMessageArray.append(message!)
                //                print(self.newMessageArray)
                self.tableChatHome.reloadData()
                
            })
                
            { (error) in
                print(error.localizedDescription)
            }
            
        }
    }
    
    func CallAPIProfile2(APIurl: URL,dictData : [String:String]?){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    //MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            let swiftyJsonVar = JSON(response.result.value!)
                            print(swiftyJsonVar)
                            
                            let status = swiftyJsonVar["status"].boolValue
                            
                            if status{
                                self.responseData = swiftyJsonVar
                                print(self.responseData)
                                self.pro_User = self.responseData["data"]["pro_user"].intValue
                                print(self.pro_User)
                                // For showing new matches
                                
                                if self.responseData["data"]["new"].isEmpty{
                                    self.collectionChatHome.isHidden = true
                                }else{
                                    self.collectionChatHome.isHidden = false
                                }
                                
                                self.tableChatHome.reloadData()
                                self.collectionChatHome.reloadData()
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                            }else{
                                self.collectionChatHome.isHidden = true
                                //  self.addAlertView(appName, message: swiftyJsonVar["message"].stringValue , buttonTitle: ALERTS.kAlertOK)
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        }
                    }
                    
                })
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
                print("1")
            }
        })
    }
    
    
    
    //Mark: Button Action
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return newArray.count
        
        return newMessageDict.count
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableChatHome.dequeueReusableCell(withIdentifier: "tableChatHomeCell1", for: indexPath) as! tableChatHomeCell1
        
        
        if arrMatchFriend.count-1 >= indexPath.row{
            let userInfo1 : [String: Any] = arrMatchFriend[indexPath.row] as Dictionary
            
            let profileImage =  userInfo1["profile_picture"] as? String
            let URimgUrl1 = profileImage!.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
            cell.imgProfile.sd_setImage(with: URL(string: URimgUrl1), placeholderImage: UIImage(named:"profile"))
            cell.txtTitleName.text = userInfo1["name"] as? String
            
        }
        
        // let timeStamp = userInfo1["is_online"] as? Int
        
        let dict = newMessageDict[indexPath.row]
        
        let timeStamp = dict["time"] as? Int
        
        
        if timeStamp == nil{
            
            cell.lblLastSeenTime.text = ""
            
        }else{
            
            let date = Date(timeIntervalSince1970: Double(timeStamp!/1000))
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
            dateFormatter.timeZone = .current
            let localDate : String = dateFormatter.string(from: date)
            let datenow = dateFormatter.date(from: localDate)
            let timeValue = (datenow)!.getElapsedInterval()
            
            cell.lblLastSeenTime.text = timeValue
        }
        
        //   let messageInfo = newMessageArray[indexPath.row] as String
        
        let messageData = newMessageDict[indexPath.row]
        cell.lblLastMessage.text = messageData["message"] as? String
        
        let seenStatus = messageData["seen"] as! Bool
        
        if seenStatus{
            cell.lblLastMessage.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }else{
            cell.lblLastMessage.textColor = #colorLiteral(red: 1, green: 0.7921568627, blue: 0.2745098039, alpha: 1)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // let is_ios = (UserDefaults.standard.value(forKey: "is_ios") as? Int) ?? 1
        let ValidDate = Date()
        var serverDate = Date()
        
        var ValidDate2 = Calendar.current.date(byAdding: .day, value: -1, to: ValidDate)
        
        if UserDefaults.standard.value(forKey: "ValidDate") != nil{
            ValidDate2 =  UserDefaults.standard.value(forKey: "ValidDate") as? Date
            serverDate =  UserDefaults.standard.value(forKey: "serverDate") as! Date
        }
        
        
        print("ValidDate----->  \(ValidDate2!)")
        print("serverDate----->  \(serverDate)")
        
        if ValidDate2! >= serverDate{
            let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            
            if arrMatchFriend.count == 0{
                return
            }
            
            let friend_Info : [String: Any] = arrMatchFriend[indexPath.row] as Dictionary
            VC.friend_Name = friend_Info["name"] as! String
            VC.friend_id = friend_Info["user_id"] as! Int
            VC.friend_quickblox_id = friend_Info["quickblox_id"] as! Int
            VC.profile_picture = friend_Info["profile_picture"] as! String
            VC.userType = "old"
            VC.responseMatch = JSON(arrMatchFriend[indexPath.row])
            self.navigationController?.pushViewController(VC, animated: true)
        }
        else{
            
            let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
            self.navigationController?.pushViewController(VC, animated: true)
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == peopleLikedCollectionView{
            return peopleLikedArr.count
        }
        return responseData["data"]["new"].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Collcell = collectionChatHome.dequeueReusableCell(withReuseIdentifier: "collectionChatHome1", for: indexPath) as! collectionChatHome1
        
        if collectionView == peopleLikedCollectionView{
            Collcell.imgMatchImage.layer.cornerRadius = Collcell.imgMatchImage.frame.size.width / 2
            Collcell.imgMatchImage.clipsToBounds = true
            
            let profileImage = peopleLikedArr[indexPath.row].profile_picture
            let yourString = profileImage
            let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
            Collcell.imgMatchImage.sd_setImage(with: URL(string: URimgUrl1), placeholderImage: UIImage(named:"profile"))
            
            return Collcell
        }
        
        Collcell.imgMatchImage.layer.cornerRadius = Collcell.imgMatchImage.frame.size.width / 2
        Collcell.imgMatchImage.clipsToBounds = true
        
        let profileImage = self.responseData["data"]["new"][indexPath.row]["profile_picture"].stringValue
        let yourString = profileImage
        let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
        Collcell.imgMatchImage.sd_setImage(with: URL(string: URimgUrl1), placeholderImage: UIImage(named:"profile"))
        
        return Collcell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == peopleLikedCollectionView{
            let VC1 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            VC1.friend_id = Int(peopleLikedArr[indexPath.row].user_id)!
            self.present(VC1, animated:true, completion: nil)
            
            return
        }
        
        
        let ValidDate = Date()
        var serverDate = Date()
        
        var ValidDate2 = Calendar.current.date(byAdding: .day, value: -1, to: ValidDate)
        
        if UserDefaults.standard.value(forKey: "ValidDate") != nil{
            ValidDate2 =  UserDefaults.standard.value(forKey: "ValidDate") as? Date
            serverDate =  UserDefaults.standard.value(forKey: "serverDate") as! Date
        }
        
        if ValidDate2! >= serverDate{
            let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            
            if responseData.count == 0{
                return
            }
            
            VC.friend_Name = responseData["data"]["new"][indexPath.row]["name"].stringValue
            VC.friend_id = responseData["data"]["new"][indexPath.row]["id"].intValue
            VC.friend_quickblox_id = responseData["data"]["new"][indexPath.row]["quickblox_id"].intValue
            VC.profile_picture = responseData["data"]["new"][indexPath.row]["profile_picture"].stringValue
            VC.userType = "new"
            VC.matchId = responseData["data"]["new"][indexPath.row]["match_id"].stringValue
            self.navigationController?.pushViewController(VC, animated: true)
            
        }
        else{
            
            let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
            self.navigationController?.pushViewController(VC, animated: true)
            
        }
        
        
        
    }
    
    
}


extension Date {
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute" :
                "\(minute)" + " " + "minutes ago"
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "second" :
                "\(second)" + " " + "seconds ago"
        } else {
            return "just now"
        }
        
    }
}

extension ChatHomeVC{
    
    //user_id
    // http://3.6.121.111/burzz/index.php/data_model/fanwall/Fan_wall/my_profile_like_users
    
    // let url = URL(string: "http://3.6.121.111/burzz/index.php/data_model/fanwall/Fan_wall/my_profile_like_users")
    
    func peopleLikeAPI(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        let data = dict["data"] as! Array<Dictionary<String,Any>>
                                        for i in 0..<data.count{
                                            self.peopleLikedArr.append(likedPeopleModel(dict: data[i]))
                                        }
                                        
                                        if self.peopleLikedArr.count != 0{
                                            self.collectPeopleLikeHeightConstrant.constant = 100
                                            self.view.updateConstraintsIfNeeded()
                                            self.peopleLikedCollectionView.updateConstraintsIfNeeded()
                                            self.peopleLikedCollectionView.delegate = self
                                            self.peopleLikedCollectionView.dataSource = self
                                            self.noLikesFound.isHidden = true
                                            self.peopleLikedCollectionView.reloadData()
                                            self.peopleLikedCollectionView.isHidden = false
                                            
                                        }
                                    }
                                    else{
                                        self.noLikesFound.isHidden = false
                                        self.collectPeopleLikeHeightConstrant.constant = 30
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
    
}


class likedPeopleModel: NSObject {
    
    var liked_users_name = ""
    var profile_picture = ""
    var email = ""
    var mobile = ""
    var creation_time = ""
    var user_id = "0"
    init(dict : Dictionary<String,Any>) {
        
        self.liked_users_name = "\(dict["liked_users_name"]!)"
        self.profile_picture = "\(dict["profile_picture"]!)"
        self.email = "\(dict["email"]!)"
        self.mobile = "\(dict["mobile"]!)"
        self.creation_time = "\(dict["creation_time"]!)"
        self.user_id = "\(dict["user_id"]!)"
        
    }
    
    
}



