//
//  SubscriptionVC.swift
//  Burzz
//
//  Created by Raul Menendez on 14/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire
import SwiftyStoreKit
import GoogleMobileAds


class SubscriptionVC: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    let bundleID = "com.appSquadz.Burzz"
    
    @IBOutlet weak var bannerAdsView: GADBannerView!
    
    var listArray = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
        bannerAdsView.rootViewController = self
        bannerAdsView.load(GADRequest())
        
        
        
        APiCalling()
        
    }
    
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func APiCalling()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSUBSCRIPTION)
        
        let dicURL : [String : String] = ["user_id": currentUser.result!.id!]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPISubscription(APIurl: url!, dictData: dicURL )
        
    }
    
    func CallAPISubscription(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        self.listArray = dict["data"] as! Array<Any>
                                        self.tableView.reloadData()
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
    func CallAPIPayment(APIurl: URL,dictData : [String:String]?){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
    
    
}


extension SubscriptionVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count-1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
        
        let categoryName : UILabel = cell.viewWithTag(1) as! UILabel
        let lbl : UILabel = cell.viewWithTag(7) as! UILabel
        
        let mainView : UIView = cell.viewWithTag(2)!
        
        categoryName.text = "Subscribe Now"
        
        let dict = listArray[indexPath.row] as! NSDictionary
        
        
        lbl.text = "\(String(describing: dict["text"]!))  \(String(describing: dict["price"]!))$/\(String(describing: dict["name"]!)) Non-Renewable"
        
        
        mainView.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        mainView.layer.cornerRadius = mainView.frame.size.height/2
        mainView.layer.borderWidth = 2.0
        return cell
    }
    
}
extension SubscriptionVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = listArray[indexPath.row] as! NSDictionary
        
        let ProductID =  "\(String(describing: dict["apple_product_id"]!))"
        
        purchase(ProductID, atomically: true)
        
    }
}

//**************************** In-AppPurchase code *********************
//**********************************************************************
extension SubscriptionVC{
    func getInfo(_ purchase: RegisteredPurchase) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.retrieveProductsInfo([bundleID + "." + purchase.rawValue]) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            self.showAlert(alert: self.alertForProductRetrievalInfo(result))
        }
    }
    
    func purchase(_ purchase: String, atomically: Bool) {
        
        self.view.isUserInteractionEnabled = false
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(purchase, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.view.isUserInteractionEnabled = true
            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                let productId = purchase.productId
                let transaction_id = purchase.transaction.transactionIdentifier
                
                self.expiryDate()
                
                let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.APPLEPAYMENT)
                
                let dicURL : [String : String] = ["user_id": currentUser.result!.id!,"product_id":productId,"post_transaction_id":"\(transaction_id!)"]
                
                self.CallAPIPayment(APIurl: url!, dictData: dicURL)
                
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                    
                }
            }
            if let alert = self.alertForPurchaseResult(result) {
                self.showAlert(alert: alert)
                
            }
        }
    }
    
    func restorePurchases() {
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            for purchase in results.restoredPurchases {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                } else if purchase.needsFinishTransaction {
                    // Deliver content from server, then:
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
            }
            self.showAlert(alert: self.alertForRestorePurchases(result: results))
        }
        
    }
    @IBAction func verifyReceipt() {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            
            print(result)
            
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.showAlert(alert: self.alertForVerifyReceipt(result))
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func expiryDate(){
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                
                guard let data = receipt as? NSDictionary else {return}
                guard let latestReceipt = data["receipt"] as? NSDictionary else {return}
                guard let in_app = latestReceipt["in_app"] as? NSArray else {return}
                
                guard let dict = in_app.lastObject as? NSDictionary else {return}
                guard let productId = dict["product_id"] as? String else {return}
                guard let duration = dict["purchase_date_ms"] as? String else {return}
                
                UserDefaults.standard.setValue(duration, forKey: "expiresDate")
                UserDefaults.standard.setValue(productId, forKey: "productId")
                
                let purchaseResult = SwiftyStoreKit.verifyPurchase(
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let receiptItem):
                    print("\(productId) is purchased: \(receiptItem)")
                case .notPurchased:
                    print("The user has never purchased \(productId)")
                }
            case .error(let error):
                print("Receipt verification failed: \(error)")
            }
            
        }
    }
    
    
    
    
    func verifyPurchase(_ purchase: RegisteredPurchase) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                
                let productId = self.bundleID + "." + purchase.rawValue
                
                switch purchase {
                case .autoRenewableWeekly, .autoRenewableMonthly, .autoRenewableYearly:
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: productId,
                        inReceipt: receipt)
                    self.showAlert(alert: self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
                case .nonRenewingPurchase:
                    let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .nonRenewing(validDuration: 60),
                        productId: productId,
                        inReceipt: receipt)
                    self.showAlert(alert: self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
                default:
                    let purchaseResult = SwiftyStoreKit.verifyPurchase(
                        productId: productId,
                        inReceipt: receipt)
                    self.showAlert(alert: self.alertForVerifyPurchase(purchaseResult, productId: productId))
                }
                
            case .error:
                self.showAlert(alert: self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func refreshReceipt() {
        SwiftyStoreKit.fetchReceipt(forceRefresh: true, completion: { result in
            
        })
    }
    
}



enum RegisteredPurchase: String {
    
    case purchase1
    case purchase2
    case nonConsumablePurchase
    case consumablePurchase
    case nonRenewingPurchase = "demo1"
    case nonRenewingPurchase2 = "demo2"
    case autoRenewableWeekly
    case autoRenewableMonthly
    case autoRenewableYearly
}
