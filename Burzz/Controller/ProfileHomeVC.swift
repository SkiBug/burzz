//
//  ProfileHomeVC.swift
//  Burzz
//
//  Created by Raul Menendez on 09/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import SDWebImage
import FittedSheets
import GoogleMobileAds

class ProfileHomeVC: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var editProfileImage: UIImageView!
    
    @IBOutlet weak var bannerAdsView: GADBannerView!
    
    @IBOutlet weak var settingImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
        bannerAdsView.rootViewController = self
        bannerAdsView.load(GADRequest())
        
        
        profileImage.sd_setImage(with: URL(string:(currentUser.result?.profile_picture!)!), placeholderImage: UIImage(named: "profile_one_default"))
        profileImage.layer.cornerRadius = profileImage.frame.width/2
        profileImage.clipsToBounds = true
        profileName.text = String(describing:currentUser.result!.username!).capitalized
        if String(describing:currentUser.result!.dob!) != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dobDate = dateFormatter.date(from:String(describing:currentUser.result!.dob!))!
            let date = NSDate()
            let year = (date as Date).years(from : dobDate as Date)
            print(year)
            profileName.text = profileName.text! + ", " + String(describing: year)
        }
        
        /*editProfileImage.layer.cornerRadius = profileImage.frame.width/2
        settingImage.layer.cornerRadius = profileImage.frame.width/2
        editProfileImage.layer.borderWidth = 0.5
        editProfileImage.layer.borderColor = UIColor .lightGray.cgColor
        settingImage.layer.borderWidth = 0.5
        settingImage.layer.borderColor = UIColor .lightGray.cgColor*/
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "editProfile"), object: nil)
        
    }
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
        
            let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePageVC") as! ProfilePageVC
            self.navigationController?.pushViewController(newviewController, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         profileImage.sd_setImage(with: URL(string:(currentUser.result?.profile_picture!)!), placeholderImage: UIImage(named: "profile_one_default"))
        
    }
    
    
    @IBAction func onClickHelpSupport(_ sender: Any) {
        
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HelpSupportVC") as! HelpSupportVC
        self.navigationController?.pushViewController(newviewController, animated: true)
        
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSettings(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapEditInfo(_ sender: Any) {
    
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePageVC") as! ProfilePageVC
            self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    @IBAction func btnProfileTap(_ sender: Any) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        //  controller.blockId = serverArray[currentIndexDisplay].user_id!
        let sheetController = SheetViewController(controller: controller, sizes: [.fullScreen, .fullScreen , .fullScreen, .fullScreen])
        
        // Adjust how the bottom safe area is handled on iPhone X screens
        sheetController.blurBottomSafeArea = false
        sheetController.adjustForBottomSafeArea = true
        sheetController.handleColor = UIColor.clear
        sheetController.handleSize = CGSize(width: 0,height: 0)
        
        // Turn off rounded corners
        sheetController.topCornersRadius = 0
        
        // Make corners more round
        sheetController.topCornersRadius = 15
        
        // Disable the dismiss on background tap functionality
        sheetController.dismissOnBackgroundTap = false
        
        // Extend the background behind the pull bar instead of having it transparent
        sheetController.extendBackgroundBehindHandle = false
        // It is important to set animated to false or it behaves weird currently
        self.present(sheetController, animated: false, completion: nil)
        
        
    }
    
    
    
    
}
