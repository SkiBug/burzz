//
//  WalletView.swift
//  Burzz
//
//  Created by Pradipta Maitra on 06/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit

class WalletView: UIViewController {

    @IBOutlet weak var labelWalletBalance: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewWalletBalance()
    }
    
    func viewWalletBalance() {
        let walletRepository = WalletRepository()
        walletRepository.getWalletBalance(vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let data = response.data
                DispatchQueue.main.async {
                    self.labelWalletBalance.text = "\(data?.bal ?? 0) Coins"
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
