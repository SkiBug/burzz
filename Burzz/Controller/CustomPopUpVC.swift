//
//  CustomPopUpVC.swift
//  Burzz
//
//  Created by MAC MINI on 05/08/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import FittedSheets

class CustomPopUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    

    @IBOutlet weak var topView : UIView!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var customHeightConstraint: NSLayoutConstraint!
    
    var blockId = String()

    
    
    var aarStr : [String] = ["I don't want them to see me", "Made me uncomfortable", "inappopriate content", "Stolen photo"]
    var arrImg :[UIImage] = [UIImage(named: "ghost")!,UIImage(named: "uncomfortable")!,UIImage(named: "inapproriate")!,UIImage(named: "stolen_pic")!]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .clear
        self.topView.layer.cornerRadius = 15
        self.topView.clipsToBounds = true
        
        customHeightConstraint.constant = 376
        UIView.animate(withDuration: 2.0) {
            self.viewDidLayoutSubviews()
        }
    
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let value = UserDefaults.standard.value(forKey: "DismissView") as! Bool

        if value == true
        {
        
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aarStr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomPopUpCell", for: indexPath) as! CustomPopUpCell
        
        cell.lbl.text = aarStr[indexPath.row]
        cell.img.image = arrImg[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        controller.reason = aarStr[indexPath.row]
        controller.blockId = self.blockId
        self.present(controller, animated: true, completion: nil)
    }
    
    
}


class CustomPopUpCell : UITableViewCell
{
    @IBOutlet weak var lbl : UILabel!
    @IBOutlet weak var img : UIImageView!

}

