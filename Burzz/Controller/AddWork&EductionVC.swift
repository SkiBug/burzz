//
//  AddWork&EductionVC.swift
//  Burzz
//
//  Created by Gopal on 08/08/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
//import MBProgressHUD

class AddWork_EductionVC: UIViewController {
    
    @IBOutlet weak var txtTittle : UITextField!
    @IBOutlet weak var txtCompany : UITextField!
    @IBOutlet weak var button1Outlet: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var lblTittle: UILabel!
    
    var minDateSelected = ""
    
    var jobOrEducation = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if jobOrEducation == "job"
        {
            txtTittle.placeholder = "Title"
            txtCompany.placeholder = "Company"
            lblTittle.text = "Occupation"
        }
        else
        {
            txtTittle.placeholder = "Study"
            txtCompany.placeholder = "institution"
            lblTittle.text = "Education"
        }
        
        
        txtTittle.layer.cornerRadius = txtTittle.frame.height/2
        txtTittle.layer.borderWidth = 1.0
        txtTittle.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0).cgColor
        
        txtCompany.layer.cornerRadius = txtCompany.frame.height/2
        txtCompany.layer.borderWidth = 1.0
        txtCompany.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0).cgColor
        
        txtTittle.setLeftPaddingPoints(10)
        txtCompany.setLeftPaddingPoints(10)
        
        button1Outlet.layer.cornerRadius = txtCompany.frame.height/2
        button1Outlet.layer.borderWidth = 1.0
        button1Outlet.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0).cgColor

        btnToDate.layer.cornerRadius = txtCompany.frame.height/2
        btnToDate.layer.borderWidth = 1.0
        btnToDate.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0).cgColor
        
    }
    
    
    @IBAction func click_to_from(_ sender: Any) {
        
        
        DatePicker.selectDate { (selectedDate) in
            // TODO: Your implementation for date
            let str = selectedDate.dateString("MMM-YYYY")
            self.button1Outlet.setTitle(str, for: .normal)
           
        }
        
    }
    
    @IBAction func click_to_to(_ sender: Any) {
        
        DatePicker.selectDate { (selectedDate) in
            // TODO: Your implementation for date
            let str = selectedDate.dateString("MMM-YYYY")
            self.btnToDate.setTitle(str, for: .normal)
        }
        
    }
    

    @IBAction func click_to_add(_ sender: Any) {
        
        let strURL : URL
        let dict : Parameters

        if jobOrEducation == "education"
        {
            
//            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.PostEducationDetails)")!
//            dict = [ "user_id": currentUser.result!.id!, "study" : txtTittle.text!, "institution" : txtCompany.text!, "year" : button1Outlet.titleLabel!.text! + btnToDate.titleLabel!.text!]
            
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.PostEducationDetails)")!
                       dict = [ "user_id": currentUser.result!.id!, "study" : txtTittle.text!, "institution" : txtCompany.text!, "year" : "\(button1Outlet.titleLabel!.text!) - \(btnToDate.titleLabel!.text!)"]
            
        }
        else
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.PostJobDetails)")!
            dict = [ "user_id": currentUser.result!.id!,"company" : txtCompany.text!, "title" : txtTittle.text!, "duration" : "\(button1Outlet.titleLabel!.text!) - \(btnToDate.titleLabel!.text!)"]
            
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIGetDetails(APIurl: strURL, dictData: dict)
        
    }
    
    
    @IBAction func click_to_back(_ sender: Any) {
        
      //  self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func CallAPIGetDetails(APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            
                            if let statusData = JSON["status"] as? Int{
                                
                                if statusData == 1{
                                    print(JSON)
                                    QLSharedPreference.sharedInstance.saveUserData(JSON)
                                    self.navigationController?.popViewController(animated: true)
                                    
                                }else{
                                    
                        
                                    
                    }}}}
                }
                
        }}
   

}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


extension Date {
    
    func dateString(_ format: String = "MMM-dd-YYYY, hh:mm a") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    func dateByAddingYears(_ dYears: Int) -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.year = dYears
        
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }
}
