//
//  switchOptionVC.swift
//  Burzz
//
//  Created by Raul Menendez on 08/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import FSPagerView
import GoogleMobileAds
class switchOptionVC: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    @IBOutlet weak var bannerAdsView: GADBannerView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagerBackColorView: UIView!
    @IBOutlet weak var pageControl: FSPageControl!
    
    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            self.pagerView.register(UINib(nibName: "PagerOptionCell", bundle: nil), forCellWithReuseIdentifier: "PagerOptionCell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pagerView.reloadData()
        
        
        bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
        bannerAdsView.rootViewController = self
        bannerAdsView.load(GADRequest())
        
        pageControl.numberOfPages = 3
        // pageControl.setFillColor(.white, for: .selected)
        //pageControl.setFillColor(UIColor.init(displayP3Red: 40.0/255.0, green: 80.0/255.0, blue: 100.0/255.0, alpha: 1.0), for: .focused)
        //pagerView.alwaysBounceHorizontal = true
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        pagerView.delegate = self
        //pagerView.interitemSpacing = 10
        pagerView.itemSize = CGSize(width: pagerView.frame.size.width - 60 , height: pagerView.frame.size.height)
        //pagerView.decelerationDistance = 2
        //pagerView.transformer = FSPagerViewTransformer(type:.depth)
        //self.pagerView.register(PagerOptionCell.self, forCellWithReuseIdentifier: "PagerOptionCell")
        
        //register(FSPagerViewCell.self, forCellWithReuseIdentifier: "PagerOptionCell") as! PagerOptionCell
        // Do any additional setup after loading the view.
        
        let acountType = currentUser.result!.interest!
        
        if acountType == "1"{
            pageControl.currentPage = 1
            let indexPath = IndexPath(item: 0, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
            
        }else if acountType == "2"{
            pageControl.currentPage = 2
            let indexPath = IndexPath(item: 1, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
            
        }else if acountType == "3"{
            pageControl.currentPage = 3
            let indexPath = IndexPath(item: 2, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
        }
        
        
        
        
        DispatchQueue.main.async {
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return 3
    }
    // date color 176  107  198
    // friend 254 107 107
    // buizz 107 107 255
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "PagerOptionCell", at: index) as! PagerOptionCell
        
        
        if index == 0{
            cell.imageProfile.backgroundColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            cell.lblTitle.text = "Burzzcouple"
            cell.lblTitle.textColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            cell.btnSwitch.setTitle("Back to Burzzcouple", for: .normal)
            cell.btnSwitch.backgroundColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            // cell.backGroundColor.backgroundColor = UIColor.yellow
            
            
            
            
        }else if index == 1{
            // cell.backGroundColor.backgroundColor = UIColor.green
            cell.imageProfile.backgroundColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
            cell.lblTitle.text = "Burzzpal"
            cell.lblTitle.textColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
            cell.btnSwitch.setTitle("Back to Burzzpal", for: .normal)
            cell.btnSwitch.backgroundColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        }else{
            // cell.backGroundColor.backgroundColor = UIColor.magenta
            cell.imageProfile.backgroundColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
            cell.lblTitle.text = "Burzzpro"
            cell.lblTitle.textColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
            cell.btnSwitch.setTitle("Back to Burzzpro", for: .normal)
            cell.btnSwitch.backgroundColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
        }
        // if BricksData[0].images == nil{
        //cell.imageView?.image = UIImage(named: "hukui")
        // }else{
        //            let imageArr = imageScrollD[index]
        //            let imageURL = imageArr["image"] as! NSString
        //            cell.imageView?.sd_setImage(with: URL(string:imageURL as String), placeholderImage: UIImage(named: "default_product"))
        //        }
        //        cell.btnSwitch.tag = index
        //        cell.btnSwitch.addTarget(self, action: #selector(switchOptionVC.handleButton(_:)), for: .touchUpInside)
        //          cell.btnSwitch.addTarget(self, action: #selector(yourButtonClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    /*@objc func handleButton(_ sender: UIButton) {
     print(sender.tag)
     
     }*/
    
    @IBAction func onClickBtnAction(_ sender: UIButton) {
        print(sender.tag)
        
        if sender.tag == 0{
            currentUser.result?.interest = "1"
        }else if sender.tag == 1{
            currentUser.result?.interest = "2"
        }else if sender.tag == 2{
            currentUser.result?.interest = "3"
        }
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int){
        // print(targetIndex)
        pageControl.currentPage = targetIndex
        // pageControl.
        if targetIndex == 0{
            pageControl.setFillColor(UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
            
            
            pagerBackColorView.backgroundColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
        }else if targetIndex == 1{
            pageControl.setFillColor(UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0), for: .normal)
            
            pagerBackColorView.backgroundColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        }else{
            pageControl.setFillColor(UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0), for: .normal)
            
            pagerBackColorView.backgroundColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SwitchCollectionViewCell", for: indexPath) as! SwitchCollectionViewCell
        
        let imageProfile = cell.viewWithTag(1)
        //
        let lblTitle = cell.lblName
        let btnSwitch = cell.btnBack
        
        btnSwitch!.tag = indexPath.row
        
        let acountType1 = currentUser.result!.interest!
        if indexPath.row == 0{
            
            if acountType1 == "1"{
                btnSwitch?.setTitle("Back to Burzzcouple", for: .normal)
            }else{
                btnSwitch?.setTitle("Switch to Burzzcouple", for: .normal)
            }
            
            
            imageProfile!.backgroundColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            lblTitle?.text = "Burzzcouple"
            cell.lblDescription.text = "Find date with other people nearby"
            lblTitle?.textColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            btnSwitch!.backgroundColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            
            // cell.backGroundColor.backgroundColor = UIColor.yellow
        }else if indexPath.row == 1{
            
            if acountType1 == "2"{
                btnSwitch?.setTitle("Back to Burzzpal", for: .normal)
            }else{
                btnSwitch?.setTitle("Switch to Burzzpal", for: .normal)
            }
            
            // cell.backGroundColor.backgroundColor = UIColor.green
            imageProfile!.backgroundColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
            cell.lblDescription.text = "Find new friends with other people nearby"
            lblTitle?.text = "Burzzpal"
            lblTitle?.textColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
            btnSwitch?.backgroundColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
            
        }else{
            
            if acountType1 == "3"{
                btnSwitch?.setTitle("Back to Burzzpro", for: .normal)
            }else{
                btnSwitch?.setTitle("Switch to Burzzpro", for: .normal)
            }
            
            // cell.backGroundColor.backgroundColor = UIColor.magenta
            imageProfile!.backgroundColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
            cell.lblDescription.text = "Grow your network with other people nearby"
            lblTitle?.text = "Burzzpro"
            lblTitle?.textColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
            btnSwitch!.backgroundColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
        }
        
        return cell
    }
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
     let dict = arraystr[indexPath.row] as! NSDictionary
     let strwidth = String(describing:dict["text"]!)
     if strwidth.count > 5{
     return CGSize(width: 60 + strwidth.count*10 , height: 40)
     }else{
     return CGSize(width: 100, height: 40)
     }
     }*/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        return CGSize(width: screenWidth, height: 447)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)//
        // pageControl.
        if pageIndex == 0{
            pageControl.setFillColor(UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
            
            pagerBackColorView.backgroundColor = UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            
        }else if pageIndex == 1{
            pageControl.setFillColor(UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0), for: .normal)
            
            pagerBackColorView.backgroundColor = UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        }else{
            pageControl.setFillColor(UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0), for: .normal)
            
            pagerBackColorView.backgroundColor = UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0, alpha:  1.0)
        }
    }
}

