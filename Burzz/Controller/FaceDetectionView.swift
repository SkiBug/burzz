//
//  FaceDetectionView.swift
//  Burzz
//
//  Created by Pradipta Maitra on 21/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit
import Alamofire

class FaceDetectionView: UIViewController {

    @IBOutlet weak var imageViewFace: UIImageView!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var buttonProceed: UIButton!
    
    var image: UIImage!
    var imageArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.buttonClose.layer.cornerRadius = self.buttonClose.frame.size.height/2
        self.buttonProceed.layer.cornerRadius = self.buttonProceed.frame.size.height/2
    }
    
    func hitUploadImageToSSS(dataDict:  [String : Any]) {
        AmazonSSS.uploadDocumentToS3Server(dataDict: dataDict as NSDictionary) { (result, response, error, errorMessage) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if( error==nil && result == true){
                
                print(response!)
                var imageUrl = String(describing: response!)
                imageUrl = imageUrl.replacingOccurrences(of: "burzz/", with: "burzz//")
                
                self.imageArray.append(imageUrl)
                let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPLOADIMG1)")!
                
                let stringImage = self.imageArray.joined(separator: ", ")
                print(stringImage)
                
                let dict : [String:Any] = ["id": currentUser.result!.id!,"images": stringImage]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.UploadImg(APIurl: url, dictData: dict )
                currentUser.result?.profile_picture = self.imageArray[0]
                
            } else {
                self.addAlertView(appName, message: ALERTS.KUnableToUpload, buttonTitle: ALERTS.kAlertOK)
            }
        }
    }
    
    func UploadImg (APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                
                self.addAlertView(appName, message: "Image Uploaded." , buttonTitle: ALERTS.kAlertOK)
        }
    }
    
    @IBAction func buttonCloseAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonProceedAction(_ sender: Any) {
    }
    
}
