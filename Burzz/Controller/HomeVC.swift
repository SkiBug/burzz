//
//  HomeVC.swift
//  Burzz
//
//  Created by Raul Menendez on 09/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Cartography
import UIColor_FlatColors
//import MBProgressHUD
import Alamofire
import FittedSheets
import SimpleImageViewer


class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var xCenter: CGFloat = 0.0
    var yCenter: CGFloat = 0.0
    var originalPoint = CGPoint.zero
    var currentIndexDisplay = 0
    var swappingCount : Int = 0
    var page : Int = 1
    var imageViewStatus = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    //weak var delegate: TinderCardDelegate?
    
    var serverArray = [OtherUserData]()
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var newsCardView: NewsCardView!
    @IBOutlet weak var newsCardBack: NewsCardView!
    @IBOutlet weak var swipeableView: ZLSwipeableView!
    @IBOutlet weak var tableHomeBack: UITableView!
    
    @IBOutlet weak var purchaseViewBG: UIView!
    @IBOutlet weak var chatBtn: UIButton!
    
    @IBOutlet weak var btnType: UIButton!
    
    var ProductlistArray = Array<Any>()
    var arraystr =  Array<Any>() //"Exerice","Education level","Drinking","Smoking","Zodiac","Religion"
    var imageArray = Array<Any>() //"ScreenS4","hqdefault.jpg"
    var jobArray = Array<Any>()
    var eductionArray = Array<Any>()
    var instgramArray = Array<Any>()
    
    var notificationData: NSDictionary = NSDictionary()
    
    
    @IBOutlet weak var tableHome: UITableView!
    
    //MARK: - Properties
    
    var progressView = UIView(frame: CGRect(x: 0, y: 50, width: 6, height: 50))
    var progressSubView = UIView(frame: CGRect(x: 0, y: 50, width: 6, height: 20))
    var newView : UIView?
    var shouldSwipe = true
    var reload = false
    
    var intrestedUserID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //        let is_ios = (UserDefaults.standard.value(forKey: "is_ios") as? Int) ?? 1
        //
        //        if is_ios == 1{
        //            chatBtn.isHidden = true
        //        }else{
        //            chatBtn.isHidden = false
        //        }
        
        
        chatBtn.isHidden = false
        self.purchaseViewBG.isHidden = true
        progressView.isHidden = true
        progressSubView.isHidden = true
        // imageViewStatus?.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        // imageViewStatus
        
        if notificationData != [:]{
            let jsonData = notificationData["json"]  as? NSDictionary
            let notificationCode = jsonData!["notification_code"] as! Int
            
            if notificationCode == 109{
                let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChatHomeVC") as! ChatHomeVC
                self.navigationController?.pushViewController(VC, animated: true)
            }
        }
        
        
        
        
        UserDefaults.standard.set(false, forKey: "DismissView")
        
        //imageViewStatus.bringSubviewToFront(viewContainer)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let value = UserDefaults.standard.value(forKey: "DismissView") as? Bool
        {
            if value == true
            {
                APiCalling()
                UserDefaults.standard.set(false, forKey: "DismissView")
            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if currentUser == nil {
            currentUser = User1.init(dictionary: QLSharedPreference.sharedInstance.getAllUserData()!)
            
        }
        
        if (currentIndexDisplay >= 0) {
            
            progressView.center = CGPoint(x: self.view.frame.size.width*0.92, y: tableHome.frame.origin.x + 100 )
            self.view.addSubview(progressView)
            progressView.backgroundColor = UIColor.init(red: 224.0/255.0, green: 196.0/255.0, blue: 232.0/255.0, alpha: 1.0)
            progressView.layer.cornerRadius = 3
            progressSubView.center = CGPoint(x: progressView.frame.size.width*0.50, y: progressView.frame.size.height*0.00 + 10)
            progressView.addSubview(progressSubView)
            progressSubView.backgroundColor = UIColor.init(red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            progressSubView.layer.cornerRadius = 3
            
            tableHomeBack.isHidden = true
            
            let gettouch = UIPanGestureRecognizer(target: self, action: #selector(self.responsePan))
            newsCardView.addGestureRecognizer(gettouch)
            
            imageViewStatus.center = CGPoint(x: self.view.frame.size.width*0.50, y: self.view.frame.size.height*0.50)
            imageViewStatus.backgroundColor = UIColor.clear
            viewContainer.addSubview(imageViewStatus)
            
        }
        
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.IS_SWIPE_ALLOWED)")!
        let param : [String:String] = ["user_id": currentUser.result!.id!]
        
        self.isSwipeAllowed(APIurl: url, dictData: param)
        
        let acountType = currentUser.result!.interest!
        
        if acountType == "1"{
            
            btnType.setTitle("Burzzcouple", for: .normal)
            btnType.setTitleColor(UIColor.init(displayP3Red: 176.0/255.0, green: 107.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
            
        }else if acountType == "2"{
            btnType.setTitle("Burzzpal", for: .normal)
            btnType.setTitleColor(UIColor.init(displayP3Red: 254.0/255.0, green: 107.0/255.0, blue: 107.0/255.0, alpha: 1.0), for: .normal)
        }else if acountType == "3"{
            btnType.setTitle("Burzzpro", for: .normal)
            btnType.setTitleColor(UIColor.init(displayP3Red: 107.0/255.0, green: 107.0/255.0, blue: 255.0/255.0,alpha: 1.0), for: .normal)
        }
        ISVerified()
        APiCalling()
        APiCallingSub()
    }
    
    
    
    func animateCard(angle:CGFloat = 0){
        self.newsCardView.layer.transform = CATransform3DMakeRotation(angle, 0.0, 0.0, 1.0)
    }
    
    var isAPICalled: Bool = false
    
    @objc func responsePan(gesture: UIPanGestureRecognizer){
        
        /*let is_verified = UserDefaults.standard.value(forKey: "is_verified") as? String ?? "0"
        
        if is_verified == "1" {
            
            if reload == true{
                currentIndexDisplay = -1
                reload = false
            }
            
            if currentIndexDisplay <= serverArray.count-1{
                
                xCenter = gesture.translation(in: newsCardView).x
                yCenter = gesture.translation(in: newsCardView).y
                switch gesture.state {
                // Keep swiping
                case .began:
                    originalPoint = newsCardView.center
                    break;
                //in the middle of a swipe
                case .changed:
                    if (currentIndexDisplay >= 0){
                        
                        let rotationStrength = min(xCenter / UIScreen.main.bounds.size.width, 1)
                        let rotationAngel = .pi/5 * rotationStrength
                        let scale = max(1 - abs(rotationStrength) / stength, range)
                        newsCardView.center = CGPoint(x: originalPoint.x + xCenter, y: originalPoint.y + yCenter)
                        let transforms = CGAffineTransform(rotationAngle: rotationAngel)
                        let scaleTransform: CGAffineTransform = transforms.scaledBy(x: scale, y: scale)
                        newsCardView.transform = scaleTransform
                        updateOverlay(xCenter)
                        tableHomeBack.isHidden = false
                        
                    }
                    
                    break;
                // swipe ended
                case .ended:
                    afterSwipeAction()
                    tableHomeBack.isHidden = true
                    break;
                    
                case .possible:break
                case .cancelled:break
                case .failed:break
                @unknown default:
                    fatalError()
                }
            }
        }else{
            self.addAlertView(appName, message: "Your account verification is still pending" , buttonTitle: ALERTS.kAlertOK)
            
        }*/
        
        let ValidDate = Date()
        var serverDate = Date()
        var ValidDate2 = Calendar.current.date(byAdding: .day, value: -1, to: ValidDate)
        
        if UserDefaults.standard.value(forKey: "ValidDate") != nil{
            ValidDate2 =  UserDefaults.standard.value(forKey: "ValidDate") as? Date
            serverDate =  UserDefaults.standard.value(forKey: "serverDate") as! Date
        }
        
        if ValidDate2! >= serverDate{
            var swapCount: Int = 0
            if UserDefaults.standard.value(forKey: "SwappingCount") != nil {
                swapCount = UserDefaults.standard.value(forKey: "SwappingCount") as! Int
            }
            if swapCount <= 18 {
                if reload == true{
                    currentIndexDisplay = -1
                    reload = false
                }
                
                if currentIndexDisplay <= serverArray.count-1{
                    
                    xCenter = gesture.translation(in: newsCardView).x
                    yCenter = gesture.translation(in: newsCardView).y
                    switch gesture.state {
                    // Keep swiping
                    case .began:
                        originalPoint = newsCardView.center
                        break;
                    //in the middle of a swipe
                    case .changed:
                        if (currentIndexDisplay >= 0){
                            
                            let rotationStrength = min(xCenter / UIScreen.main.bounds.size.width, 1)
                            let rotationAngel = .pi/5 * rotationStrength
                            let scale = max(1 - abs(rotationStrength) / stength, range)
                            newsCardView.center = CGPoint(x: originalPoint.x + xCenter, y: originalPoint.y + yCenter)
                            let transforms = CGAffineTransform(rotationAngle: rotationAngel)
                            let scaleTransform: CGAffineTransform = transforms.scaledBy(x: scale, y: scale)
                            newsCardView.transform = scaleTransform
                            updateOverlay(xCenter)
                            tableHomeBack.isHidden = false
                            
                        }
                        
                        break;
                    // swipe ended
                    case .ended:
                        afterSwipeAction()
                        
                        tableHomeBack.isHidden = true
                        break;
                        
                    case .possible:break
                    case .cancelled:break
                    case .failed:break
                    @unknown default:
                        fatalError()
                    }
                }
            }else{
                if isAPICalled == false {
                    swappingCount = 0
                    UserDefaults.standard.set(swappingCount, forKey: "SwappingCount")
                    self.page += 1
                    self.APiCalling()
                }
                isAPICalled = true
            }
        }else{
            print("swap_count...\(swappingCount)")
            var swapCount: Int = 0
            if UserDefaults.standard.value(forKey: "SwappingCount") != nil {
                swapCount = UserDefaults.standard.value(forKey: "SwappingCount") as! Int
            }
            print("swap_count...\(swapCount)")
            if swapCount <= 18 {
                
                if reload == true{
                    currentIndexDisplay = -1
                    reload = false
                }
                
                if currentIndexDisplay <= serverArray.count-1{
                    
                    xCenter = gesture.translation(in: newsCardView).x
                    yCenter = gesture.translation(in: newsCardView).y
                    switch gesture.state {
                    // Keep swiping
                    case .began:
                        originalPoint = newsCardView.center
                        break;
                    //in the middle of a swipe
                    case .changed:
                        if (currentIndexDisplay >= 0){
                            
                            let rotationStrength = min(xCenter / UIScreen.main.bounds.size.width, 1)
                            let rotationAngel = .pi/5 * rotationStrength
                            let scale = max(1 - abs(rotationStrength) / stength, range)
                            newsCardView.center = CGPoint(x: originalPoint.x + xCenter, y: originalPoint.y + yCenter)
                            let transforms = CGAffineTransform(rotationAngle: rotationAngel)
                            let scaleTransform: CGAffineTransform = transforms.scaledBy(x: scale, y: scale)
                            newsCardView.transform = scaleTransform
                            updateOverlay(xCenter)
                            tableHomeBack.isHidden = false
                            
                        }
                        
                        break;
                    // swipe ended
                    case .ended:
                        afterSwipeAction()
                        
                        tableHomeBack.isHidden = true
                        break;
                        
                    case .possible:break
                    case .cancelled:break
                    case .failed:break
                    @unknown default:
                        fatalError()
                    }
                }
            }else{
                //            self.addAlertView(appName, message: "Please purchase to see more user" , buttonTitle: ALERTS.kAlertOK)
                self.purchaseViewBG.isHidden = false
                let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.INFORM_SWIPE_LIMIT)")!
                let currentDate = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                let dateStr = formatter.string(from: currentDate)
                let param : [String:String] = ["user_id": currentUser.result!.id!, "getToDaysCrossLimit": "20", "pageNo": "\(self.page)", "timeAndDate": dateStr]
                print(url)
                print(param)
                
                self.sendSwipeLimit(APIurl: url, dictData: param)
            }
        }
    }
    
    func sendSwipeLimit(APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            if let statusData = JSON["status"] as? Int{
                                if statusData == 1{
                                    
                                }else{
                                    
                                }
                            }
                        }
                    }
                }
        }
    }
    
    func isSwipeAllowed(APIurl: URL,dictData : Parameters) {
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            if let statusData = JSON["status"] as? Int{
                                if statusData == 1{
                                    let arrayData  = JSON["data"] as! NSArray
                                    if arrayData.count != 0 {
                                        print(arrayData)
                                        let dictData = arrayData[0] as! NSDictionary
                                        print(dictData)
                                        let isSwipe: Bool = dictData["isSwap"] as! Bool
                                        let pageNo = dictData["pageNo"]
                                        print(isSwipe)
                                        print(pageNo!)
                                        if isSwipe == true {
                                            self.purchaseViewBG.isHidden = true
                                            var swapCount: Int = 0
                                            if UserDefaults.standard.value(forKey: "SwappingCount") != nil {
                                                swapCount = UserDefaults.standard.value(forKey: "SwappingCount") as! Int
                                            }
                                            if swapCount>18 {
                                                UserDefaults.standard.removeObject(forKey: "SwappingCount")
                                            }
                                        }else{
                                            self.purchaseViewBG.isHidden = false
                                        }
                                    }
                                }else{
                                    let arrayData  = JSON["data"] as! NSArray
                                    if arrayData.count != 0 {
                                        print(arrayData)
                                        let dictData = arrayData[0] as! NSDictionary
                                        print(dictData)
                                        let isSwipe: Bool = dictData["isSwap"] as! Bool
                                        let pageNo = dictData["pageNo"]
                                        
                                        print(isSwipe)
                                        print(pageNo!)
                                        if isSwipe == true {
                                            self.purchaseViewBG.isHidden = true
                                            var swapCount: Int = 0
                                            if UserDefaults.standard.value(forKey: "SwappingCount") != nil {
                                                swapCount = UserDefaults.standard.value(forKey: "SwappingCount") as! Int
                                            }
                                            if swapCount>18 {
                                                UserDefaults.standard.removeObject(forKey: "SwappingCount")
                                            }
                                        }else{
                                            self.purchaseViewBG.isHidden = false
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        }
    }
    
    @IBAction func onClickLikeDislike(_ sender: UIButton) {
        
        //let is_verified = UserDefaults.standard.value(forKey: "is_verified") as? String ?? "0"
        
        //if is_verified == "1" {
            
            if reload == true{
                currentIndexDisplay = -1
                reload = false
            }
            
            if currentIndexDisplay <= serverArray.count-1{
                
                if sender.tag == 100{
                    
                    cardGoesRight()
                    
                    intrestedUserID = serverArray[currentIndexDisplay].user_id!
                    APIlike()
                    serverArray.remove(at: currentIndexDisplay)
                    
                    if serverArray.count >= 1{
                        
                        tableHome.scroll(to: .top, animated: true)
                        tableHomeBack.scroll(to: .top, animated: true)
                        self.imageArray = (self.serverArray[self.currentIndexDisplay].image!)
                        
                        self.arraystr = Array<Any>()
                        let Dummy = (serverArray[currentIndexDisplay].preference!)
                        for i in 0..<Dummy.count{
                            let dict = Dummy[i] as! NSDictionary
                            if String(describing:dict["text"]!) != ""{
                                self.arraystr.append(dict)
                            }
                        }
                        
                        self.instgramArray = (self.serverArray[self.currentIndexDisplay].insta_images!)
                        self.jobArray = (self.serverArray[self.currentIndexDisplay].job_data!)
                        self.eductionArray = (self.serverArray[self.currentIndexDisplay].education_data!)
                        
                        self.tableHome.reloadData()
                        if currentIndexDisplay + 1 >= serverArray.count{
                            self.tableHomeBack.isHidden = true
                            if currentIndexDisplay + 1 >= serverArray.count-1{
                                reload = true
                            }
                        }
                        else{
                            if currentIndexDisplay < serverArray.count - 2{
                                self.tableHomeBack.reloadData()
                                self.tableHomeBack.isHidden = false
                            }
                        }
                    }else{
                        tableHome.isHidden = true
                        tableHomeBack.isHidden = true
                        self.viewContainer.isHidden = true
                    }
                    
                }
                else{
                    cardGoesLeft()
                    
                    intrestedUserID = serverArray[currentIndexDisplay].user_id!
                    APIdislike()
                    serverArray.remove(at: currentIndexDisplay)
                    if serverArray.count >= 1{
                        
                        //              currentIndexDisplay = currentIndexDisplay + 1
                        tableHome.scroll(to: .top, animated: true)
                        tableHomeBack.scroll(to: .top, animated: true)
                        
                        
                        self.imageArray = (self.serverArray[self.currentIndexDisplay].image!)
                        self.arraystr = Array<Any>()
                        let Dummy = (serverArray[currentIndexDisplay].preference!)
                        for i in 0..<Dummy.count{
                            let dict = Dummy[i] as! NSDictionary
                            if String(describing:dict["text"]!) != ""{
                                self.arraystr.append(dict)
                            }
                        }
                        
                        self.instgramArray = (self.serverArray[self.currentIndexDisplay].insta_images!)
                        self.jobArray = (self.serverArray[self.currentIndexDisplay].job_data!)
                        self.eductionArray = (self.serverArray[self.currentIndexDisplay].education_data!)
                        
                        self.tableHome.reloadData()
                        if currentIndexDisplay + 1 >= serverArray.count{
                            self.tableHomeBack.isHidden = true
                            if currentIndexDisplay + 1 >= serverArray.count-1{
                                reload = true
                            }
                        }
                        else{
                            if currentIndexDisplay < serverArray.count - 2{
                                self.tableHomeBack.reloadData()
                                self.tableHomeBack.isHidden = false
                            }
                        }
                    }else{
                        tableHome.isHidden = true
                        tableHomeBack.isHidden = true
                        self.viewContainer.isHidden = true
                    }
                    tableHomeBack.isHidden = true
                }
                
            }
        //}else{
        //    self.addAlertView(appName, message: "Your account verification is still pending" , buttonTitle: ALERTS.kAlertOK)
        //}
    }
    
    @IBAction func didTapPremiumButton(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    @IBAction func onClicktoBlockAndReport(_ sender : UIButton)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CustomPopUpVC") as! CustomPopUpVC
        
        controller.blockId = serverArray[currentIndexDisplay].user_id!
        
        UserDefaults.standard.set(false, forKey: "DismissView")
        
        let sheetController = SheetViewController(controller: controller, sizes: [.fixed(420), .fixed(420), .halfScreen, .fullScreen])
        
        // Adjust how the bottom safe area is handled on iPhone X screens
        sheetController.blurBottomSafeArea = false
        sheetController.adjustForBottomSafeArea = true
        
        // Turn off rounded corners
        sheetController.topCornersRadius = 0
        
        // Make corners more round
        sheetController.topCornersRadius = 15
        
        // Disable the dismiss on background tap functionality
        sheetController.dismissOnBackgroundTap = false
        
        // Extend the background behind the pull bar instead of having it transparent
        sheetController.extendBackgroundBehindHandle = false
        
        // It is important to set animated to false or it behaves weird currently
        self.present(sheetController, animated: false, completion: nil)
    }
    
    
    func updateOverlay(_ distance: CGFloat) {
        if (currentIndexDisplay >= 0) {
            imageViewStatus.image = distance > 0 ? #imageLiteral(resourceName: "swipe_like_date-1") : #imageLiteral(resourceName: "swipe_dislike-1")
            //       backGroundImage.image = distance > 0 ? #imageLiteral(resourceName: "overlay_like") : #imageLiteral(resourceName: "overlay_skip")
            imageViewStatus.alpha = min(abs(distance) / 100, 0.8)
            //       backGroundImage.alpha = min(abs(distance) / 100, 0.5)
            //        currentCardStatus(card: newsCardView, distance: distance)
        }
        
    }
    
    
    func afterSwipeAction() {
        if reload == true{
            currentIndexDisplay = -1
            reload = false
        }
        if currentIndexDisplay <= serverArray.count-1{
            if xCenter > theresoldMargin {
                
                if (currentIndexDisplay >= 0) {
                    cardGoesRight()
                    
                    intrestedUserID = serverArray[currentIndexDisplay].user_id!
                    APIlike()
                    serverArray.remove(at: currentIndexDisplay)
                    
                    if serverArray.count >= 1{
                        
                        tableHome.scroll(to: .top, animated: true)
                        tableHomeBack.scroll(to: .top, animated: true)
                        self.imageArray = (self.serverArray[self.currentIndexDisplay].image!)
                        
                        self.arraystr = Array<Any>()
                        let Dummy = (serverArray[currentIndexDisplay].preference!)
                        
                        for i in 0..<Dummy.count{
                            let dict = Dummy[i] as! NSDictionary
                            if String(describing:dict["text"]!) != ""{
                                self.arraystr.append(dict)
                            }
                        }
                        
                        self.instgramArray = (self.serverArray[self.currentIndexDisplay].insta_images!)
                        self.jobArray = (self.serverArray[self.currentIndexDisplay].job_data!)
                        self.eductionArray = (self.serverArray[self.currentIndexDisplay].education_data!)
                        
                        self.tableHome.reloadData()
                        if currentIndexDisplay + 1 >= serverArray.count{
                            self.tableHomeBack.isHidden = true
                            if currentIndexDisplay + 1 >= serverArray.count-1{
                                reload = true
                            }
                        }
                        else{
                            if currentIndexDisplay < serverArray.count - 2{
                                self.tableHomeBack.reloadData()
                                self.tableHomeBack.isHidden = false
                            }
                        }
                    }else{
                        tableHome.isHidden = true
                        tableHomeBack.isHidden = true
                        self.viewContainer.isHidden = true
                    }
                    // asm160896
                }else{
                    tableHome.isHidden = true
                    tableHomeBack.isHidden = true
                }
                
            }
            else if xCenter < -theresoldMargin {
                
                if (currentIndexDisplay >= 0){
                    cardGoesLeft()
                    swappingCount += 1
                    var swapCount: Int = 0
                    if UserDefaults.standard.value(forKey: "SwappingCount") != nil && swappingCount == 1 {
                        
                        swapCount = UserDefaults.standard.value(forKey: "SwappingCount") as! Int
                        swapCount += 1
                        UserDefaults.standard.set(swapCount, forKey: "SwappingCount")
                        
                    } else if UserDefaults.standard.value(forKey: "SwappingCount") != nil {
                        
                        swapCount = UserDefaults.standard.value(forKey: "SwappingCount") as! Int
                        swapCount += 1
                        UserDefaults.standard.set(swapCount, forKey: "SwappingCount")
                        
                    } else {
                        
                        swapCount += 1
                        UserDefaults.standard.set(swapCount, forKey: "SwappingCount")
                    }
                    
                    intrestedUserID = serverArray[currentIndexDisplay].user_id!
                    APIdislike()
                    serverArray.remove(at: currentIndexDisplay)
                    
                    if serverArray.count >= 1{
                        tableHome.scroll(to: .top, animated: true)
                        tableHomeBack.scroll(to: .top, animated: true)
                        self.imageArray = (self.serverArray[self.currentIndexDisplay].image!)
                        self.arraystr = Array<Any>()
                        
                        let Dummy = (serverArray[currentIndexDisplay].preference!)
                        for i in 0..<Dummy.count{
                            let dict = Dummy[i] as! NSDictionary
                            if String(describing:dict["text"]!) != ""{
                                self.arraystr.append(dict)
                            }
                        }
                        
                        self.instgramArray = (self.serverArray[self.currentIndexDisplay].insta_images!)
                        self.jobArray = (self.serverArray[self.currentIndexDisplay].job_data!)
                        self.eductionArray = (self.serverArray[self.currentIndexDisplay].education_data!)
                        
                        
                        self.tableHome.reloadData()
                        if currentIndexDisplay + 1 >= serverArray.count{
                            self.tableHomeBack.isHidden = true
                            if currentIndexDisplay + 1 >= serverArray.count-1{
                                reload = true
                            }
                        }
                        else{
                            if currentIndexDisplay < serverArray.count - 2{
                                self.tableHomeBack.reloadData()
                                self.tableHomeBack.isHidden = false
                            }
                        }
                    }else{
                        tableHome.isHidden = true
                        tableHomeBack.isHidden = true
                        self.viewContainer.isHidden = true
                        
                    }
                    
                }else{
                    tableHomeBack.isHidden = true
                    tableHome.isHidden = true
                }
            }
            else {
                
                if (currentIndexDisplay >= 0){
                    //reseting image
                    UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: [], animations: {
                        
                        self.newsCardView.center = self.originalPoint
                        self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
                        self.imageViewStatus.alpha = 0
                        //self.backGroundImage.alpha = 0
                    })
                }
            }
        }
    }
    
    func cardGoesRight() {
        
        let finishPoint = CGPoint(x: newsCardView.frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        let finishPoint2 = CGPoint(x: -newsCardView.frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        UIView.animate(withDuration: 0.5, animations: {
            self.newsCardView.center = finishPoint
            self.imageViewStatus.center = finishPoint2
        }, completion: {(_) in
            // self.newsCardView.removeFromSuperview()
            //  self.tableHome.reloadData()
            // self.tableHomeBack.reloadData()
            // self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
            //            self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
            //            self.viewContainer.insertSubview(self.newsCardView, at: 1)
        })
        // self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
        // self.newsCardView.removeFromSuperview()
        // isLiked = true
        //removeCardAndAddNewCard()
        // delegate?.cardGoesRight(card: self)
        //self.viewContainer.insertSubview(subview2!, belowSubview: subview1!)
        //self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
        self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
        imageViewStatus.center = CGPoint(x: self.view.frame.size.width*0.50, y: self.view.frame.size.height*0.50)
        self.viewContainer.insertSubview(newsCardView, at: 1)
        self.imageViewStatus.alpha = 0
        //self.insertSubview(tinderCard, belowSubview: currentLoadedCards.last!)
        //   print("WATCHOUT RIGHT")
        
    }
    
    func cardGoesLeft() {
        
        let finishPoint = CGPoint(x: -newsCardView.frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        let finishPoint2 =  CGPoint(x: newsCardView.frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        UIView.animate(withDuration: 0.5, animations: {
            // self.newsCardView.center = finishPoint
            //  self.imageViewStatus.center = finishPoint2
        }, completion: {(_) in
            
            //    print("card goes left")
            // self.tableHome.reloadData()
            //   self.newsCardView.removeFromSuperview()
            
            //            self.viewContainer.insertSubview(self.newsCardView, at: 1)
            //self.newsCardView.reloadInputViews()
        })
        // self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
        // self.newsCardView.removeFromSuperview()
        //  isLiked = false
        //removeCardAndAddNewCard()
        //delegate?.cardGoesLeft(card: self)
        //
        // self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
        self.newsCardView.transform = CGAffineTransform(rotationAngle: 0)
        //imageViewStatus.center = CGPoint(x: self.view.frame.size.width*0.50, y: self.view.frame.size.height*0.50)
        self.imageViewStatus.alpha = 0
        self.viewContainer.insertSubview(newsCardView, at: 1)
        
        
        
    }
    //    func removeCardAndAddNewCard(){
    //
    //        currentLoadedCards.remove(at: 0)
    //        index += 1
    //        if (index + currentLoadedCards.count) < allCards.count {
    //            let tinderCard = createTinderCard(element: allCards[index + currentLoadedCards.count])
    //            self.insertSubview(tinderCard, belowSubview: currentLoadedCards.last!)
    //            currentLoadedCards.append(tinderCard)
    //        }
    //
    //        animateCardAfterSwiping()
    //    }
    
    func APiCalling() {
        let strURL =  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KHomePageAPI
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let dicURL : [String : String] = ["user_id": currentUser.result!.id!, "type": currentUser.result!.interest!, "page": "\(self.page)"]
                
        APIHelper.sharedInstance.requestPOSTWithouAUTHURL(strURL, params :dicURL,headers :nil, success: {
            (JSONResponse, resp) -> Void in
            // print(JSONResponse)
            MBProgressHUD.hide(for: self.view, animated: true)
            let responseDict = JSONResponse.dictionary
            let statusCode = responseDict!["status"]?.bool
            if(statusCode == false)
            {
                //   self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
                
            }
            if(statusCode == true)
            {
                self.tableHome.isHidden = false
                self.tableHomeBack.isHidden = false
                self.viewContainer.isHidden = false
                
                self.serverArray = [OtherUserData]()
                if let JSON =  responseDict!["data"]?.dictionary {
                    if let usersData = JSON["users"]?.arrayObject {
                        
                        for dic in usersData
                        {
                            let allNewsData = OtherUserData(dictionary: dic as! NSDictionary)
                            self.serverArray.append(allNewsData!)
                        }
                    }
                }
                
                if self.serverArray.count>0{
                    self.imageArray = (self.serverArray[self.currentIndexDisplay].image!)
                }else{
                    self.viewContainer.isHidden = true
                }
                
                self.arraystr = Array<Any>()
                let Dummy = (self.serverArray[self.currentIndexDisplay].preference!)
                
                for i in 0..<Dummy.count{
                    let dict = Dummy[i] as! NSDictionary
                    if String(describing:dict["text"]!) != ""{
                        self.arraystr.append(dict)
                    }
                }
                
                self.instgramArray = (self.serverArray[self.currentIndexDisplay].insta_images!)
                self.jobArray = (self.serverArray[self.currentIndexDisplay].job_data!).reversed()
                self.eductionArray = (self.serverArray[self.currentIndexDisplay].education_data!).reversed()
                
            }else{
                self.tableHome.isHidden = true
                self.tableHomeBack.isHidden = true
                self.viewContainer.isHidden = true
            }
            
            DispatchQueue.main.async {
                // self.totalIndexData = self.serverArray.count
                self.tableHome.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
                self.tableHomeBack.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
                self.tableHome.reloadData()
                self.tableHomeBack.reloadData()
            }
            
        },failure:{
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
            //    self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
            DispatchQueue.main.async {
                
                
            }
        })
    }
    
    
    func APIdislike(){
        
        let strURL =  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.notInterestedInUser
        // MBProgressHUD.showAdded(to: self.view, animated: true)
        let dicURL : [String : String] = ["user_id": currentUser.result!.id!,"user_id_interested": intrestedUserID]
        APIHelper.sharedInstance.requestPOSTWithouAUTHURL(strURL, params :dicURL,headers :nil, success: {
            (JSONResponse, resp) -> Void in
            
            //          MBProgressHUD.hide(for: self.view, animated: true)
            let responseDict = JSONResponse.dictionary
            let statusCode = responseDict!["status"]?.bool
            if(statusCode == true)
            {
                print("dislike")
            }
            
        },failure:{
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
            //    self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
            DispatchQueue.main.async {
                
                
            }
        })
        
    }
    
    func APIlike(){
        
        let strURL =  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.interestedInUser
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        let dicURL : [String : String] = ["user_id": currentUser.result!.id!,"user_id_interested": intrestedUserID]
        APIHelper.sharedInstance.requestPOSTWithouAUTHURL(strURL, params :dicURL,headers :nil, success: {
            (JSONResponse, resp) -> Void in
            
            //   MBProgressHUD.hide(for: self.view, animated: true)
            let responseDict = JSONResponse.dictionary
            let statusCode = responseDict!["status"]?.bool
            if(statusCode == true)
            {
                let dictData  = responseDict!["data"]!.dictionary
                
                if dictData!.isEmpty{
                    print("isempty")
                    
                }
                else{
                    
                    let is_ios = UserDefaults.standard.value(forKey: "is_ios") as! Int
                    
                    //             if is_ios == 1{
                    
                    
                    
                    //               }else{
                    
                    let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "MatchViewController") as! MatchViewController
                    newviewController.dictData = dictData!
                    self.navigationController?.pushViewController(newviewController, animated: true)
                    //           }
                    //      print("user match")
                }
                
            }
            
        },failure:{
            (error) -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
            //    self.addAlertView(appName, message: "error" , buttonTitle: ALERTS.kAlertOK)
            DispatchQueue.main.async {
                
                
            }
        })
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = scrollView.contentSize.height - tableHome.frame.size.height
        let value = scrollView.contentOffset.y
        //print("scrollView.contentOffset.y" + "\(scrollView.contentOffset.y)")
        var getvalue = (scrollView.contentOffset.y/total)*100
        if value < 0 {
            progressSubView.center = CGPoint(x: progressView.frame.size.width*0.50, y: progressView.frame.size.height*0.10 )
        }else if value > total {
            progressSubView.center = CGPoint(x: progressView.frame.size.width*0.50, y: progressView.frame.size.height*0.90)
        }
        else{
            getvalue = (scrollView.contentOffset.y/total*(80/100))*100
            // print(getvalue)
            progressSubView.center = CGPoint(x: progressView.frame.size.width*0.50, y: progressView.frame.size.height*(getvalue/100) )
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableView{
            
            let intrestIn = currentUser.result!.interest ?? "3"
            if intrestIn == "3"{
                
                if indexPath.row < imageArray.count{
                    
                    return 680
                    
                }else if indexPath.row == imageArray.count{
                    
                    return UITableView.automaticDimension
                    
                }else if indexPath.row == imageArray.count+1{
                    return UITableView.automaticDimension
                    
                }else if indexPath.row == imageArray.count+2{
                    
                    return UITableView.automaticDimension
                    
                }else if indexPath.row == imageArray.count+3{
                    let jobData = jobArray.count
                    
                    if jobData == 0{
                        return 0
                    }else{
                        return UITableView.automaticDimension
                    }
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count{
                    
                    let jobData = jobArray.count
                    
                    if jobData == 0{
                        return 0
                        
                    }else{
                        return UITableView.automaticDimension
                    }
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1{
                    
                    let eduData = eductionArray.count
                    if eduData == 0{
                        return 0
                    }else{
                        return UITableView.automaticDimension
                    }
                    
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1 + eductionArray.count{
                    
                    let eduData = eductionArray.count
                    if eduData == 0{
                        return 0
                    }else{
                        return UITableView.automaticDimension
                    }
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1 + eductionArray.count + 1{
                    
                    return UITableView.automaticDimension
                }else{
                    return UITableView.automaticDimension
                }
                
            }else{
                
                if indexPath.row < imageArray.count{
                    
                    return 600
                    
                }else if indexPath.row == imageArray.count{
                    
                    return UITableView.automaticDimension
                    
                }else if indexPath.row == imageArray.count+1{
                    return UITableView.automaticDimension
                    
                }else if indexPath.row == imageArray.count+2{
                    
                    return UITableView.automaticDimension
                    
                }else if indexPath.row == imageArray.count+3{
                    let jobData = jobArray.count
                    
                    return 0
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count{
                    
                    return 0
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1{
                    
                    return 0
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1 + eductionArray.count{
                    
                    return 0
                    
                }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1 + eductionArray.count + 1{
                    
                    return UITableView.automaticDimension
                }else{
                    return UITableView.automaticDimension
                }
            }
            
        }else{
            
            if indexPath.row < imageArray.count{
                
                return 700
                
            }else{
                return UITableView.automaticDimension
            }
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentIndexDisplay < serverArray.count{
            //let dict = serverArray[section]
            if self.imageArray.count == 0{
                return 5 + eductionArray.count + jobArray.count + 1
            }else{
                return imageArray.count + 4 + eductionArray.count + jobArray.count + 1 + 2
            }
        }else{
            return 0
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableHome{
            
            if indexPath.row < imageArray.count{
                let cell = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCell1", for: indexPath) as! tableHomeCell1
                
                if indexPath.row == 0{
                    cell.viewTransparent.isHidden = false
                    if jobArray.count > 0{
                        let jobDetail = jobArray[0] as! NSDictionary
                        
                        cell.lblDesigCompany.text = "\(jobDetail["title"] as! String) at \(jobDetail["company"] as! String)"
                    }else{
                        
                        cell.lblDesigCompany.text = ""
                    }
                    
                    if eductionArray.count > 0{
                        let educationDetail = eductionArray[0] as! NSDictionary
                        cell.lblEducation.text = "\(educationDetail["study"] as! String)"
                        
                    }else{
                        cell.lblEducation.text = ""
                    }
                    
                    cell.lblDetail.text = serverArray[currentIndexDisplay ].headline!
                    
                }else{
                    cell.viewTransparent.isHidden = true
                }
                
                
                cell.lblNameAge.isHidden = false
                cell.lblDesigCompany.isHidden = false
                //cell.lblNameAge.text = serverArray[currentIndexDisplay].name!
                // cell.lblDesigCompany.text = serverArray[currentIndexDisplay].name!
                
                if imageArray.count == 0{
                    cell.imgCell.image = UIImage(named: "default_hero_img@1,5x")
                }
                else{
                    let dict = imageArray[indexPath.row] as! NSDictionary
                    cell.imgCell.sd_setImage(with: URL(string:((dict["image_url"] as! String))), placeholderImage: UIImage(named: "default_hero_img@1,5x"))
                }
                
                if String(describing:serverArray[currentIndexDisplay ].dob!) != ""{
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let date = serverArray[currentIndexDisplay ].dob!
                    if date.contains("/"){
                        let dobDate = dateFormatter.date(from:serverArray[currentIndexDisplay ].dob!)!
                        let date = NSDate()
                        let year = (date as Date).years(from : dobDate as Date)
                        
                        cell.lblNameAge.text = serverArray[currentIndexDisplay ].name! + ", " + String(describing: year)
                    }
                    else{
                        cell.lblNameAge.text = serverArray[currentIndexDisplay ].name!
                    }                    }
                //image =  UIImage(named: "\(imageArray[indexPath.row])")
                
                let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
                cell.imgCell.addGestureRecognizer(tapGesture)
                
                
                return cell
                
            }else if indexPath.row == imageArray.count{
                let cell3 = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCell3", for: indexPath) as! tableHomeCell3
                
                if serverArray[currentIndexDisplay].about_me! == ""{
                    cell3.aboutmeLabel.text = ""
                }
                else{
                    cell3.aboutmeLabel.text = serverArray[currentIndexDisplay ].about_me!
                    
                }
                
                return cell3
                
            }else if indexPath.row == imageArray.count+1{
                let cellColl2 = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCollecCell2", for: indexPath) as! tableHomeCollecCell2
                
                cellColl2.collectionView.reloadData()
                
                let height = cellColl2.collectionView.collectionViewLayout.collectionViewContentSize.height
                cellColl2.collectionHeight.constant =  height
                if arraystr.count == 0 {
                    cellColl2.collectionHeight.constant =  0
                }
                else{
                    cellColl2.collectionHeight.constant =  height
                }
                
                return cellColl2
                
            }else if indexPath.row == imageArray.count+2{
                
                let cell = tableHome.dequeueReusableCell(withIdentifier: "InstaImageTableViewCell", for: indexPath) as! InstaImageTableViewCell
                
                return cell
                
                
            }else if indexPath.row == imageArray.count+3{
                
                let cell = tableHome.dequeueReusableCell(withIdentifier: "tableHomeJobCell", for: indexPath) as! tableHomeJobCell
                
                let userName = serverArray[currentIndexDisplay].name!
                cell.lblExperance.text = "\(userName)'s Experience"
                
                return cell
                
                
            }else if indexPath.row <= imageArray.count+3 + jobArray.count{
                
                let cell = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCell6P", for: indexPath) as! tableHomeCell6
                
                if jobArray.count > 0{
                    
                    let jobData = jobArray[indexPath.row - (imageArray.count + 4)] as! NSDictionary
                    cell.lblProfile.text = jobData["title"] as? String
                    cell.lblCompany.text = jobData["company"] as? String
                    cell.lblDate.text = jobData["duration"] as? String
                }
                return cell
                
            }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1{
                
                let cell = tableHome.dequeueReusableCell(withIdentifier: "TableHomeEducationCell", for: indexPath) as! TableHomeEducationCell
                
                let userName = serverArray[currentIndexDisplay].name!
                cell.lblExparence.text = "\(userName)'s Education"
                
                return cell
                
            }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1 + eductionArray.count{
                
                let cell = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCell7P", for: indexPath) as! tableHomeCell7
                
                if eductionArray.count > 0{
                    
                    let eduData = eductionArray[indexPath.row - (imageArray.count + 5 + jobArray.count)] as! NSDictionary
                    
                    cell.lblDegree.text = eduData["study"] as? String
                    cell.lblCollage.text = eduData["institution"] as? String
                    cell.lblData.text = eduData["year"] as? String
                    
                }
                
                return cell
                
            }else if indexPath.row <= imageArray.count+3 + jobArray.count + 1 + eductionArray.count + 1{
                let cell4 = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCell4", for: indexPath) as! tableHomeCell4
                let location = serverArray[currentIndexDisplay ].location!
                let locationArray = location.components(separatedBy: ",")
                
                if locationArray.count>=2{
                    cell4.kmCityLbl.text = serverArray[currentIndexDisplay ].distance! + "km away ," + locationArray[locationArray.count-2]
                }
                else{
                    cell4.kmCityLbl.text = serverArray[currentIndexDisplay ].distance! + "km away"
                }
                cell4.locationTitleLAbel.text = serverArray[currentIndexDisplay ].name! + "'s Location"
                return cell4
            }else{
                let cell5 = tableHome.dequeueReusableCell(withIdentifier: "tableHomeCell5", for: indexPath) as! tableHomeCell5
                return cell5
            }
            
        }
            
            // Back tableview
            
        else{
            
            if serverArray.count > 1{
                
                if indexPath.row < imageArray.count{
                    let cell = tableHomeBack.dequeueReusableCell(withIdentifier: "tableHomeCell1", for: indexPath) as! tableHomeCell1
                    if indexPath.row == 0{
                        
                        cell.lblNameAge.isHidden = false
                        cell.lblDesigCompany.isHidden = false
                        //cell.lblNameAge.text = serverArray[currentIndexDisplay].name!
                        cell.lblDesigCompany.text = serverArray[currentIndexDisplay + 1 ].name!
                        
                        
                        
                        if (serverArray[currentIndexDisplay + 1].profile_picture!) == ""{
                            cell.imgCell.image = UIImage(named: "default_hero_img@1,5x")
                        }
                        else{
                            cell.imgCell.sd_setImage(with: URL(string:(serverArray[currentIndexDisplay + 1].profile_picture!)), placeholderImage: UIImage(named: "default_hero_img@1,5x"))
                        }
                        if String(describing:serverArray[currentIndexDisplay + 1 ].dob!) != ""{
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/yyyy"
                            let date = serverArray[currentIndexDisplay + 1 ].dob!
                            if date.contains("/"){
                                let dobDate = dateFormatter.date(from:serverArray[currentIndexDisplay + 1 ].dob!)!
                                let date = NSDate()
                                let year = (date as Date).years(from : dobDate as Date)
                                
                                cell.lblNameAge.text = serverArray[currentIndexDisplay + 1 ].name! + ", " + String(describing: year)
                            }
                            else{
                                cell.lblNameAge.text = serverArray[currentIndexDisplay + 1 ].name!
                            }
                        }
                        
                        return cell
                    }
                    
                }else if indexPath.row == imageArray.count{
                    let cell3 = tableHomeBack.dequeueReusableCell(withIdentifier: "tableHomeCell3", for: indexPath) as! tableHomeCell3
                    if serverArray[currentIndexDisplay ].about_me! == ""{
                        cell3.aboutmeLabel.text = ""
                        cell3.labelHeight.constant = 0.0
                    }
                    else{
                        cell3.aboutmeLabel.text = serverArray[currentIndexDisplay ].about_me!
                        cell3.labelHeight.constant = 40.0
                    }
                    return cell3
                }else if indexPath.row == imageArray.count+1{
                    let cellColl2 = tableHomeBack.dequeueReusableCell(withIdentifier: "tableHomeCollecCell2", for: indexPath) as! tableHomeCollecCell2
                    if cellColl2.collectionView.contentSize.height > 0{
                        cellColl2.collectionView.reloadData()
                        cellColl2.collectionHeight.constant =  cellColl2.collectionView.contentSize.height
                    }else{
                        cellColl2.collectionView.reloadData()
                        cellColl2.collectionHeight.constant =  cellColl2.collectionView.contentSize.height
                    }
                    return cellColl2
                }else if indexPath.row == imageArray.count+2{
                    let cell4 = tableHomeBack.dequeueReusableCell(withIdentifier: "tableHomeCell4", for: indexPath) as! tableHomeCell4
                    let location = serverArray[currentIndexDisplay + 1 ].location!
                    let locationArray = location.components(separatedBy: ",")
                    
                    
                    if locationArray.count>=2{
                        cell4.kmCityLbl.text = locationArray[locationArray.count-2]
                    }
                    else{
                        cell4.kmCityLbl.text = ""
                    }
                    cell4.locationTitleLAbel.text = serverArray[currentIndexDisplay + 1 ].name! + "'s Location"
                    return cell4
                }else{
                    let cell5 = tableHomeBack.dequeueReusableCell(withIdentifier: "tableHomeCell5", for: indexPath) as! tableHomeCell5
                    return cell5
                }
            }
            
            return UITableViewCell()
        }
        
    }
    
    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        let imgView = tapGesture.view as! UIImageView
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imgView
        }
        let imageViewerController = ImageViewerController(configuration: configuration)
        present(imageViewerController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arraystr.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collcell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionTableHomeCell", for: indexPath) as! collectionTableHomeCell
        let dict = arraystr[indexPath.row] as! NSDictionary
        collcell.imgIcon.sd_setImage(with: URL(string:String(describing:dict["icon"]!)), placeholderImage: UIImage(named: "default_hero_img@1,5x"))
        
        collcell.lblText.text = String(describing:dict["text"]!)
        
        return collcell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let dict = arraystr[indexPath.row] as! NSDictionary
        let strwidth = String(describing:dict["text"]!)
        
        let label = UILabel(frame: CGRect.zero)
        label.text = strwidth
        label.sizeToFit()
        
        /*if strwidth.count > 5{
            return CGSize(width: 60 + strwidth.count*10 , height: 40)
        }else{
            return CGSize(width: 100, height: 40)
        }*/
        return CGSize(width: label.frame.width + 60, height: 40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    @IBAction func didTapProfileHome(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileHomeVC") as! ProfileHomeVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    @IBAction func didTapChatHome(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatHomeVC") as! ChatHomeVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    @IBAction func didTapBurzzSwitch(_ sender: Any) {
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "switchOptionVC") as! switchOptionVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    
    
    
    @IBAction func didtapRightSwipe(_ sender: Any) {
        print("Right swipe hua")
        newView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        newView?.backgroundColor = UIColor.green
        newView?.center = CGPoint(x: tableHome.center.x - tableHome.frame.size.width*0.50 - 50, y: tableHome.center.y)
        self.tableHome.addSubview(newView!)
        let finalPointView = CGPoint(x: tableHome.center.x , y: tableHome.center.y)
        let finishPoint = CGPoint(x: tableHome.center.x - tableHome.frame.size.width * 2, y: tableHome.center.y)
        let tableCenter = self.tableHome.center
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            //self.animateCard(to: finishPoint, angle: -1, alpha: 1.0)
        }, completion: {(_ complete: Bool) -> Void in
            self.tableHome.transform = CGAffineTransform(rotationAngle: 0)
            self.tableHome.center = tableCenter
            self.tableHome.reloadData()
            self.newView?.removeFromSuperview()
        })
    }
    
    
    @IBAction func didTapLeftSwipe(_ sender: Any) {
        print("left swipe hua")
        // let touch = sender.first!
        //let location = (sender as AnyObject).location(in: self)
        
        newView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        newView?.backgroundColor = UIColor.green
        newView?.center = CGPoint(x: tableHome.center.x - tableHome.frame.size.width*0.50 - 50, y: tableHome.center.y)
        self.tableHome.addSubview(newView!)
        let finishPoint = CGPoint(x: tableHome.center.x + tableHome.frame.size.width * 2, y: tableHome.center.y)
        let tableCenter = self.tableHome.center
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            // self.animateCard(to: finishPoint, angle: -1, alpha: 1.0)
        }, completion: {(_ complete: Bool) -> Void in
            self.tableHome.transform = CGAffineTransform(rotationAngle: 0)
            self.tableHome.center = tableCenter
            self.tableHome.reloadData()
        })
    }
    
}



extension HomeVC : TinderSwipeViewDelegate{
    
    func currentCardStatus(card object: Any, distance: CGFloat) {
        print(distance)
    }
    
    func dummyAnimationDone() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear, animations: {
            self.newsCardView.alpha = 1.0
        }, completion: nil)
    }
    
    func cardGoesLeft(_ object: Any) {
        
    }
    
    func cardGoesRight(_ object: Any) {
        
    }
    
    func endOfCardsReached() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            self.newsCardView.alpha = 0.0
        }, completion: nil)
    }
}

class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }
}
//extension TinderCard: UIGestureRecognizerDelegate {
//
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
//}
extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
    }
    enum scrollsTo {
        case top,bottom
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

extension HomeVC{
    func APiCallingSub()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSUBSCRIPTION)
        
        let dicURL : [String : String] = ["user_id": currentUser.result!.id!]
        //  MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPISubscription(APIurl: url!, dictData: dicURL )
        
    }
    
    func CallAPISubscription(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    //   MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        self.ProductlistArray = dict["data"] as! Array<Any>
                                        
                                        let serverDate = dict["timestamp"] as! Int
                                        
                                        let date = Date(timeIntervalSince1970: (Double(serverDate)))
                                        
                                        UserDefaults.standard.setValue(date, forKey: "serverDate")
                                        
                                        let duration =  UserDefaults.standard.value(forKey: "expiresDate") as? String ?? ""
                                        let productId =  UserDefaults.standard.value(forKey: "productId") as? String ?? ""
                                        
                                        if duration != ""{
                                            for i in 0..<self.ProductlistArray.count{
                                                let dict = self.ProductlistArray[i] as! NSDictionary
                                                let ProductID =  "\(String(describing: dict["apple_product_id"]!))"
                                                
                                                if productId == ProductID{
                                                    let dates = Date(timeIntervalSince1970: Double(Int(duration)!)/1000)
                                                    
                                                    let days = Double(dict["validity"] as! String) ?? 0.0
                                                    
                                                    if days == 1.0{
                                                        let modifiedDate = Calendar.current.date(byAdding: .day, value: 7, to: dates)!
                                                        print(modifiedDate)
                                                        UserDefaults.standard.setValue(modifiedDate, forKey: "ValidDate")
                                                        
                                                    }else{
                                                        let modifiedDate = Calendar.current.date(byAdding: .day, value: Int(days), to: dates)!
                                                        print(modifiedDate)
                                                        UserDefaults.standard.setValue(modifiedDate, forKey: "ValidDate")
                                                    }
                                                    break
                                                }
                                            }
                                        }else{
                                            
                                            UserDefaults.standard.setValue(nil, forKey: "ValidDate")
                                        }
                                        
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
}

extension HomeVC{
    func ISVerified()
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.IS_VERIFIED)
        let dicURL : [String : String] = ["id": currentUser.result!.id!]
        self.ISVerifiedAPI(APIurl: url!, dictData: dicURL )
        
    }
    
    func ISVerifiedAPI(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        
                                        let data = dict["data"] as! NSDictionary
                                        
                                        let is_verified = data["is_verified"] as? String ?? "0"
                                        UserDefaults.standard.setValue(is_verified, forKey: "is_verified")
                                        
                                    }
                                    else{
                                        
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
}
