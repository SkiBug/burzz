//
//  ForgotPasswordVC.swift
//  Burzz
//
//  Created by Raul Menendez on 07/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire
import SkyFloatingLabelTextField
import CountryPickerView
class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var countryCodePicker: CountryPickerView!
    @IBOutlet weak var txtRegMobileNum: SkyFloatingLabelTextField!
    
    var otpGet = 0
    var dictData : [String: String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCodePicker.showCountryCodeInView = false
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTabResetSubmit(_ sender: Any) {
       // let deviceToken =  myUser_defaults.value(forKey: KdeviceToken) as! String
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KForgot)")!
        let dict : [String:Any] = ["password":"", "otp":"","username":txtRegMobileNum.text!]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIForGot(APIurl: url, dictData: dict as! [String : String])
    }
    
    func CallAPIForGot(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        let newDict = JSON["data"] as? NSDictionary
                                        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                                        newviewController.otpGetForgot = true
                                        newviewController.dictData = ["password":"","otp":"\(newDict!["otp"] as! NSNumber)","username":self.txtRegMobileNum.text!]
                                        let number = Int(truncating: newDict!["otp"] as! NSNumber)
                                        newviewController.otpGet = number
                                        self.navigationController?.pushViewController(newviewController, animated: true)
                                        //                                        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                                        //                                        newviewController.dictData = ["name":self.txtSignUpUsername.text!,"email":self.txtSignUpEmail.text! , "password":self.txtSignUpPassword.text!,"mobile":self.txtSignUpPassword.text!  ,"login_type" :"0","device_type":"2","device_token":KdeviceToken,"otp":"\((newDict!["otp"] as! NSString) as String)","otp_verify":"1","username":self.txtSignUpUsername.text!]
                                        //                                        newviewController.otpGet = (newDict!["otp"] as! NSString) as String
                                        //                                        self.navigationController?.pushViewController(newviewController, animated: true)
                                        //}
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as? NSString as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                                
                                //QLSharedPreference.sharedInstance.setLoginStatus(true)
                                //QLSharedPreference.sharedInstance.saveUserData(JSON)
                                //QLSharedPreference.sharedInstance.saveUserId(String(describing: result.value(forKey: "id")!))
                                //                                currentUser = User.init(dictionary:JSON)
                                //                                if currentUser.isSuccess!{
                                //                                    self.navigatToHomeScreen()
                                //                                }else{
                                //                                    self.addAlertView(appName, message: currentUser.message!, buttonTitle: ALERTS.kAlertOK)
                                //                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
}

