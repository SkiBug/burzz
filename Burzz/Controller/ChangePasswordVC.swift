//
//  ChangePasswordVC.swift
//  Burzz
//
//  Created by Raul Menendez on 07/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
//import MBProgressHUD
class ChangePasswordVC: UIViewController {
    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtReTypePassword: SkyFloatingLabelTextField!
    var dictData : [String: String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func didTapReset(_ sender: Any) {
        if txtNewPassword.text == txtReTypePassword.text{
            
          
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KForgot)")!
            
            dictData!["password"] = txtNewPassword.text
           // let dict : [String:Any] = ["password":txtNewPassword.text!, "otp":"","username":txtRegMobileNum.text!]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CallAPIForGot(APIurl: url, dictData: (dictData as! [String : String]))
            
            
        }else{
             self.addAlertView(appName, message: "Passwords doesn't match with each other.", buttonTitle: ALERTS.kAlertOK)
            
        }
        
//        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateProfileTableVC") as! CreateProfileTableVC
//        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func CallAPIForGot(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        self.navigationController?.popToRootViewController(animated: true)
                                        //                                        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                                        //                                        newviewController.dictData = ["name":self.txtSignUpUsername.text!,"email":self.txtSignUpEmail.text! , "password":self.txtSignUpPassword.text!,"mobile":self.txtSignUpPassword.text!  ,"login_type" :"0","device_type":"2","device_token":KdeviceToken,"otp":"\((newDict!["otp"] as! NSString) as String)","otp_verify":"1","username":self.txtSignUpUsername.text!]
                                        //                                        newviewController.otpGet = (newDict!["otp"] as! NSString) as String
                                        //                                        self.navigationController?.pushViewController(newviewController, animated: true)
                                        //}
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as? NSString as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                                
                                //QLSharedPreference.sharedInstance.setLoginStatus(true)
                                //QLSharedPreference.sharedInstance.saveUserData(JSON)
                                //QLSharedPreference.sharedInstance.saveUserId(String(describing: result.value(forKey: "id")!))
                                //                                currentUser = User.init(dictionary:JSON)
                                //                                if currentUser.isSuccess!{
                                //                                    self.navigatToHomeScreen()
                                //                                }else{
                                //                                    self.addAlertView(appName, message: currentUser.message!, buttonTitle: ALERTS.kAlertOK)
                                //                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }

    
}
