//
//  SettingsRequest.swift
//  Burzz
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit
import Alamofire

class SettingsRequest: NSObject {
    
    let load = Loader()
    
    func getReferelCode<T : APIResponseParentModel>(vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let postDataUrl = "http://3.215.188.72/index.php/data_model/user/registration/getCreateRefCode"//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/user/registration/getCreateRefCode"
            
            var request = URLRequest(url: URL(string: postDataUrl)!)
            
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let param = ["user_id":currentUser.result!.id!] as [String : String]
            
            Alamofire.request(postDataUrl, method: .post, parameters: param, encoding: URLEncoding.default).responseData { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        print(value)
                        if hud {
                            self.load.hide(delegate: vc)
                        }
                        completion(response.result.value, true)
                    }
                case .failure(let error):
                    print(error)
                    if hud {
                        self.load.hide(delegate: vc)
                    }
                }
            }
        }
    }
}
