//
//  CoinPackageModel.swift
//  Burzz
//
//  Created by Pradipta Maitra on 06/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import Foundation

// MARK: - CoinPackages
struct CoinPackages: Codable {
    let status: Bool?
    let message: String?
    let data: [[Datum]]?
    let isIos, timestamp: Int?

    enum CodingKeys: String, CodingKey {
        case status, message, data
        case isIos = "is_ios"
        case timestamp
    }
}

// MARK: - Datum
struct Datum: Codable {
    let coinID, coin, name, price: String?
    let appleID, googeID, createdAt: String?

    enum CodingKeys: String, CodingKey {
        case coinID = "coin_id"
        case coin, name, price
        case appleID = "apple_id"
        case googeID = "googe_id"
        case createdAt = "created_at"
    }
}
