//
//  CoinPackagesView.swift
//  Burzz
//
//  Created by Pradipta Maitra on 14/07/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyStoreKit

class CoinPackagesView: UIViewController {

    @IBOutlet weak var table_view: UITableView!
    
    var coinPackages: [Datum] = []
    let bundleID = "com.appSquadz.Burzz"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
        
        self.getCoinPackages()
    }
    
    func getCoinPackages() {
        let coinRepository = CoinPackagesRepository()
        coinRepository.getCoinPackages(vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let data = response.data
                self.coinPackages = data?.first ?? []
                DispatchQueue.main.async {
                    self.table_view.reloadData()
                }
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func CallAPIPayment(APIurl: URL,dictData : [String:String]?){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension CoinPackagesView : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coinPackages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coinCell", for: indexPath) as! CoinCell
        cell.selectionStyle=UITableViewCell.SelectionStyle.none
        
        cell.labelCoinValue.text = self.coinPackages[indexPath.row].name
        cell.labelCoinPurchaseAmount.text = "Purchase this for \(self.coinPackages[indexPath.row].price ?? "")"
//        if indexPath.row == 0 {
//            cell.labelCoinPurchaseAmount.text = "Purchase this for $0.99"
//        }else if indexPath.row == 1 {
//            cell.labelCoinPurchaseAmount.text = "Purchase this for $3.99"
//        }else if indexPath.row == 2 {
//            cell.labelCoinPurchaseAmount.text = "Purchase this for $4.99"
//        }else if indexPath.row == 3 {
//            cell.labelCoinPurchaseAmount.text = "Purchase this for $9.99"
//        }else{
//            cell.labelCoinPurchaseAmount.text = "Purchase this for $14.99"
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let appleID = self.coinPackages[indexPath.row].appleID else {
            return
        }
        
        guard let price = self.coinPackages[indexPath.row].price else {
            return
        }
        
        guard let coin = self.coinPackages[indexPath.row].coin else {
            return
        }
        
        self.purchase(appleID, atomically: true, price: price, coin: coin)
    }
}
extension CoinPackagesView{
    func getInfo(_ purchase: RegisteredPurchase) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.retrieveProductsInfo([bundleID + "." + purchase.rawValue]) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            self.showAlert(alert: self.alertForProductRetrievalInfo(result))
        }
    }
    
    func purchase(_ purchase: String, atomically: Bool, price: String, coin: String) {
        
        self.view.isUserInteractionEnabled = false
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(purchase, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            self.view.isUserInteractionEnabled = true
            if case .success(let purchase) = result {
                let downloads = purchase.transaction.downloads
                let productId = purchase.productId
                let transaction_id = purchase.transaction.transactionIdentifier
                MBProgressHUD.hide(for: self.view, animated: true)
                self.expiryDate()
                
                let url = URL(string:  "http://3.215.188.72/index.php/data_model/plans/Plans/savetransactions")//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/plans/Plans/savetransactions")

                let dicURL : [String : String] = ["user_id": currentUser.result!.id!, "transactions_type":"credit","product_id":productId,"transaction_id":"\(transaction_id!)","transaction_from":"apple", "cost": price, "coin": coin]

                self.CallAPIPayment(APIurl: url!, dictData: dicURL)
                
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                    
                }
            }else{
                MBProgressHUD.hide(for: self.view, animated: true)
            }
            if let alert = self.alertForPurchaseResult(result) {
                self.showAlert(alert: alert)
                
            }
        }
    }
    
    func showAlert(alert : UIAlertController) {
        guard let _ = self.presentedViewController else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {

        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
         return alertWithTitle(title: product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let invalidProductId = result.invalidProductIDs.first {
         return alertWithTitle(title: "Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
        } else {
            let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
         return alertWithTitle(title: "Could not retrieve product info", message: errorString)
        }
    }
    
    func alertWithTitle(title : String, message : String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
        
    }
    
    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
            return nil
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle(title: "Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
             return alertWithTitle(title: "Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return nil
            case .paymentInvalid: // purchase identifier was invalid, etc.
             return alertWithTitle(title: "Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
             return alertWithTitle(title: "Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
             return alertWithTitle(title: "Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
             return alertWithTitle(title: "Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
             return alertWithTitle(title: "Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
             return alertWithTitle(title: "Purchase failed", message: "Cloud service was revoked")
            default:
             return alertWithTitle(title: "Purchase failed", message: (error as NSError).localizedDescription)
            }
        }
    }
    
//    func restorePurchases() {
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        SwiftyStoreKit.restorePurchases(atomically: true) { results in
//            NetworkActivityIndicatorManager.networkOperationFinished()
//
//            for purchase in results.restoredPurchases {
//                let downloads = purchase.transaction.downloads
//                if !downloads.isEmpty {
//                    SwiftyStoreKit.start(downloads)
//                } else if purchase.needsFinishTransaction {
//                    // Deliver content from server, then:
//                    SwiftyStoreKit.finishTransaction(purchase.transaction)
//                }
//            }
//            self.showAlert(alert: self.alertForRestorePurchases(result: results))
//        }
//
//    }
//    @IBAction func verifyReceipt() {
//
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        verifyReceipt { result in
//
//            print(result)
//
//            NetworkActivityIndicatorManager.networkOperationFinished()
//            self.showAlert(alert: self.alertForVerifyReceipt(result))
//        }
//    }
    
//    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
//
//        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
//        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
//    }
    
    func expiryDate(){
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                
                guard let data = receipt as? NSDictionary else {return}
                guard let latestReceipt = data["receipt"] as? NSDictionary else {return}
                guard let in_app = latestReceipt["in_app"] as? NSArray else {return}
                
                guard let dict = in_app.lastObject as? NSDictionary else {return}
                guard let productId = dict["product_id"] as? String else {return}
                
                UserDefaults.standard.setValue(productId, forKey: "productId")
                
                let purchaseResult = SwiftyStoreKit.verifyPurchase(
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let receiptItem):
                    print("\(productId) is purchased: \(receiptItem)")
                case .notPurchased:
                    print("The user has never purchased \(productId)")
                }
            case .error(let error):
                print("Receipt verification failed: \(error)")
            }
            
        }
    }
    
    
    
    
//    func verifyPurchase(_ purchase: RegisteredPurchase) {
//
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        verifyReceipt { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()
//
//            switch result {
//            case .success(let receipt):
//
//                let productId = self.bundleID + "." + purchase.rawValue
//
//                switch purchase {
//                case .autoRenewableWeekly, .autoRenewableMonthly, .autoRenewableYearly:
//                    let purchaseResult = SwiftyStoreKit.verifySubscription(
//                        ofType: .autoRenewable,
//                        productId: productId,
//                        inReceipt: receipt)
//                    self.showAlert(alert: self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
//                case .nonRenewingPurchase:
//                    let purchaseResult = SwiftyStoreKit.verifySubscription(
//                        ofType: .nonRenewing(validDuration: 60),
//                        productId: productId,
//                        inReceipt: receipt)
//                    self.showAlert(alert: self.alertForVerifySubscriptions(purchaseResult, productIds: [productId]))
//                default:
//                    let purchaseResult = SwiftyStoreKit.verifyPurchase(
//                        productId: productId,
//                        inReceipt: receipt)
//                    self.showAlert(alert: self.alertForVerifyPurchase(purchaseResult, productId: productId))
//                }
//
//            case .error:
//                self.showAlert(alert: self.alertForVerifyReceipt(result))
//            }
//        }
//    }
    
//    func refreshReceipt() {
//        SwiftyStoreKit.fetchReceipt(forceRefresh: true, completion: { result in
//            
//        })
//    }
    
}
