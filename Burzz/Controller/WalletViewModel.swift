//
//  WalletViewModel.swift
//  Burzz
//
//  Created by Pradipta Maitra on 11/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import Foundation

// MARK: - ViewWallet
struct ViewWalletBalance: Codable {
    let status: Bool?
    let message: String?
    let data: BalanceData?
    let isIos, timestamp: Int?

    enum CodingKeys: String, CodingKey {
        case status, message, data
        case isIos = "is_ios"
        case timestamp
    }
}

// MARK: - DataClass
struct BalanceData: Codable {
    let bal: Int?
}
