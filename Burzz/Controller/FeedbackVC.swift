//
//  FeedbackVC.swift
//  
//
//  Created by MAC MINI on 07/08/19.
//

import UIKit
import Alamofire
//import MBProgressHUD

class FeedbackVC: UIViewController {
    
    @IBOutlet weak var txtView : UITextView!
    @IBOutlet weak var btnDone : UIButton!
    
    
    var dictData : [String: String] = [:]
    
    var blockId = String()
    var reason = String()



    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtView.layer.borderColor = UIColor.gray.cgColor
        txtView.layer.borderWidth = 1
        txtView.layer.cornerRadius = 10
        txtView.clipsToBounds = true
        
        btnDone.layer.cornerRadius = btnDone.frame.height/2
        btnDone.clipsToBounds = true

    }
    
    @IBAction func onClickUserUpdateDetail(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func click_to_send(_ sender: Any)
    {
        var url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.BLOCKUSER)")!
        
        dictData = ["blocked_id" : blockId, "description" : txtView.text, "user_id" : currentUser.result!.id!, "reason" : reason]

        CallAPIResendOTP(APIurl: url, dictData: dictData)
    }
    
    
    
    func CallAPIResendOTP (APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            if let statusData = JSON["status"] as? Int{
                                
                                if statusData == 1{
                                    
                                    let myAlert = UIAlertController(title: "", message:"User Blocked", preferredStyle: UIAlertController.Style.alert);
                                  
                                    
                                    let delete = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                                        
                                       UserDefaults.standard.set(true, forKey: "DismissView")

                                        self.dismiss(animated: true, completion: nil)
                                        
                                        
                                    })
                                    
                                    // this action can add to more button
                                    myAlert.addAction(delete);
                                    
                                    self.present(myAlert, animated: true, completion: nil)
                                    
                                    
                                }else{
                                    
                                }}}}
                }
                
        }}
    

}
