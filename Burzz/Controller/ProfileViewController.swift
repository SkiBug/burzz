//
//  ProfileViewController.swift
//  Burzz
//
//  Created by mac on 20/09/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import MBProgressHUD
import SwiftyJSON
import SDWebImage


class ProfileViewController: UIViewController {
    
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var tblFriendProfile: UITableView!
    
    var friend_id = 0
    var responseFriendProfileData: JSON = []
    
    var arrImage = [String]()
    var arrName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        viewCard.layer.masksToBounds = false
        viewCard.layer.cornerRadius = 10
        viewCard.layer.shadowColor = UIColor.gray.cgColor
        viewCard.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        viewCard.layer.shadowOpacity = 0.2
        viewCard.layer.shadowRadius = 1.0
        
        tblFriendProfile.delegate = self
        tblFriendProfile.dataSource = self

        let url =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.getFriendProfile)")!
        let dictData = ["id" : "\(friend_id)"]
        CallAPIForGot(APIurl: url, dictData: dictData)
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
       
    // Do any additional setup after loading the view.
    
        
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func CallAPIForGot(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        MBProgressHUD.showAdded(to: view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            
                            let JsonData = JSON(response.result.value!)
                            self.responseFriendProfileData = JsonData
                            
                            self.arrImage.removeAll()
                            self.arrName.removeAll()
                            
                            for collectionData in self.responseFriendProfileData["data"]["preference"].arrayValue{
                                
                                let text = collectionData["text"].stringValue
                                
                                if text != ""{
                                    self.arrName.append(text)
                                    self.arrImage.append(collectionData["icon"].stringValue)
                                }
                                
                            }
                
                            self.tblFriendProfile.reloadData()
                            
                            print(JsonData)
                            MBProgressHUD.hide(for: self.view, animated: false)
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
}

extension ProfileViewController : UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row < responseFriendProfileData["data"]["images"].count{
            return viewCard.bounds.height - 20
        }else{
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseFriendProfileData["data"]["images"].count + 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < responseFriendProfileData["data"]["images"].count{
            let cell = tblFriendProfile.dequeueReusableCell(withIdentifier: "tableHomeCell1P", for: indexPath) as! tableHomeCell1
            
            if indexPath.row > 0{
                cell.viewTransparent.isHidden = true
            }else{
                cell.viewTransparent.isHidden = false
            }
            
            let imageUrl = responseFriendProfileData["data"]["images"][indexPath.row]["image_url"].stringValue
            let yourString = imageUrl
            let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
            cell.imgCell.sd_setImage(with: URL(string: URimgUrl1))
            
           // cell.imgCell.image = UIImage(named: )
            
            return cell
        }else if indexPath.row == responseFriendProfileData["data"]["images"].count{
            let cell = tblFriendProfile.dequeueReusableCell(withIdentifier: "tableHomeCell3P", for: indexPath) as! tableHomeCell3
            
            let aboutMe = responseFriendProfileData["data"]["about_me"].stringValue
            
            if aboutMe == ""{
                cell.aboutmeLabel.text = ""
            }else{
                cell.aboutmeLabel.text = responseFriendProfileData["data"]["about_me"].stringValue
            }
            return cell
            
        }else if indexPath.row == responseFriendProfileData["data"]["images"].count + 1{
            let cell = tblFriendProfile.dequeueReusableCell(withIdentifier: "tableHomeCollecCell2P", for: indexPath) as! tableHomeCollecCell2
            
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            
            let height = cell.collectionView.contentSize.height
            cell.collectionView.reloadData()
            
            if arrName.count == 0 {
                cell.collectionHeight.constant =  0
            }
            else{
                cell.collectionHeight.constant =  height
            }
            
            return cell
            
        }else{
            let cell = tblFriendProfile.dequeueReusableCell(withIdentifier: "tableHomeCell4P", for: indexPath) as! tableHomeCell4
            
            cell.locationTitleLAbel.text = "\(responseFriendProfileData["data"]["name"].stringValue) Location"
            let location = responseFriendProfileData["data"]["location"].stringValue
            if location == ""{
                cell.kmCityLbl.text = "Not Available"
                cell.kmCityLbl.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }else{
                cell.kmCityLbl.text = location
            }
            
            return cell
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collcell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionTableHomeCellP", for: indexPath) as! collectionTableHomeCell
        collcell.imgIcon.sd_setImage(with: URL(string:String(describing: arrImage[indexPath.row])), placeholderImage: UIImage(named: "ScreenS1"))
        
        collcell.lblText.text = String(describing:arrName[indexPath.row])
        
        print(arrName.count)
        
        return collcell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let strwidth = String(describing:arrName[indexPath.row])
        if strwidth.count > 5{
            return CGSize(width: 60 + strwidth.count*10 , height: 40)
        }else{
            return CGSize(width: 100, height: 40)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
}

