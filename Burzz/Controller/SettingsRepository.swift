//
//  SettingsRepository.swift
//  Burzz
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit

class SettingsRepository: NSObject {

    var utility = Utility()
    
    func getReferelCode(vc: UIViewController, completion: @escaping (CreateReferelCodeResponseModel, Bool, NSError?) -> Void) {
        let request = SettingsRequest()
        request.getReferelCode(vc: vc, hud: false, codableType: APIResponseParentModel.self) { (response, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(CreateReferelCodeResponseModel.self, from: response! as! Data)
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
