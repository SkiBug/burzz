//
//  EditJobEducationProfileVC.swift
//  Burzz
//
//  Created by mac on 27/09/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
//import MBProgressHUD
import SwiftyJSON

class EditJobEducationProfileVC: UIViewController {
    @IBOutlet weak var lblTittle: UILabel!
    @IBOutlet weak var txtTittle: UITextField!
    @IBOutlet weak var txtCompanyNAme: UITextField!
    @IBOutlet weak var txtFromDate: UIButton!
    @IBOutlet weak var txtToDAte: UIButton!
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    
    
    var jobOrEducation = String()
    var jobData : [String : Any] = [:]
    var fromDate = ""
    var toDate = ""
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(jobOrEducation)
        print(jobData)
        
        if jobOrEducation == "job"{
            lblTittle.text = "Add Job"
            
            txtTittle.placeholder = "Title"
            txtTittle.text = jobData["title"] as? String
            
            txtCompanyNAme.placeholder = "Company (or just industry)"
            txtCompanyNAme.text = jobData["company"] as? String
            
            let myString: String = jobData["duration"] as! String
            let myStringArr = myString.components(separatedBy: "-")
            
            let from = "\(myStringArr[0])-\(myStringArr[1])"
            let to = "\(myStringArr[2])-\(myStringArr[3])"
            
            btnFromDate.setTitle(from, for:.normal)
            btnToDate.setTitle(to, for: .normal)
            
            fromDate = from
            toDate = to
            
        }else{
            lblTittle.text = "Add Education"
            
            txtTittle.placeholder = "Field of study"
            txtTittle.text = jobData["study"] as? String
            
            txtCompanyNAme.placeholder = "Institution"
            txtCompanyNAme.text = jobData["institution"] as? String
            
            let myString: String = jobData["year"] as! String
            let myStringArr = myString.components(separatedBy: "-")
            
            
            let from = "\(myStringArr[0])-\(myStringArr[1])"
            let to = "\(myStringArr[2])-\(myStringArr[3])"
            
            btnFromDate.setTitle(from, for:.normal)
            btnToDate.setTitle(to, for: .normal)
            
            fromDate = from
            toDate = to
            
        }
        
        
    }

    @IBAction func btnFromDate(_ sender: Any) {
        DatePicker.selectDate { (selectedDate) in
            // TODO: Your implementation for date
            let str = selectedDate.dateString("MMM-YYYY")
            self.btnFromDate.setTitle(str, for: .normal)
        }
        
    }
    
    @IBAction func btnToDate(_ sender: Any) {
        DatePicker.selectDate { (selectedDate) in
            // TODO: Your implementation for date
            let str = selectedDate.dateString("MMM-YYYY")
            
            self.btnToDate.setTitle(str, for: .normal)
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnSave(_ sender: Any) {
        
        let strURL : URL
        var dict = [String : Any]()
        
        if jobOrEducation == "education"
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.editUserEducationDetail)")!
            dict = [ "user_id": currentUser.result!.id!, "study" : txtTittle.text!, "institution" : txtCompanyNAme.text!, "year" : "\(btnFromDate.titleLabel!.text!)  -  \(btnToDate.titleLabel!.text!)", "id" : jobData["id"] as! String]
        }
        else
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.editUserJobDetail)")!
            
            dict = [ "user_id": currentUser.result!.id!,"company" : txtCompanyNAme.text!, "title" : txtTittle.text!, "duration" : "\(btnFromDate.titleLabel!.text!)  -  \(btnToDate.titleLabel!.text!)", "id" : jobData["id"] as! String]
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIGetDetails(APIurl: strURL, dictData: dict)
        
    }
    
    @IBAction func btnRemove(_ sender: Any) {
        
        let strURL : URL
        var dict = [String : Any]()
        
        if jobOrEducation == "education"
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.deleteUserEducationDetail)")!
            dict = [ "user_id": currentUser.result!.id!, "id" : jobData["id"] as! String]
        }
        else
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.deleteUserJobDetail)")!
            
            dict = [ "user_id": currentUser.result!.id!,"id" : jobData["id"] as! String]
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIGetDetails(APIurl: strURL, dictData: dict)
    }
    
    
    func CallAPIGetDetails(APIurl: URL,dictData : [String : Any]){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            
                            if let statusData = JSON["status"] as? Int{
                                
                                if statusData == 1{
                                    print(JSON)
                                    QLSharedPreference.sharedInstance.saveUserData(JSON)
                                    self.navigationController?.popViewController(animated: true)
                                    
                                }else{
                                    
                                    
                                    
                                }}}}
                }
                
        }}
    
    
    
    
    
}
