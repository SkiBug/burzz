//
//  CreateProfileTableVC.swift
//  Burzz
//
//  Created by Raul Menendez on 07/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
//import MBProgressHUD
import SkyFloatingLabelTextField
import UserNotifications
import CoreLocation
import AWSCore
import AWSS3
import SDWebImage
import Vision

class CreateProfileTableVC: UIViewController,CLLocationManagerDelegate {
    var AddressText = ""
    var imageUrl = ""
    var screenNo = 0
    var centerMapCoordinate:CLLocationCoordinate2D!
    let locationManager = CLLocationManager()
    @IBOutlet weak var imgFemale: UIImageView!
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var viewProfileName: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProfileBirthday: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProfileLikes: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProfileDislikes: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProfileIntrestedIn: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProfileRecoveryEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProfileImage2: UIView!
    @IBOutlet weak var viewProfileDetail3: UIView!
    @IBOutlet weak var viewSelOptionLast5: UIView!
    @IBOutlet weak var viewLocation4: UIView!
    @IBOutlet weak var btnAllowLocation: UIButton!
    @IBOutlet weak var viewPickPicture1: UIView!
    @IBOutlet weak var lblNumberProfile: UILabel!
    @IBOutlet weak var btnUploadProfile: UIButton!
    @IBOutlet weak var btnFacebookFill: UIButton!
    @IBOutlet weak var collectionProfile: UICollectionView!
    @IBOutlet weak var viewFaceDetection: UIView!
    @IBOutlet weak var imageViewFace: UIImageView!
    @IBOutlet weak var buttonCloseFaceView: UIButton!
    @IBOutlet weak var buttonProceedFaceView: UIButton!
    
    
    @IBOutlet weak var backBtn: UIButton!
    var userProfileImage = UIImage()
    var imageArray = [String]()
    var maleFemale = 1 //1 male 2 female
    var intrestedIn = 0 // 1 male 2 female
    var image: UIImage!
    var likeArray: [String] = []
    var dislikeArray: [String] = []
    var like_Picker : UIPickerView?
    var dislike_Picker : UIPickerView?
    var isFaceDetected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // view1 Pick picture
        viewProfileImage2.isHidden = true
        viewProfileDetail3.isHidden = true
        viewLocation4.isHidden = true
        viewSelOptionLast5.isHidden = true
        viewFaceDetection.isHidden = true
        
        btnUploadProfile.layer.borderWidth = 2.0
        btnUploadProfile.layer.borderColor = UIColor.yellow.cgColor
        btnFacebookFill.layer.borderWidth = 2.0
        btnFacebookFill.layer.borderColor = UIColor.yellow.cgColor
        btnAllowLocation.layer.borderColor = UIColor.yellow.cgColor
        
        self.buttonCloseFaceView.layer.cornerRadius = self.buttonCloseFaceView.frame.size.height/2
        self.buttonCloseFaceView.layer.borderWidth = 2.0
        self.buttonCloseFaceView.layer.borderColor = UIColor.yellow.cgColor
        self.buttonProceedFaceView.layer.cornerRadius = self.buttonProceedFaceView.frame.size.height/2
        self.buttonProceedFaceView.layer.borderWidth = 2.0
        self.buttonProceedFaceView.layer.borderColor = UIColor.yellow.cgColor
        
        self.likeArray = ["The word “darling”", "Talking to strangers", "Tulips", "The 90s", "Starry nights", "Dogs", "Libraries", "Winning", "Late nights", "Debates", "Listening to other people’s love stories", "Long walks", "Pine cones", "The British accent", "Cool wind blowing to my face (and my hair)", "Numbers", "Wagasa", "Thrift shops", "Bamboo", "Museums", "Magic", "Happy endings", "Hoodies", "Postage stamps", "Vintage stuffs/designs", "Airports/airplanes", "Spontaneity"]
        
        self.dislikeArray = ["Being late", "Needles", "Milk and cheese", "Long queues", "Noisy housemates", "Strong scents", "People trying to control me", "Invasion of privacy", "Spiders", "Bad customer service", "The sea (or any bodies of water)", "Lazy people", "Arrogance and stupidity combined", "Ironing my clothes", "My favorite character killed in a book/movie", "Two-timers", "Hospitals"]
        
        /* if screenNo == 5{
         //dating friend buzz
         viewPickPicture1.isHidden = true
         viewLocation4.isHidden = true
         viewSelOptionLast5.isHidden = false
         lblNumberProfile.text = "4/4"
         }else if screenNo == 4{
         //location
         viewPickPicture1.isHidden = true
         viewProfileDetail3.isHidden = true
         viewLocation4.isHidden = false
         lblNumberProfile.text = "3/4"
         }else if screenNo == 3{
         // profile info
         viewPickPicture1.isHidden = true
         viewProfileImage2.isHidden = false
         imageArray.append((currentUser.result?.profile_picture)!)
         collectionProfile.reloadData()
         lblNumberProfile.text = "2/4"
         }else if screenNo == 2{
         lblNumberProfile.text = "1/4"
         imageArray.append((currentUser.result?.profile_picture)!)
         viewPickPicture1.isHidden = true
         viewProfileImage2.isHidden = false
         }
         //        else if currentUser.result.intrestedIn == 0{
         //
         //        }else if currentUser.result.intrest
         //view 2 after pick profile pic
         
         // Do any additional setup after loading the view.*/
        
        if currentUser == nil {
            currentUser = User1.init(dictionary: QLSharedPreference.sharedInstance.getAllUserData()!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.isFaceDetected = false
        self.createLikePicker()
        self.createDislikePicker()
    }
    
    //MARK: - Prepare  Like PickerViews
    func createLikePicker()
    {
        
        like_Picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 216))
        like_Picker?.dataSource = self
        like_Picker?.delegate = self
        
        self.viewProfileLikes.inputView = like_Picker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.bounds.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDoneLike), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancelLike), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        view_Accessory.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        
        self.viewProfileLikes.inputAccessoryView = view_Accessory
    }
    
    //MARK: - Prepare  Like PickerViews
    func createDislikePicker()
    {
        
        dislike_Picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 216))
        dislike_Picker?.dataSource = self
        dislike_Picker?.delegate = self
        
        self.viewProfileDislikes.inputView = dislike_Picker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.bounds.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDoneDislike), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancelDislike), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        view_Accessory.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        
        self.viewProfileDislikes.inputAccessoryView = view_Accessory
        
    }
    
    @objc func selectionDoneLike(_ sender: UIButton ) {
        
        self.viewProfileLikes.resignFirstResponder()
    }
    
    @objc func selectionCancelLike(_ sender: UIButton ) {
        self.viewProfileLikes.resignFirstResponder()
    }
    
    @objc func selectionDoneDislike(_ sender: UIButton ) {
        
        self.viewProfileDislikes.resignFirstResponder()
    }
    
    @objc func selectionCancelDislike(_ sender: UIButton ) {
        self.viewProfileDislikes.resignFirstResponder()
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        print("tag \(sender.tag)")
        self.isFaceDetected = false
        if sender.tag == 1{
            self.viewPickPicture1.isHidden = false
            self.viewProfileImage2.isHidden = true
            imageArray.removeAll()
            self.collectionProfile.reloadData()
            backBtn.tag = 0
        }
        else if sender.tag == 2{
            viewProfileImage2.isHidden = false
            viewProfileDetail3.isHidden = true
            lblNumberProfile.text = "1/4"
            self.collectionProfile.reloadData()
            backBtn.tag = 1
        }
        else if sender.tag == 3{
            viewLocation4.isHidden = true
            viewProfileDetail3.isHidden = false
            lblNumberProfile.text = "2/4"
            backBtn.tag = 2
        }
        else if sender.tag == 4{
            backBtn.tag = 3
            viewSelOptionLast5.isHidden = true
            viewLocation4.isHidden = false
            lblNumberProfile.text = "3/4"
        }else{
            self.addAlertView(appName, message: "Please complete your profile"  , buttonTitle: ALERTS.kAlertOK)
        }
        
    }
    @IBAction func didTapContinue23(_ sender: Any) {
        
        viewProfileImage2.isHidden = true
        viewProfileDetail3.isHidden = false
        lblNumberProfile.text = "2/4"
        self.backBtn.tag = 2
        
    }
    
    
    @IBAction func didTapIntrestedIn(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Male", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.intrestedIn = 1
            self.viewProfileIntrestedIn.text = "Male"
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Female", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.intrestedIn = 2
            self.viewProfileIntrestedIn.text = "Female"
        }))
        /* actionSheet.addAction(UIAlertAction(title: "Everyone", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
         self.intrestedIn = 3
         self.viewProfileIntrestedIn.text = "Everyone"
         }))*/
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func buttonCloseFaceDetectionView(_ sender: Any) {
        self.viewFaceDetection.isHidden = true
    }
    @IBAction func buttonProceedFaceDetectionAction(_ sender: Any) {
        if self.isFaceDetected == true {
            let dic : [String : Any] = ["name" : image ,"type":"image","identifier" :"image","path" : APIManager.sharedInstance.S3BucketNameProfileImage]
            UserDefaults.standard.set(0, forKey: "rotetedBy")

            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.hitUploadImageToSSS(dataDict: dic)
        }else{
            self.addAlertView(appName, message: "Please choose valid face", buttonTitle: ALERTS.kAlertOK)
        }
        self.viewFaceDetection.isHidden = true
    }
    
    func faceDetect() {
        var orientation:Int32 = 0
        
        // detect image orientation, we need it to be accurate for the face detection to work
        switch userProfileImage.imageOrientation {
        case .up:
            orientation = 1
        case .right:
            orientation = 6
        case .down:
            orientation = 3
        case .left:
            orientation = 8
        default:
            orientation = 1
        }
        
        // vision
        let faceLandmarksRequest = VNDetectFaceLandmarksRequest(completionHandler: self.handleFaceFeatures)
        let requestHandler = VNImageRequestHandler(cgImage: image.cgImage!, orientation: CGImagePropertyOrientation(rawValue: CGImagePropertyOrientation.RawValue(orientation))! ,options: [:])
        do {
            try requestHandler.perform([faceLandmarksRequest])
        } catch {
            print(error)
        }
    }
    
    func handleFaceFeatures(request: VNRequest, errror: Error?) {
        guard let observations = request.results as? [VNFaceObservation] else {
            fatalError("unexpected result type!")
        }
        
        for face in observations {
            addFaceLandmarksToImage(face)
        }
    }
    
    func addFaceLandmarksToImage(_ face: VNFaceObservation) {
        
        UIGraphicsBeginImageContextWithOptions(image.size, true, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        // draw the image
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        
        context?.translateBy(x: 0, y: image.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        // draw the face rect
        let w = face.boundingBox.size.width * image.size.width
        let h = face.boundingBox.size.height * image.size.height
        let x = face.boundingBox.origin.x * image.size.width
        let y = face.boundingBox.origin.y * image.size.height
        let faceRect = CGRect(x: x, y: y, width: w, height: h)
        context?.saveGState()
        context?.setStrokeColor(UIColor.red.cgColor)
        context?.setLineWidth(8.0)
        context?.addRect(faceRect)
        context?.drawPath(using: .stroke)
        context?.restoreGState()
        
        // face contour
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.faceContour {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // outer lips
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.outerLips {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // inner lips
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.innerLips {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // left eye
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.leftEye {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // right eye
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.rightEye {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // left pupil
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.leftPupil {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // right pupil
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.rightPupil {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // left eyebrow
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.leftEyebrow {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // right eyebrow
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.rightEyebrow {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // nose
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.nose {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.closePath()
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // nose crest
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.noseCrest {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // median line
        context?.saveGState()
        context?.setStrokeColor(UIColor.yellow.cgColor)
        if let landmark = face.landmarks?.medianLine {
            for i in 0...landmark.pointCount - 1 { // last point is 0,0
                let point = landmark.normalizedPoints[i]
                if i == 0 {
                    context?.move(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                } else {
                    context?.addLine(to: CGPoint(x: x + CGFloat(point.x) * w, y: y + CGFloat(point.y) * h))
                }
            }
        }
        context?.setLineWidth(8.0)
        context?.drawPath(using: .stroke)
        context?.saveGState()
        
        // get the final image
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // end drawing context
        UIGraphicsEndImageContext()
        self.isFaceDetected = true
        self.imageViewFace.image = finalImage
    }
    
    @IBAction func didTapContinue34(_ sender: Any) {
        
        
        if viewProfileName.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.kPleaseEnterName, buttonTitle: ALERTS.kAlertOK)
        }else if viewProfileBirthday.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.KPLEASEENTERBirthday, buttonTitle: ALERTS.kAlertOK)
        }else if maleFemale == 0{
            self.addAlertView(appName, message: ALERTS.KPLEASEENTERGender, buttonTitle: ALERTS.kAlertOK)
        }else if viewProfileLikes.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.KPLEASEENTERLikes, buttonTitle: ALERTS.kAlertOK)
        }else if viewProfileDislikes.text?.isEmpty == true{
            self.addAlertView(appName, message: ALERTS.KPLEASEENTERDisLikes, buttonTitle: ALERTS.kAlertOK)
        }else if intrestedIn == 0{
            self.addAlertView(appName, message: ALERTS.KPLEASEENTERIntrestedIn, buttonTitle: ALERTS.kAlertOK)
            //        }
            //
            //        else if viewProfileRecoveryEmail.text != ""{
            //            if !isValidEmail(testStr: viewProfileRecoveryEmail.text!){
            //
            //                self.addAlertView(appName, message: ALERTS.KPleaseEnterVaildEmailID, buttonTitle: ALERTS.kAlertOK)
            //
            //            }
            //
            //            else{
            //
            //
            //        }
        }else{
            
            
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
            let dict : [String:String] = ["id":(currentUser.result?.id)!, "name":viewProfileName.text!,"dob":viewProfileBirthday.text!, "gender": "\(maleFemale)", "likes":viewProfileLikes.text!, "dislikes":viewProfileDislikes.text!,
                                          "interest_in":"\(intrestedIn)", "recovery_email":viewProfileRecoveryEmail.text!]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.CallAPIProfile(APIurl: url, dictData: dict as! [String : String])
            
            // self.addAlertView(appName, message: ALERTS.KPleaseEnterVaildEmailID, buttonTitle: ALERTS.kAlertOK)
        }
        
    }
    
    @IBAction func didTapContinue45(_ sender: Any) {
        viewLocation4.isHidden = true
        viewSelOptionLast5.isHidden = false
        lblNumberProfile.text = "4/4"
    }
    
    
    func CallAPIProfile(APIurl: URL,dictData : [String:String]?){
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            self.viewProfileDetail3.isHidden = true
                            self.viewLocation4.isHidden = false
                            self.lblNumberProfile.text = "3/4"
                            self.backBtn.tag = 3
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        //currentUser = User.init(dictionary:JSON)
                                        QLSharedPreference.sharedInstance.saveUserData(JSON)
                                        QLSharedPreference.sharedInstance.setLoginStatus(true)
                                        
                                        let data = JSON["data"] as! NSDictionary
                                        let intrestType = data["interest"] as! String
                                        QLSharedPreference.sharedInstance.intrestType(intrestType)
                                        
                                        
                                        currentUser = User1.init(dictionary:JSON)
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
    
    @IBAction func didTapMale(_ sender: Any) {
        imgMale.image = UIImage(named: "radio_on")
        imgFemale.image = UIImage(named: "radio_off")
        maleFemale = 1
    }
    
    @IBAction func didTapFemale(_ sender: Any) {
        imgMale.image = UIImage(named: "radio_off")
        imgFemale.image = UIImage(named: "radio_on")
        maleFemale = 2
    }
    
    @IBAction func didTapDating(_ sender: Any) {
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        let dict : [String:String] = ["id":(currentUser.result?.id)!,"interest": "1"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url, dictData: dict )
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapFriends(_ sender: Any) {
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        let dict : [String:String] = ["id":(currentUser.result?.id)!,"interest": "2"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url, dictData: dict )
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapBuzzNet(_ sender: Any) {
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        let dict : [String:String] = ["id":(currentUser.result?.id)!,"interest": "3"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIProfile(APIurl: url, dictData: dict )
        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(newviewController, animated: true)
    }
    
    @IBAction func didTapProfilePic(_ sender: Any) {
        //btnUploadProfile.backgroundColor = UIColor.yellow
        self.isFaceDetected = false
        let myAlert = UIAlertController(title: "Warning", message:"You are required to use a real photo with your face and nothing else.If we suspect you are an impostor, your  account will be permanently deleted and phone IP blocked.  ", preferredStyle: UIAlertController.Style.alert);
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
            self.getImageSource()
        })
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    
    func getImageSource(){
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    
    
    @IBAction func didTapFacebookPic(_ sender: Any) {
        
    }
    
    
    @IBAction func didTapCalender(_ sender: Any) {
        self.view.endEditing(true)
        let dateView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250))
        dateView.backgroundColor = UIColor.white
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
        let components: NSDateComponents = NSDateComponents()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        components.year = -18
        let minDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        
        //datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = UIColor.clear
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        dateView.center = CGPoint(x: self.view.frame.size.width*0.50, y: self.view.frame.size.height - 125)
        self.view.addSubview(dateView)
        dateView.addSubview(datePicker)
        let doneBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        doneBtn.setTitle("Done", for:UIControl.State.normal)
        doneBtn.setTitleColor(UIColor.blue, for: UIControl.State.normal)
        doneBtn.addTarget(self, action: #selector(didtapDone), for:UIControl.Event.touchUpInside)
        dateView.addSubview(doneBtn)
        doneBtn.center = CGPoint(x: dateView.frame.size.width - 50, y: dateView.frame.size.height - 25)
        let dateFormatter1: DateFormatter = DateFormatter()
        dateFormatter1.dateFormat = "dd/MM/yyyy"
        // dateFormatter1.dateFormat = .date
        // dateFormatter1.dateFormat = "MMddyyyy"
        
        viewProfileBirthday.text = dateFormatter1.string(from: datePicker.date)
        datePicker.date = minDate as Date
    }
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        
        // Create date formatter yyyy/mm/dd hh:mm:ss
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        // dateFormatter.dateFormat = .da
        dateFormatter.dateFormat = "dd/MM/yyyy" //yyyy/MM/dd"
        //"MM/dd/yyyy hh:mm a"
        // Apply date format
        
        let date = NSDate()
        var year = (date as Date).offset(from : sender.date)
        print(year)
        if year.contains("m") || year.contains("d"){
            year = ""
        }
        year = year.replacingOccurrences(of: "y", with: "")
        if Int(year)! < 18{
            self.addAlertView(appName, message: "Minimum Age should be 18 yrs", buttonTitle: ALERTS.kAlertOK)
        }
        else{
            viewProfileBirthday.text = dateFormatter.string(from: sender.date)
        }
        
    }
    @objc func didtapDone(_ sender: UIButton){
        //let dateFormatter: DateFormatter = DateFormatter()
        // dateFormatter.dateFormat = "dd/MM/yyyy"
        //viewProfileBirthday.text = dateFormatter.string(from: dateFormatter)
        
        // datePickerValueChanged(_:)
        sender.superview?.removeFromSuperview()
        // tablePayment.reloadData()
    }
    
    @IBAction func didTapAllowLocation(_ sender: Any) {
        //        latitude
        //        longitude
        MBProgressHUD.showAdded(to: self.view, animated: true)
        backBtn.isHidden = true
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        MBProgressHUD.hide(for: self.view, animated: true)
        
        let location = locations.last!
        self.centerMapCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        self.placeMarkerOnCenter(centerMapCoordinate:self.centerMapCoordinate)
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!
        
        var lat = 28.6210
        var long = 77.3812
        
        lat = location.coordinate.latitude
        long = location.coordinate.longitude
        
        let dict : [String:String] = ["id":(currentUser.result?.id)!,"location": AddressText , "latitude": "\(lat)", "longitude": "\(long)", "distance" : "10"]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        self.CallAPIProfile(APIurl: url, dictData: dict as! [String : String])
        
        viewLocation4.isHidden = true
        viewSelOptionLast5.isHidden = false
        lblNumberProfile.text = "4/4"
        
    }
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
            MBProgressHUD.hide(for: self.view, animated: true)
        case .denied:
            MBProgressHUD.hide(for: self.view, animated: true)
            self.addAlertView(appName, message: "Registration pending due to deny to access device location.", buttonTitle: ALERTS.kAlertOK)
            
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    func placeMarkerOnCenter(centerMapCoordinate:CLLocationCoordinate2D) {
        centerMapCoordinate.latitude
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(centerMapCoordinate.latitude)")!
        //21.228124
        let lon: Double = Double("\(centerMapCoordinate.longitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks != nil{
                    let pm = placemarks! as [CLPlacemark]
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country!)
                        print(pm.locality!)
                        print(pm.subLocality!)
                        //   print(pm.thoroughfare!)
                        //                        print(pm.postalCode!)
                        //                        print(pm.subThoroughfare!)
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        self.AddressText = "\(pm.locality)"
                        //self.txtAddress.text = "\(addressString)"
                        print(addressString)
                    }
                }
        })
    }
}

extension CreateProfileTableVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func camera()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate;
//        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPicController, animated: true, completion: nil)
        
    }
    
    func photoLibrary()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
//        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        userProfileImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.dismiss(animated: true, completion: nil)
        
//        let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "FaceDetectionView") as! FaceDetectionView
//        newviewController.image = self.userProfileImage
//        newviewController.imageArray = self.imageArray
//        self.navigationController?.pushViewController(newviewController, animated: true)
        
        self.image = userProfileImage
        
        self.viewFaceDetection.isHidden = false
        self.imageViewFace.image = userProfileImage
        self.faceDetect()
    }
    
    func hitUploadImageToSSS(dataDict:  [String : Any]) {
        AmazonSSS.uploadDocumentToS3Server(dataDict: dataDict as NSDictionary) { (result, response, error, errorMessage) in
            MBProgressHUD.hide(for: self.view, animated: true)
            //self.activityIndicatorView.stopAnimating()
            if( error==nil && result == true){
                
                
                
                print(response!)
                self.imageUrl = String(describing: response!)
                self.imageUrl = self.imageUrl.replacingOccurrences(of: "burzz/", with: "burzz//")
                
                self.imageArray.append(self.imageUrl)
                let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPLOADIMG1)")!
                
                let stringImage = self.imageArray.joined(separator: ", ")
                print(stringImage)
                
                
                
                // self.viewWillAppear(true)
                
                let dict : [String:Any] = ["id": currentUser.result!.id!,"images": stringImage]
                MBProgressHUD.showAdded(to: self.view, animated: true)
                self.UploadImg(APIurl: url, dictData: dict )
                self.collectionProfile.reloadData()
                currentUser.result?.profile_picture = self.imageArray[0]
                
            }
            else{
                self.addAlertView(appName, message: ALERTS.KUnableToUpload, buttonTitle: ALERTS.kAlertOK)
                // self.userImageView.image = UIImage(named: "ScreenS1")
            }
            // self.collectionProfile.reloadData()
        }
    }
    
    func UploadImg (APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                
                self.addAlertView(appName, message: "Image Uploaded." , buttonTitle: ALERTS.kAlertOK)
                self.backBtn.tag = 1
                self.viewPickPicture1.isHidden = true
                self.viewProfileImage2.isHidden = false
                //  self.collectionProfile.reloadData()
        }
    }
}


extension CreateProfileTableVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionProfile.dequeueReusableCell(withReuseIdentifier: "collectionProfileCell", for: indexPath) as! collectionProfileCell
        
        cell.deleteBtn.setImage(nil, for: .normal)
        cell.deleteBtn.isUserInteractionEnabled = false
        cell.deleteBtn.tag = indexPath.row
        
        if indexPath.row < imageArray.count{
            cell.imgProfile.sd_setImage(with: URL(string:imageArray[indexPath.row]), placeholderImage: UIImage(named: ""))
            cell.deleteBtn.setImage(UIImage(named: "yellow_cross.png"), for: .normal)
            cell.deleteBtn.isUserInteractionEnabled = true
            cell.deleteBtn.addTarget(self, action: #selector(removeImage), for: .touchUpInside)
            
        }else{
            if indexPath.row == imageArray.count{
                cell.addImageSign.image = UIImage(named: "add_image_active")
            }else{
                cell.addImageSign.image = UIImage(named: "add_image_default")
            }
            cell.imgProfile.image = nil
            
        }
        return cell
    }
    @objc func  removeImage(_ sender:UIButton){
        
        if imageArray.count == 1{
            self.addAlertView(appName, message: "Atleast one picture is required to proceed further." , buttonTitle: ALERTS.kAlertOK)
        }
        else{
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPLOADIMG1)")!
            imageArray.remove(at: sender.tag)
            let stringImage = self.imageArray.joined(separator: ", ")
            let dict : [String:Any] = ["id": currentUser.result!.id!,"images": stringImage]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.UploadImg(APIurl: url, dictData: dict )
            currentUser.result?.profile_picture = self.imageArray[0]
            self.collectionProfile.reloadData()
            print(stringImage)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // return CGSize(width: collectionView.frame.size.width*0.33 , height: collectionView.frame.size.width*0.33)
        return CGSize(width: collectionView.contentSize.width/3, height: collectionView.contentSize.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == imageArray.count{
            
            let myAlert = UIAlertController(title: "Warning", message:"You are required to use a real photo with your face and nothing else.If we suspect you are an impostor, your  account will be permanently deleted and phone IP blocked.  ", preferredStyle: UIAlertController.Style.alert);
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:  { action in
                self.isFaceDetected = false
                self.getImageSource()
            })
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
        }
    }
}
extension CreateProfileTableVC : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == like_Picker{
            return self.likeArray.count
        }else if pickerView == dislike_Picker{
            return self.dislikeArray.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == like_Picker{
            return self.likeArray[row]
        }else if pickerView == dislike_Picker{
            return self.dislikeArray[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == like_Picker{
            self.viewProfileLikes.text = self.likeArray[row]
        }else if pickerView == dislike_Picker{
            self.viewProfileDislikes.text = self.dislikeArray[row]
        }
    }
}
