//
//  MyWork&EducationVC.swift
//  Burzz
//
//  Created by Gopal on 08/08/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Alamofire
//import MBProgressHUD

protocol refreshUserData {
    func pageRefresh()
}

class MyWork_EducationVC: UIViewController {

    @IBOutlet weak var addJobBackView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblWorkAndEducation: UILabel!
    @IBOutlet weak var lblAddJob: UILabel!
    @IBOutlet weak var lblDescTittle: UILabel!
    @IBOutlet weak var lblJobTittle: UILabel!
    
    
    var jobOrEducation = String()
    var educationDetails = NSArray()
   
    var refreshData : refreshUserData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblWorkAndEducation.text = jobOrEducation
        lblAddJob.text = "Add " + jobOrEducation
        
        if jobOrEducation == "job"{
            
            lblDescTittle.text = "You can show 4 job on your profile at a time"
            lblJobTittle.text = "Jobs you've added"
            
        }else{
            lblDescTittle.text = "You can show upto 4 institutions on your profile on a time"
            lblJobTittle.text = "Institutions you've added"
        }
        
      
    }

    override func viewDidAppear(_ animated: Bool) {

     
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let strURL : URL
        
        if jobOrEducation == "education"
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.GETEDUCATIONDETAILS)")!
        }
        else
        {
            strURL =  URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.GETJOBDETAILS)")!
        }
        
        let dict = [ "user_id": currentUser.result!.id!]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIGetDetails(APIurl: strURL, dictData: dict)
        
    }
    
    
    
    func CallAPIGetDetails(APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            if let statusData = JSON["status"] as? Int{
                                
                                if statusData == 1{
                               
                                    //QLSharedPreference.sharedInstance.saveUserData(JSON)
                                    
                                    self.educationDetails = []
                                    self.educationDetails = JSON["data"] as! NSArray
                                    
                                    print(self.educationDetails)
                                    self.tableView.reloadData()
                    
                                }else{
                                    
                    }}}}
                }
                
        }}
    
    
    
    
    @IBAction func click_to_add(_ sender: Any) {
        
//        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddWork_EductionVC") as! AddWork_EductionVC
//        newViewController.jobOrEducation = self.jobOrEducation
//        self.present(newViewController, animated: true, completion: nil)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AddWork_EductionVC") as! AddWork_EductionVC
        newViewController.jobOrEducation = self.jobOrEducation
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    @IBAction func click_to_back(_ sender: Any) {
        
        refreshData?.pageRefresh()
        self.navigationController?.popViewController(animated: true)
       // self.dismiss(animated: true, completion: nil)
    }
  
}

extension MyWork_EducationVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return   educationDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWork_EducationCell", for: indexPath) as! MyWork_EducationCell
        
        cell.backView.layer.cornerRadius = cell.backView.frame.height/2
        cell.backView.layer.borderWidth = 1.0
        cell.backView.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0).cgColor
        cell.txtT3.textColor = UIColor.lightGray
        
        let education = educationDetails[indexPath.row] as! NSDictionary
        
        if jobOrEducation == "education"
        {
            cell.txtT2.text = education["study"] as? String
            cell.txtT1.text = education["institution"] as? String
            cell.txtT3.text = education["year"] as? String
        }
        else
        {
            cell.txtT2.text = education["company"] as? String
            cell.txtT1.text = education["title"] as? String
            cell.txtT3.text = education["duration"] as? String
        }
        
        cell.btnEdit.addTarget(self, action: #selector(edit(sender:)), for: .touchUpInside)
        cell.btnEdit.tag = indexPath.row
        return cell
    }
  
    @objc func edit(sender: UIButton){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "EditJobEducationProfileVC") as! EditJobEducationProfileVC
        newViewController.jobOrEducation = self.jobOrEducation
        newViewController.jobData = educationDetails[sender.tag] as! [String : Any]
        self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
    
    
    
}


class MyWork_EducationCell : UITableViewCell
{
    @IBOutlet weak var txtT2: UILabel!
    @IBOutlet weak var txtT1: UILabel!
    @IBOutlet weak var txtT3: UILabel!

    
    @IBOutlet weak var backView : UIView!
    @IBOutlet weak var btnEdit: UIButton!
    
    
    override func awakeFromNib() {
       
    }
    
  

}
